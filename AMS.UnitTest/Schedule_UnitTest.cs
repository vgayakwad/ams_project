using AMS.BL;
using AMS.Models.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS.UnitTest
{
  [TestClass]
  public class Schedule_UnitTest
  {
    [TestMethod]
    public void GetArtistsScheduledEventsWithInvalidId()
    {
      ScheduleBL account = new ScheduleBL();

      List<ScheduledEventsDTO> scheduledEventsDTO = account.GetArtistsScheduledEvents(0);
      try
      {
        Assert.AreEqual(0, scheduledEventsDTO.Count);
      }
      catch (Exception ex)
      {
        Assert.Fail("Expected no exception, but got: " + ex.Message);
        throw;
      }
    }
    //[TestMethod]
    //public void GetArtistsScheduledEventsWithValidId()
    //{
    //  ScheduleBL common = new ScheduleBL();
    //  List<ScheduledEventsDTO> scheduledEventsDTO = common.GetArtistsScheduledEvents(2);
    //  try
    //  {
    //    Assert.AreEqual(true, scheduledEventsDTO.Count > 0);
    //  }
    //  catch (Exception ex)
    //  {
    //    Assert.Fail("Expected no exception, but got: " + ex.Message);
    //    throw;
    //  }
    //}
    [TestMethod]
    public void ValidEventInvalidArtist()
    {
      try
      {
        ScheduleBL common = new ScheduleBL();
        List<ScheduledEventsDTO> scheduledEventsDTO = common.GetArtistsScheduledEvents(33);
        Assert.AreEqual(0, scheduledEventsDTO.Count);
      }
      catch (Exception ex)
      {
        Assert.Fail("Expected no exception, but got: " + ex.Message);
        throw;
      }
    }

    [TestMethod]
    public void CreateArtistScheduleWithInvalidId()
    {
      try
      {
        ScheduleBL common = new ScheduleBL();
        ArtistScheduleDTO lr = new ArtistScheduleDTO();
        lr.ArtistId =0;
        lr.FirstName = "";
        lr.CountryName = "";
        lr.CountryId = 0;
        lr.CountryCode = "";
        lr.CityId = 0;
        lr.Address = "";
        bool check = common.CreateArtistSchedule(lr);
        Assert.AreEqual(false, check);
      }
      catch (Exception ex)
      {
        Assert.Fail("Expected no exception, but got: " + ex.Message);
        throw;
      }
    }

    [TestMethod]
    public void CreateArtistScheduleWithValidId()
    {
      try
      {
        ScheduleBL common = new ScheduleBL();
        ArtistScheduleDTO lr = new ArtistScheduleDTO();
        lr.ArtistId=10069;
        lr.CityId=10443;
        lr.CountryCode = "US";
        lr.StateCode = "AZ";
        lr.Status = "BUSY";
        lr.Address = "baner teerth technospace";
        lr.AvailableFrom = "12/1/2019";
        lr.AvailableTo = "12/2/2019";
        bool check = common.CreateArtistSchedule(lr);
        Assert.AreEqual(true, check);
      }
      catch (Exception ex)
      {
        Assert.Fail("Expected no exception, but got: " + ex.Message);
        throw;
      }
    }
   
    [TestMethod]
    public void CreateArtistScheduleWithEmptySpace()
    {
      try
      {
        ScheduleBL common = new ScheduleBL();
        ArtistScheduleDTO lr = new ArtistScheduleDTO();

        lr.ArtistId = 0;
        lr.CityId = 0;
        lr.CountryCode = " ";
        lr.StateCode = " ";
        lr.Status = " ";
        lr.Address = " ";
        lr.AvailableFrom = " ";
        lr.AvailableTo = " ";

        bool check = common.CreateArtistSchedule(lr);
        Assert.AreEqual(false, check);
      }
      catch (Exception ex)
      {
        Assert.Fail("Expected no exception, but got: " + ex.Message);
        throw;
      }
    }
   
    [TestMethod]
    public void CreateArtistScheduleWithInValidStateCode()
    {
      try
      {
        ScheduleBL common = new ScheduleBL();
        ArtistScheduleDTO lr = new ArtistScheduleDTO();

        lr.ArtistId = 10069;
        lr.CityId = 10443;
        lr.CountryCode = "US";
        lr.StateCode = "abc";
        lr.Status = "BUSY";
        lr.Address = "baner teerth technospace";
        lr.AvailableFrom = "12/1/2019";
        lr.AvailableTo = "12/2/2019";

        bool check = common.CreateArtistSchedule(lr);
        Assert.AreEqual(false, check);
      }
      catch (Exception ex)
      {
        Assert.Fail("Expected no exception, but got: " + ex.Message);
        throw;
      }
    }
    [TestMethod]
    public void CreateArtistScheduleWithInValidCountryCode()
    {
      try
      {
        ScheduleBL common = new ScheduleBL();
        ArtistScheduleDTO lr = new ArtistScheduleDTO();

        lr.ArtistId = 10069;
        lr.CityId = 10443;
        lr.CountryCode = "abc";
        lr.StateCode = "AZ";
        lr.Status = "BUSY";
        lr.Address = "baner teerth technospace";
        lr.AvailableFrom = "12/1/2019";
        lr.AvailableTo = "12/2/2019";

        bool check = common.CreateArtistSchedule(lr);
        Assert.AreEqual(false, check);
      }
      catch (Exception ex)
      {
        Assert.Fail("Expected no exception, but got: " + ex.Message);
        throw;
      }
    }

    [TestMethod]
    public void GetArtistSchedulesWithValidId()
    {
      ScheduleBL account = new ScheduleBL();

      List<ArtistScheduleDTO> artistscheduledEventsDTO = account.GetArtistSchedules();
      try
      {
        Assert.AreEqual(true, artistscheduledEventsDTO.Count>0);
      }
      catch (Exception ex)
      {
        Assert.Fail("Expected no exception, but got: " + ex.Message);
        throw;
      }
    }

    //[TestMethod]
    //public void GetArtistScheduleWithValidArtistId()
    //{
    //  try
    //  {
    //    ScheduleBL common = new ScheduleBL();
       
    //    List<ArtistScheduleDTO> data = common.GetArtistScheduleByArtistId(20809);

    //    Assert.AreEqual(true, data.Count > 0);
    //  }
    //  catch (Exception ex)
    //  {
    //    Assert.Fail("Expected no exception, but got: " + ex.Message);
    //    throw;
    //  }
    //}

    [TestMethod]
    public void GetArtistScheduleWithInInvalidArtistId()
    {
      try
      {
        ScheduleBL common = new ScheduleBL();
        List<ArtistScheduleDTO> data = common.GetArtistScheduleByArtistId(0);
        Assert.AreEqual(0, data.Count);
      }
      catch (Exception ex)
      {
        Assert.Fail("Expected no exception, but got: " + ex.Message);
        throw;
      }

    }


    
    [TestMethod]
    public void GetScheduleWithInInvalidScheduleId()
    {
      try
      {
        ScheduleBL common = new ScheduleBL();
        ArtistScheduleDTO data = common.GetScheduleByScheduleId(0);
        Assert.AreEqual(0, data.ScheduleId);
      }
      catch (Exception ex)
      {
        Assert.Fail("Expected no exception, but got: " + ex.Message);
        throw;
      }

    }

    [TestMethod]
    public void DeleteScheduleWithInInvalidScheduleId()
    {
      try
      {
        ScheduleBL common = new ScheduleBL();
        bool data = common.DeleteScheduleByScheduleId(0);
        Assert.AreEqual(false, data);
      }
      catch (Exception ex)
      {
        Assert.Fail("Expected no exception, but got: " + ex.Message);
        throw;
      }

    }
    [TestMethod]
    public void DeleteScheduleWithDeletedScheduleId()
    {
      try
      {
        ScheduleBL common = new ScheduleBL();
        bool data = common.DeleteScheduleByScheduleId(325);
        Assert.AreEqual(false, data);
      }
      catch (Exception ex)
      {
        Assert.Fail("Expected no exception, but got: " + ex.Message);
        throw;
      }

    }
  }
}

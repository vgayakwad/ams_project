﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AMS.BL;
using AMS.Models.Models;

namespace AMS.UnitTest
{
    [TestClass]
    public class Common_UnitTest
    {   /// <summary>
        /// get state list by country id
        /// given valid country id=77
        /// if count greater than 0 then match
        /// </summary>
        [TestMethod]
        public void GetStateByValidCountryCode()
        {
            CommonBL Common = new CommonBL();
            ICommon com = new CommonBL();
            try
            {
                List<StateDTO> stateDTO = com.GetStateByCountryCode("IN");

                Assert.AreEqual(true, stateDTO.Count > 0);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
                throw;
            }
        }

        /// <summary>
        /// get state list by incorrect country id
        /// given incorrect country id
        /// </summary>

        [TestMethod]
        public void GetStateByIncorrectCountryCode()
        {
            CommonBL Common = new CommonBL();
            try
            {
                List<StateDTO> stateDTO = Common.GetStateByCountryCode("PR");

                Assert.AreEqual(0, stateDTO.Count);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
                throw;
            }
        }

        /// <summary>
        /// get empty state by giving null value 
        /// </summary>
        [TestMethod]
        public void EmptyState()
        {
            try
            {
                CommonBL Common = new CommonBL();
                List<StateDTO> stateDTO = Common.GetStateByCountryCode("");
                Assert.AreEqual(0, stateDTO.Count);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
                throw;
            }
        }
        [TestMethod]
        public void EmptyWithSpaceState()
        {
            try
            {
                CommonBL Common = new CommonBL();
                List<StateDTO> stateDTO = Common.GetStateByCountryCode(" ");
                Assert.AreEqual(0, stateDTO.Count);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
                throw;
            }
        }

        [TestMethod]
        public void GetCityByValidStateCode()
        {
            CommonBL Common = new CommonBL();
            try
            {
                List<CityDTO> cityDTO = Common.GetCityByStateCode("Bihar");
                Assert.AreEqual(true, cityDTO.Count > 0);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
                throw;
            }
        }

        [TestMethod]
        public void GetCityByIncorrectStateCode()
        {
            CommonBL Common = new CommonBL();
            List<CityDTO> cityDTO = Common.GetCityByStateCode("Xyz");
            try
            {
                Assert.AreEqual(0, cityDTO.Count );
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
                throw;
            }

        }

        [TestMethod]
        public void EmptyCity()
        {
            try
            {
                CommonBL Common = new CommonBL();
                List<CityDTO> cityDTO = Common.GetCityByStateCode("");
                Assert.AreEqual(0, cityDTO.Count);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
                throw;
            }
        }
        [TestMethod]
        public void EmptyWithSpaceCity()
        {
            try
            {
                CommonBL Common = new CommonBL();
                List<CityDTO> cityDTO = Common.GetCityByStateCode(" ");
                Assert.AreEqual(0, cityDTO.Count);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
                throw;
            }
        }


        /// <summary>
        /// getting the dropdown list of all countries
        /// if count>0 data exists and or else its failed
        /// </summary>
        [TestMethod]
        public void GetCountryById()
        {
            CommonBL common = new CommonBL();

            List<CountryDTO> countryDTO = common.GetAllCountries();
            try
            {
                Assert.AreEqual(true, countryDTO.Count > 0);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
                throw;
            }

        }
        [TestMethod]
        public void GetArtistCategoryByID()
        {
            CommonBL common = new CommonBL();
            List<ArtistCategoryDTO> artistCategoryDTOs = common.GetAllArtistCategory();
            try
            {
                Assert.AreEqual(true, artistCategoryDTOs.Count > 0);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
                throw;
            }
        }
        [TestMethod]
        public void GetTimeZoneByCountryCode()
        {
            CommonBL common = new CommonBL();
            string countryCode = "US";
            List<TimeZoneDTO> desZones = common.GetTimeZoneByCountryCode(countryCode);
            try
            {
                Assert.AreEqual(true, desZones.Count > 0);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
                throw;
            }
        }

        [TestMethod]
        public void GetTimeZoneByCountryCode_InCorrect()
        {
            CommonBL common = new CommonBL();
            string countryCode = "00";
            List<TimeZoneDTO> desZones = common.GetTimeZoneByCountryCode(countryCode);
            try
            {
                Assert.AreEqual(0, desZones.Count);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
                throw;
            }
        }
    }
}

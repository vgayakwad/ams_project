using System;
using AMS.BL;
using AMS.Models;
using AMS.Models.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AMS.UnitTest
{
    [TestClass]
    public class Account_UnitTest 
    {     

        [TestMethod]
        public void GetCategoryById()
        {
            AccountBL account = new AccountBL();
            EventCategoryDTO categoryDTO = account.GetCategory(4);
            Assert.AreEqual("tech", categoryDTO.EventCategoryName.ToLower());
            Console.WriteLine(categoryDTO.EventCategoryName.ToLower());
         
        }
        [TestMethod]
        public void GetCategory()
        {
            AccountBL account = new AccountBL();
            EventCategoryDTO categoryDTO = account.GetCategory(1);
            Assert.AreEqual("marathons", categoryDTO.EventCategoryName.ToLower());
            Console.WriteLine(categoryDTO.EventCategoryName.ToLower());
        }
        [TestMethod]
        public void GetCategoryId()
        {
            AccountBL account = new AccountBL();
            EventCategoryDTO categoryDTO = account.GetCategory(1);
            Assert.AreEqual(1, categoryDTO.Id);
            Console.WriteLine(categoryDTO.Id);
        }
        [TestMethod]
        public void GetCategoryName()
        {
            AccountBL account = new AccountBL();
            EventCategoryDTO categoryDTO = account.GetCategory(4);
            Assert.AreEqual("tech", categoryDTO.EventCategoryName.ToLower());
            Console.WriteLine(categoryDTO.EventCategoryName.ToLower());
        }

        /// <summary>
        /// Unit Test Login User commentted on 12-02-2020 object is modified
        /// </summary>
        //[TestMethod]
        //public void LoginWithInvalidUsername()
        //{
        //    try
        //    {
        //        AccountBL bl = new AccountBL();
        //        LoginResponse lr = bl.Authentication("adminn@gmail.com", "123",false);
        //        Assert.AreEqual("Error",lr.LoginMessage);
        //    }
        //    catch(Exception ex)
        //    {
        //        Assert.Fail("Expected no exception, but got: " + ex.Message);
        //        throw;
        //    }
        //}

        //[TestMethod]
        //public void LoginWithInvalidPassword()
        //{
        //    try
        //    {
        //        AccountBL bl = new AccountBL();
        //        LoginResponse lr = bl.Authentication("a@gmail.com", "123456", false);
        //        Assert.AreEqual("Error", lr.LoginMessage);
        //    }
        //    catch(Exception ex)
        //    {
        //        Assert.Fail("Expected no exception, but got: " + ex.Message);
        //        throw;
        //    }
        //}

        //[TestMethod]
        //public void LoginWithValidCredentials()
        //{
        //    try
        //    {
        //        AccountBL bl = new AccountBL();
        //        LoginResponse lr = bl.Authentication("sri004@gmail.com", "123", false);
        //        Assert.AreEqual("Success", lr.LoginMessage);
        //    }
        //    catch (Exception ex)
        //    {
        //        Assert.Fail("Expected no exception, but got: " + ex.Message);
        //        throw;
        //    }
        //}

        //[TestMethod]
        //public void LoginWithInvalidCredentials()
        //{
        //    try
        //    {
        //        AccountBL bl = new AccountBL();
        //        LoginResponse lr = bl.Authentication("adminnn@gmail.com", "122353", false);
        //        Assert.AreEqual("Error", lr.LoginMessage);
        //    }
        //    catch(Exception ex)
        //    {
        //        Assert.Fail("Expected no exception, but got: " + ex.Message);
        //        throw;
        //    }
        //}

        //[TestMethod]
        //public void LoginWithCredentialsEmpty()
        //{
        //    try
        //    {
        //        AccountBL bl = new AccountBL();
        //        LoginResponse lr = bl.Authentication("","", false);
        //        Assert.AreEqual("Error", lr.LoginMessage);
        //    }
        //    catch (Exception ex)
        //    {
        //        Assert.Fail("Expected no exception, but got: " + ex.Message);
        //        throw;
        //    }
        //}

        //[TestMethod]
        //public void LoginWithCredentialsWithSpace()
        //{
        //    try
        //    {
        //        AccountBL bl = new AccountBL();
        //        LoginResponse lr = bl.Authentication(" "," ", false);
        //        Assert.AreEqual("Error", lr.LoginMessage);
        //    }
        //    catch(Exception ex)
        //    {
        //        Assert.Fail("Expected no exception, but got: " + ex.Message);
        //        throw;
        //    }
        //}

        //[TestMethod]
        //public void LoginWithEndSpace()
        //{
        //    try
        //    {
        //        AccountBL bl = new AccountBL();
        //        LoginResponse lr = bl.Authentication("sri004@gmail.com ", "123 ", false);
        //        Assert.AreEqual("Success", lr.LoginMessage);
        //    }
        //    catch(Exception ex)
        //    {
        //        Assert.Fail("Expected no exception, but got: " + ex.Message);
        //        throw;
        //    }
        //}

        //[TestMethod]
        //public void LoginWithStartSpace()
        //{
        //    try
        //    {
        //        AccountBL bl = new AccountBL();
        //        LoginResponse lr = bl.Authentication(" sri004@gmail.com", " 123", false);
        //        Assert.AreEqual("Success", lr.LoginMessage);
        //    }
        //    catch(Exception ex)
        //    {
        //        Assert.Fail("Expected no exception, but got: " + ex.Message);
        //        throw;
        //    }
        //}

        /// <summary>
        /// Unit Test for registration of User
        /// </summary>
        [TestMethod]
        public void RegisterWithEmptyCredentials()
        {
            try
            {
                AccountBL bl = new AccountBL();
                RegisterUserDTO lr = new RegisterUserDTO();
                lr.Email = "";
                lr.Password = "";
                lr.FirstName = "";
                bool check = bl.Register(lr);
                Assert.AreEqual(false, check);
            }
            catch(Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
                throw;
            }
        }

        [TestMethod]
        public void RegisterWithEmptySpace()
        {
            try
            {
                AccountBL bl = new AccountBL();
                RegisterUserDTO lr = new RegisterUserDTO();
                lr.Email = " ";
                lr.Password = " ";
                lr.FirstName = " ";
                bool check = bl.Register(lr);
                Assert.AreEqual(false, check);
            }
            catch(Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
                throw;
            }
        }

        [TestMethod]
        public void DuplicateEmail()
        {
            try
            {
                AccountBL bl = new AccountBL();
                RegisterUserDTO lr = new RegisterUserDTO();
                lr.Email = "a@gmail.com";
                lr.Password = "123";
                lr.FirstName = "ABC";
                bool check = bl.Register(lr);
                Assert.AreEqual(false, check);

            }
            catch(Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
                throw;
            }
        }

        [TestMethod]
        public void ValidRegistration()
        {
            try
            {
                AccountBL bl = new AccountBL();
                RegisterUserDTO lr = new RegisterUserDTO();
                
                string data = DateTime.Now.Ticks.ToString();
                lr.Email = "sonora@gmail.com" + data ;
                lr.Password = "1234";
                lr.FirstName = "  Sonora  ";
                lr.UserType = "Artist";
                bool check = bl.Register(lr);
                Assert.AreEqual(true, check);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
                throw;
            }
        }

        /// <summary>
        ///   Test cases for updating profile of Artist
        ///// </summary>
        //[TestMethod]
        //public void ValidUserData()
        //{
        //    try
        //    {
        //        AccountBL bl = new AccountBL();
        //        UserProfileDTO r1 = new UserProfileDTO();

        //        r1.Id = 1;
        //        r1.UserType = "Artist";
        //        r1.ArtistCategoryId =new int[1];
        //        r1.BirthDate = "07/03/2000";
        //        r1.FirstName = "Alka";
        //        r1.LastName = "Chaudhary";
        //        r1.CountryCode = "US";
        //        r1.StateCode = "IL";
        //        r1.CityId = 14809;
        //        r1.PhoneNumber = "+919934";
        //        r1.Description = "I am an artist";
        //        r1.ProfileImage = "/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUTExMVFhUXGRgXGRcXGBUYGxgYGRcYFxgYHRcYHSggGBolHRcYITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGxAQGy0lICYtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAMIBAwMBEQACEQEDEQH/xAAcAAACAwEBAQEAAAAAAAAAAAAFBgMEBwIBCAD/xABBEAABAwIEBAQDBgQFAwQDAAABAgMRAAQFEiExBkFRYRMicYEHMpEUQqGxwdEjUuHwFTNysvFDYpIWJDSCosLS/8QAGwEAAgMBAQEAAAAAAAAAAAAAAwQBAgUGAAf/xAA2EQACAgEEAAUCBQIHAAIDAAABAgADEQQSITEFEyJBUWFxFDKBkaHB0QYjM0Kx4fAV8SQ0cv/aAAwDAQACEQMRAD8AdnHvNW6FwsxE7hCzcrOtPM06hxDNsaWMOJeRVJMkr09PJr09PCa9PTnNQbm2iSJEtVY194hAJGU0Bb3AwJecqFK2seSTJEH3/wAtM+HXMxnrBxAjgkxWjc+1SYuo3HEIW9oANBXH63VFmmmiisYEstsxWcbSeJLPmW26e05CjmLtJkrFaSXpiUIM5UaDYwZuJInWaiixVXmRiDsUxdtkeY6nYDc/sKZ0Ghv1zE1cKO2P9PmNafSPcfT18xIxLjR8qzNnImNEwk69yRJ0mutp8C0qJhxuPySR/AM2K/DaVXDDJk1jx28TBSlWsRsY5nT0rz+CaYjCZH65/wCZR/DKSOMiXrzjtlKdvNzEzH0rIb/Cpd/Vb6foOf8AnEAnhhB5biLPEOKpuEyYREwoa9JBB33Fbeg8Ir0VZVGJ++I6ujC9GIl7aqTkJOZCjooc+cEcjTbKRiA1BasZMpXoSnzJ+WY9DQ9wMUr1AedYXeFLiVgAkGQCJntFVK7hgw4AcEEx34u4TdcZbuGWinyypqNddZAHrtSI1Lo/l2H7H2P/AHMlrnBKls46PzM0W0QrKfKe/wDelOo4JwYSvUnOG4mkcGfD925ZDjkpaUZMGCpCR8o/1GPYUaxwMIT941frAieWDzAHEVq8w6UotkobTOgQFD1KiJJ7zWg1IGNoBEsGYY28x9+Ddw3cFchKXG48o/lP3hPfl+9I3uEXCiL6u0lJqL2IMIJSpxCVDcFQBHPakNxmaEcjIBiG87rXQuPROfr7hTDnKyLO5q19Q/aqoJhhCDZqpkySaienhNenpyo1V3CjmSBmRFVZeq1aEYEIFxIyaxWbLZl5+K6hrgonsSJa6Rew2HAhAIKxF3lXReH6fy0yYK1vaDG3IWKnVgmsgSlRw4hhtyuL1CHdNIjMlDgpXkHqDKmS5qP5nHErichdA8wgycTtC6aovyeZUiVcVxJtlBUtQTvEncxMVo6bS2ay4VoOMjJ+BCUUtY2AJleM4wlxRK1qJOh0j2HQfvX0ypKtPWK6xgD2nRALUu1RxBjq0rGVBObuRHUUbeDL74Mv75TSAiIURJPMzBIB6bbV4vgQb2YGYEdxAzM0E2xZtRiTHEVKQlMmNvr/AMVJsJGIQXblEOYNinhOtrUM4SR5IBBERGu1EPIhLq1tQo3vHH4kYZZGzizSnxiUrATA0UQVSToDBmCZpT/NcciYFem1JBO3qS/DHALW2Z8W4dZU84f5kqyAfc7HrSL/AIhm4Q4Eg06luCpj5iV4AgKRDk+VtKYOZUHaOkGsZtWzXstgwoHORzFHrKjBHPxMN4jbeVcrDwbUZgpSkTPfpG3Wn6tVtTrH3iy3MpxjP0MZbPDLtpITbPrQ2dQkKcMdgnNFLr4ozdV5+0ZNm3kKIwXloRaqJFypcGfEVoT7AD8K3PCtW1z+sEfSMaS8s4BH8TLODeIzZXweyE/OlSQRsoEactDB9qb1A3krjEO6eZlOpWv8VU84p105lrMqJ5mqqQowJq1uiKFXgTZnLSTT9tvpxPntVXMJ2LMVmMeZoKMCG7UUIwohBs1UyZJNenp4a9PSN2snxBmA4hUlNTlcdZqmDRgLPUuVevU5kFZytVButMkCROuQK0/DdPvOTPOdolBy0J1M10jWoowIkxMoXOHnkaVssDdSuZXN042NRIrFv0ysYzXqmXgzlOPpG4NLnw4nqMDWIexJv/UTcbmqDw1578TVCFld5xMET1pS/ShDiHX1LnEiv8eaaSVZkqIIGUKEkkx/X2pzQ+Cam25VdSqn/cRCJpmYgEYB95lXGuOqfeKjISkQkTt19yf0ru9HoU0VXlqc/J+ZoeWKFCiLdxezzp1mzLvYCZOy9pINSpl1PEu4lL7CtBnBlMaeoorDKy1yZr47ESXCZ1ERSpmIzHPMI2TRMDrrRkHE0aRhRmEVJKYCSZjlvP8AelWYn2hmBhFDy0tpCgobgzOs+vPWfftRFyFGYxW2FAM5sL4pKhqOcd9B+R/CvKeZKPzidXWOONEhK1BGaMoJ0jYxyMb0s2BYSR9Jlm7bedwzmXbPFZPLUySdTERudTTICN2BNEVVNztH7CHcI4gKCHANupOvWIIj1pb/AOO04JKDb9uv2i1vhlD8qMfb+0ZeJ+IG3bMlhxKVlJOVRSDG0id/Wp0qiiwhv0Mx1qOmu22foZh9qyCpZzhSgCTBmOpo/Jy3cPWy7s55lQmgwu6fTSmaJY85dEkzKKWJhwJeZNVlpbQqonpKFV6TPQa9PThxVLaikWLLKcSo4K4/X6Ha2Y0pyJGayWr2dS04WqiUVGwy4E9tFCZroFU0oBF2O4zq8fApW25/aUKjEEv3sVWm12PIgWAEpOKz8qPYGMoIJu7DWr1McSpGILvE+EMxEnkO/etnQ0C1st0I94fp1usy/Q7lBXFtwkkFWhBBBAEA806aGnX8N0pZT5Y9JyP+/n9Z0T1IMemCsNel4kkaAmD6aRWjWdzwytubMA4+uFmNqrdwYDWmB1HWgmJlvVCFkDFXWNVtCTDkSeQBpgHiObvSTAbLX2iESAsSQTPmA1yTG55T+tAzuwJlPi1Rxzn+ISs8gKpPygbb+nv+lNlRux8R9SrOQD1Ol4rAITCR0H771PmKvUNvRZV+3qCpSvXr0qjW/EG9wPAMntrsK8q5Ur7q+c769RXlYHuQhGe+YNvnCS6DvmJ/HWlLO5j6w+vMsWD2mpOo/T1050RDNLT2EqITt7iAB7+/9KYDcR1W4kGOp8dppBEw4POfuoghRJOgEkadhVXXfgY95m+JKHCDHv8Ax/7E0Hgb4f2KXP8A5KHl5dUJWkkSClWgMxBojXpSjKi9+5mLY2wEKuIwYD8O7RthKH2krcSVgrI1UPEVlP8A45ay2sOYKy92bIjShmatfcEEElcspYFZZ1fMNsE/FMU3TdulGWdJXTGZWd569InviV6enCl1BnpEa53xbKjiM1dSFxVcZZazNiHUSutyK6Hwuj3M9a21ZXQTXQGlX7iGTIsRdDbanFHRI+p5D61KaJbHCQlNbW2BB7wRhl2HmfFJjKVBXaNfyg1NuiVL/LQd9S+o05W7YPfqD08QkmEpAHfWtQeG6fGGGZpJ4bUBzkw7Zr8VoLIiSfwMVgayqum4pX0MTJ1VS12lVlDGMKLrZCRKtxRtHqlqf1dGG0Noqty3R4Mz7E2d21iFp015HpW9uDLkHInRMeOOR7QPh6yHBO8EfhU1/mEpSfUJSxtcn3olwzI1YyOJSBkkdIFAxzExy0IttmBVwI0krYndQkoB1Pzdu1STxiRdaNu2CrV2FAEwCpInoCRrQGYqpxM3z2rVh7xg4qs0W1w4hs6LhwjoTOn6+9B8N1Zv04c9+8t4fbtqJ9yeYvLuZpovCNqMmetr+tQJZGlhhwzvV1MZrc5nWJL85VvIk+4qLe4nr+GndltUp1G9OcpmEWyNzAnmSBPYTvRxHQwHcr3YccVkGiBGpPlJPOee/KvFscHqIahnd9oHELcPNtWr7L5U4VIWFSggCBuNRroeo6c6k2IB1mCfT+juasv4ntz5LZSk8lFYST7ZTH1pD8Ix53QC+FkjJYR1aECsjW38xIDEkJrH86exInFVqaKwkySvEizVrbxBFDPPEq26VxP3i1OZE5LlVY8T06bcrA8QO4YhajziRuisSnw4u+cRsMAIOcUSa6SmgVLiJ2PuM9bVT9YBGZUCDuLV/wDtHNxqn/cKNphi79DHPDx/nD7GJ/DNxn8e2mPGbOX/AFpBP4ifpV9blGS4D8p5+xjer9DLZ8QTY3Okn3+lP5j4IxHhWJi3tWyrfIDHcif1rndPozrNVYxOFBmRTpDqr2PtnmKj/wARnUyEJb9SCSPxitY+FaUHnP7zU/8AjdMvef3gC44iL61F4ZlK+8kAH8NKfrqqVfLUYAjShANi9Sve2ZSkPtypoHVQB8p6KHL1qllJTkdRRxsaDrhhTiwhIkrUAn3qSu6Ta2RmD8VQtlamwD5SQTrqRvQrlKMQIjaXX8olNN0tMakUHcRB+e69iF8Awv7W8G8wBUDMmPcGDr2ogdMeoybCCNwMv8R/Di7twFIHjoOktg5h/qRy9QSKAXQ9GJNl+B3GHh/4aP3LSrm7UQ6tIDbRlMBKQkFyNRoPlGvU6xVtElKkKfy/SDqba2D1ELE8CuWVKK2HEoBIzZSE05bp3DHYOPpG9jgnHIg1LlKZnlt55jDw5hbl04ltpMqP0AG5J5Cipj3mlXagTex4lnjThh61uUNFJV4qUeHlBOYkBKkgfzZ507p61V8FjjqZ9lguYkfP8QpfcE3Vs2j+HmdPmUB5ktCNjyUsnkJAj6OU0llysbrtyuK4sXNjcpUVONrHKVA9JiTuY1iqNRbuPEr/AJu6c2d0oHQkcoE6+1DViDGKbTnmEHFK2gweURr6VVkMs4PtOPth6D8KqDIW3ifTwVXC6y4hyDMfE4Wuk1fcZYLIFrrd0hwOITbPwNNvYynBkbRI1mjV3ZgWSQ+JTYbMARPPFqG6lZ2lykmo3HqenqlzTVWmVOZfkyPwsxihan08yMcwrbWYA2pBLXbqX6lfGsLDzLjegzJIB6HkfrFE09lgvX7wtNmxwwmIWTire7bJBBbcTmHSFQr8JrorKw6lTNa9fMQ4952toJuHULUEoDrhkmJSCSMvXQjahlm8vNYz8QYaw1+gZPE64qxht5SfDUoJiBsARttyq+gqNFIRu8knH1jmhrNFW1u8wBgTrYuUh6C2owdJjpp0p2pgH55hi2GxNywzgeyhLrbaQSNxt9KC+qapiMATGs8QurcriF2cHt2wpKUJGb50wNRtMc6GdQ79mKWamyz8xgO24BtmbhNykaIByo5AmYPsCYFV/Ej8vUu2sdk2H9525h1s64EltBJlR0Ggnc+pox1TAfM8uptUdxD+MvD7fhJdaQAUHU6AZf8AmvWZsp3HsRhC19JBPI5ma8Hl1V00lkDxCry5pCToZkjWIms8kAcxdH4IM3HhxNyy0pF2tKnipakAEqCW9IGciVRO5123rG8Q1K1jcvA/rKJXv6E4xrin7K/bJUFKbfJSSgZ1hYAypCRyM6nfSmdFetqnB6g3TEOcV3AFk6pWVMoMFyAASNJnY1qaK0i3uX0o/wA0T5YB8x9ahjkmV4LGbDwhjScPtrNDrKw48oqACAfFacXGYEa50SkwdcpMb6VGWzgzzZY4mq3Fs1mQ4tKFOtBRQogSjMAFFJOxOXftSY1APA95TBAIgnA+Kra7Urw1oyBWQEkZnFAalDfzZP8AuIkwdI1LmXQSuCIJ+KjwRaqKAgKIiVAmAd4AG9aWjd/LY5mjod2GOZg6Xsg8vzHdXPXkOlK7sdQqvsHHchQ4SdJqy5PUqLWJzLSb0xyPqAfxNEDQwuE+rVjWuB8S0rtqtqiZwPEheMaVq1+Any93vCLIkI1p/wAK0hrBZ+/aXJ4ky01HioB2/MGDIHRSCAiS3UrKFaNbcRZhOFCjQc8CquoEkCdhVGAhgktWR81ZfiTBVniss45fKZtnHEAFSUyJ26UDQbLCqH3M9TX5loUzN7T4gXPiR5XP+zKBPoRBmujHhtDEFRgj6zRfR1/7RBHFtn4rf29pBKDIeQZlCxpJj21/rVrxs+khHwAhPPt9REO6vVLJUSSSZ1npAHsNKXz8RzcMcSFp7PKSfQ9J5VZWzxPK27Inti/4LqVEbEGemtGqfy2yRPI3ltzPojgbiNm4ZGVYJ58iD0ip1Kb/AFp1M3xCg7vMXqVPiNZXDrKTZvEPIWFBI3PUBQEj3060vX6fzDAidJwfUISbxJfhthSSlYSM6VESDGxyyJrlfEdZZTbjv3/SM10BwWEVrbh9xGKquxnUy40MyQsylU6eXYo305Geta+hveysZ7PUBb6WIBkXxfv0CyylOqiAK2VUpSxaM6P0q7H4mZ/DV37Pcs3K0/wlOBjMRoFOAwQeoIHtNZtykocAxYcL95oab5FriN2q5AQHS1kdAUWgAk/wyvKAle6jJiCK53xGq3U1J5fOM5Hv94zp8LkQjiFzbqaQ64lK0KuEJYUJCkqRJU4OY1kabj1pzwqg0UHd32f7QWobL/aV/iTiM4erwpWFaFQ80esDnW54fsIZlPOOp7SkAk/TiZFwjgiHHkKuT4bAMqKgRmjXLETB5mpDLySZROAWM17E2rAtov21KV4H+UtbjoRmKsqEgOfKnPG0D2pO6xmVkHR74+nt9YNHO7mXscvVNtKXn1UUNkqyiErWlKiM0AlKSpUHpXMaFns1AVicf26+3M0bq1RCQJBwrworCmlEONrSSVOL8JQcMDRIPiHQRtHOuzqQXNgzOrUWNtmY8f8AG67tZbbKfCnQgEE+oUPL7Udn2jYnUea0VL5dX6n3iQ+rb2oEVdiJqHwm4VDyS46gEK+U6EEEEK9+1a1B8mnd7mNeYaa8+5h6++DrSnFKQ4pKSZCdNJ5DtQxZQeSIEX1Hkiad9p8+WKS/Dow8z3gfaXITzpVtWqttzKer2la6hJp2o7hkQ1eWkS1Vl66hi24y3Urr1pFmCDEkcz8GqlLCZVlE5Vbmm0ZoFgJAq2UOVNKZAIEjWlQ5UTdiGVxLmHp51zni92eBPZzPOJElVq6mJlB09NaR8M1GzV1BjgZhtLgXKT8zCrlamXkuAgZVT5jE/v7A19FDbGBmzegB4mkY9j9s9hjqmXGyuG/EbSRMFxCZKd+e9JWP/nD3HtM6lGXVDdyOcftMZv2oOn0obD4j1qfEqoEVAgVG0zpRnQmO9WzPPzHv4VMLQtx9RHgNjzqnQHcfgDTNNgUFSe4Kx8VlD2epoHB/Gxv3rhDTeVpsAJWfvGTJ7DSlNQ6bCR7fzM1qlC5zzmKttxI8xeXTKre5fCnc7a0pnyqCU84GWUqg9q5rXaEajFgYDjkRrT2Y9BB+ku33HRbtLa7Q2crj628qtFeGMySOxzJkdq2PD6TTQFPJH9YvcBYxI+kB8c42zf2uZtKlFlWZaRAMbHflruK07NSmNjgyA3kgo4lJziazdww27SQhachQjmFpUCFTz9e5ojtW9HHBHtJDh1OIz8L/ABKtLthxq9SlpSUHNpKVg+U5eeaOXeues0WGD1mQGBGV/aZr8QeMjfXOdrMhlEBtMxqPv6bEwPSBTtVQrXbKFpxYcVLUC3coKkK3UjyrBGyu5nXlQTpQrb6+DBCs5yvct3t+pxxSkr8ROXMIBBOmxSRorTbvRtPQFTZjEaSshCpEvW1wP8IdbU62Ltx0KyuKALaGgCkJEEDROUTHzmjPWxIHtz/b/uQdPYVGB9ZRxO4ulNqYcu0PoJlRCcw8pbKSlRAzD+H+Kt5penQ1hhYF2n/7/vG/wtzrhj/7iMfFnGYubdtpkus5SQVFasw08sLQZUORCtwOsKGhWAFwe/mVXQlc/PtM0vbdxOq5M/e3n360MqfeL202L+aU1a+1VxFjzxN/+CTk2kEQQTHfvT9xYadSYxqidiE/Ec7zidtpam1tvZk75WlrSeYIUkQZBBpIV55Bie2E8PuEK10mjXVsOIaxGEjvbttKvmE1kW+Fu7bwJevOOZwtYVFa9NXlJthVG0SBa5MVk+IanD7BKnky0wzNZyLvOZ4tiXAwK0ErAgC88LQowGIMtPW2wdDV1b2lczx20FVtbC5kgyopITtXFeI6z14MaReIG4nxr7PbuOCM4HlHVR025xv7VfwbRnXXjI9C8t/b9Y5p6N9gB6958241cEuqUCSFHczXe2H1S2ssK2enqeFp5kIcUhSUmQlRBAJG49a8QycmD3204LdSX7ZngmpzmNrqN8jJqDJLSVtG30r0soh9jjJCbdNmUKS2lZU4W3AkO/zBScsrOgHzculBAIctn7fSZruDaWz1CPwyxtNtcqQSAxcaSfunlr+FXI4InkXdlP1H9pr3+P2iHUtqcSFrUEpGmwGnt+9Z50B/5l/wt23P0gLjmxtsg8WA22lTgA0GdUgH8zVgz1qtadk+8UZ2zgdmYM5dIIyHOSScykkJmeWoOYesVpEhvSYdnBTym7+ZPY2zPguKbVmcEGFCFJSFDYbR1I/CqbTnjqAorxZx1gwe+sq0J/sVBEI6ADiVs3SB9PzqIKdWrZUoJFexLVqXbaIXUonyiQOn6nrV/pNbbgYE5Q2B61YDE8qY5lth+N6sDGVYCetOgk17MkMCZJ4sjKdZ/v2qwMq+1htMZLzghpdoxcWygXgRm8pIcQfmSUJ3WnXUakTOsEA1GqRHBVcEfzMC4FLTj2mh/DQAslv7M+yUmFBxJAPdC4GZPcwaat1guUe2IO+42HPxCmI8HLccUsXagDGhQFEQAIzSJ23ies70suoKjAAlRaQMYnKkEbVsg/M2iVMovWJzBZnSieYo7gtyjuF7HEhsdDWXqydp8vkwVhVupZD4Kq4/UB1b1RfcOoVtnBTWnYQLtI7+/CRTpsCjmLs2Ip8Q43chOa3KRGpzCZ7USh2s6EvT6uxKeCcc3bsJVZnMN1pV5fxE/nRbUC8ky1i7Y4tYssplSMvvWe1uW2k8QRc+wg44xmMEQKybfD69QGwOveHpv55ETfiO8TCdk5M09Tm1/IVv/wCG9Iun0rAHktz+wxOh0DKVJEz/AIUwoXVyGlBKhMlKhoRz99eVbSKjE7pd9hDbxnE3K74bt12wt3WwtCQIB1PlGhB3nvWc+pbzPpMRtQxfdMxx34XZEuPWzmZoNeIlO6lKBzaRv5Zj1pjzK264PxDLqK8g4x/7uZmnU/lVYQP6szt5a8vkSf8AXsPYnf1n96jn2lbLnYbUH6wabZQ0II9RFUIxE9hHBhTDXIBQdjqD0NSpyMGHWslcCVn7tSXM0nMk6GTOm1VxKC9wwJPIj+7iTj+GXC3lSstt5SeWVeoHSQaVsuB1KKe8St13mWqcYiTw5hinn28ozDMJHaddfStXT1ksG+IxRSWYWewmj4x8LlB3xLVzIDBA18pJ116URLKGJ9jKB6ySTwZn/GGFm2fWgiNlDpChMDsDI9qBaAOpFjArkRepeLZhjh9MKVMfL+oq6dx/QDFn6SwrRZHrHerHuPOcNIwrlUzwOZ4Otenp+bVXpK8SQK6bmpzJJmiN3SrKxbeUnMEIlKR95bnM9B+lc81x1GtNY6Gf4iGopIy56hH4McQPXb7xfuPNEoa0CY5kDtXSbx+GKgDg/sImwDU5A95o15alS1EWiV6/MpSATymCCf6VnefjgmAH3lBLSo13roC6jmOeZgZlm2Z081ZmssY/l6gWYmQP2yelZukvbzJHK8yulMGeVe8RU2HgcwDnJlxazEg0iPD7x6gZXBIlB14nelheRYFeD9+YPvbmNIntXX1orVDZNZKgU4lFPEa0aBsAetJW6InktF20h7zJrO/eeVBMdh+9YGsYLwsW2eraIxrsMjWglXWgUa3Z6PmM+VgcRb4gYU8xkIBUkn1giCBW7oqvwTk2H0uOD7ZjeiuFTEP0Yp/D3AnkXgcSk+HqJIKSfr/wa16bK9zLuHA694691ahxuH2mncfPvItFLYbcU6iFoUgZilSSJBSNVApzAwIgmY0NZ+xQ/JmIuMwP8O13Is0ru9CZCUHQ5CrMgFP3SMykx/Lk6Updqq1YgH3/AJ+JbYTK9t8OMPbdUoF1ShKsqiktI/lBhA0Eg5SqTAmRNO139HHEv+Ib3xE/GfhE+pRXbvpdSQCFOKkqJmTIERtETTJNTc7sH6wu6tuSSDEPGcOfs3PBfSJ3iQofgdD9DUWIyd9QrsyAE8gw7whgSLklebIlBGZI1UZ2idADB112rO1msXT4GOT18Suo1a0KMLyf2hniT4cKddbVbZUpUIUCSdRsepPWgVa3na/JPWJl+bu9TS+zhyGmV2jx8oSUkgayY29P0pGy8nUizHRHH/vpIa8b8y58N+CVMueMFpcbOgUDoRPTdJ7GurF9AqzW2czUGopFRFZPMcOMMfbsUJWsgJK0J2JiTJBA1EoC4O0is1AScxDMzj4scOuup+2NgqQAkKA1ypT4nn7pk7952oguydh/f+n3hM5TaO5lCE6xz6VeUUHOIUtFhBBJ7EDpzqwGDNGlTWdxMmedSrzIOo1jnVjg9Rp7FcZE/abipEspyMyPPofWvSM8GdtoJO1TiWHcL4Jgzj6ylAAhJUpatEoAEyo8hQ7rRUuTItsWtcmHb3D8WdRbsPpZ8N0pgTBORsPZFEA5SQ2oSBuTyrFqGlrsa1M5H9Tj+sy3vtcbWxAV3ZO4ZfFVu5mUyUkGNQFpCsqhzMHWPwrWqtyob5gVbY2R+0a2/jZegAKYZJ5nzifaa9tT4nt1Xup/f/qbDjCgNRTmlbzR9JFYLNiVW1kimWUdR7ylAnFy6AO9ZFulNdm9eolYGBxKzTg2rUqoGAxHMstXuZM6dIFTc2xCTPWYAlUMRvXz7XHJYiIkcylfWgVsYNC8P/xBfpxtYZEPXqGr4gF7h9TjyDm8g3rqPDPEm1zFW4hk1JfgxqwmwS2o5RrFX/xBXVToiwHOYalRnMPIMiuT0Ye0AqJZhgxYxy2W1KzsTX0WqpdTpRS3YEoWDjbImLtDban1HKltKlqPZI/M7Vyeh09lfiGXPWf2iiJm0SlivGb7OEi+8FIW6oBpK1lQCVGEqIgSYBVGg71sXhHs75+Pj7Ri9VDcS3ZXbTNqHVuggDMpxaplaiFElUD5lLB2HzDQbVxdi236wADBJ/Yf9Y/juPnalePYCCcT4PubnE0Xbjg8BogBKEhakqQSDKFyJza5gk6RoImuv02Er2qc9zLL5bMf7p4MtkqWVlInXLmPskD8BRaaWdxkYEJVU1jAAT5o43xj7VdLclcTACtQB22j0pu5gWwPaNaojIUe0ZfhMwAbh1c+H4YT6rnNp6AH/wAhWRr1RlCt95n6srsAmjYXiiQtInQbSPwrI0zqup3OfbAmeH9oHxd1Crh8rUhIKd1kAA+UbnnvRsPde3lDP2lkVnbAEscH8PO5g83dI8Mcm1lWbTSQPKQJ6mtH8FbV6rRjMN+HsRueJzxzcsBbTFy2Lh1xQS0nwlJhUiVFwkwgSJyg6Toaja9aGwtwO8f/AGYwiFmAh+7xbwGS44UgJCRCQQNwDAEkwNhSXh2os1Fp+P8A3vH7dLjAXkxG+JOINXSG/CWkkTIUgZhIEEZhmTz2610tYOwqw5+YbTaNhkuPtM1ewhfWo8oy1mibvMvHgq88NLyGy4hUwUakR1HKpNDA4i7adkfaGEqosXhmCm1goGZQKSIT/N6VPlsM5EOm5TgysRM9BqakLLMZWC1qMAKJ5ASaEdxMUL2E8CXsPxe4tlnIpbatlA8+xSd6o654YSxsI9LiaBgnxQSP/momDmQptOoMZYyk7wTr3NJXaFT+Q4lLgij4iNieJKffduFSPGWVweSdkj2SAParOAMKPaZNz5bIjbhXDzJZQXQSsjMdY31A9gQPasm3xG1XKr0JIsIHJm1uqzma6+mvyV2zWqXbyZG8sIEmijmELcZMGlZcVtpVU1FTHaDnEXFys0ruWa0q7UXUa6upMy9l4A4k4uQjc1lrf+N9Kxfd5hkN84VJlJoeq8EqdCD3IfT8QDbXTqlZTXIr4Q1tpqrHImaFYtiNdpZFASSZmur8L8KGiJO7Jj1dZXGZcYSAT7Uj/itx+EC/JjlcrYheqbhQ2G9B/wAJhGrZT3CuoxK9zdpuGjJ32rrAtiWf5Y4gFqcn0iBsQwsrs7lpCpKmVj3iR+IrLTR3rqbLLOiOJNdLK5zLLPDtviNtZKfIW2GW9Myk65R5UAEZST8yt4Rl0k0G4WZZl95FwJsJx9YpWfBTjGIFKnCrD2VqcdBXzQ0biSg6FKVqaE90+wUZXIYD1f0ziVdywAB4/rHhrFFAl1AjxFFWUnSND9dd+1LNra9OSw5BJ/jgxJiVYgcxI45wS8xB6WltBERlUtST3BIBBH7VpJ4jXqECp0I2NaPLFYGPmU8I+Fob/i3LqHI/6bZOWe6jqodgBSPiWoenTl6zzx/JgPP+Iz/Z22UpGXIMojKISB7aVn+W4rDtyTzmJs3OTKjzad0HUUI1iwc8/wDMG2D1Mx46u3F3Kkr+VEQBtsDPqa6DQVJVp0VB9T9/ebNKqKVx+v3mxfDG+SuwSlORtQBE6HbTNHOmtZlyp5Ix1DalCSGHxF/i/gtN44XGnVeMkf5jipW6qQB5RCWm0wQAlIkq5blcelcNBHTsq7m4gLHLt9thm2dcK3G0y4okklSlFQknUwkpEmi6ehKxlBia2hrOzexyYrqepjMezLWHqmZqQYN24m58DOA2iJQpMDZW/wDWga4tuBmL4gCLczz/AB2zN19lKmy8RonSVAzKf9Q3y9DQfMs2ZJ4ivqInDPBtg02seCkJK/EVm+7GoEnZIjbvUVa49CXF1g5nXDdjZODxmW2vNmylOWSnMfNpsDvRbtVZuEu2ptOOZjvxesW270lBEqAJEc6YuJYKxjF+WqWw9xStbfxBB050nZZtEQ1No8sA9wtZWgefQ0PlGp/0p1P4CPekLbDXWXMze41XWIwogGs2vSllBlwjHkTYnLhLWit67N7FHJM2LLlEgv7B14AghCe8zHpQbsvWUQ4zF3ZrBgSzZ2IaH61gXGzS8H94v5RSUcUdKjCdKAdSWHqllBbiAsTtlATNP+GaxKn4jCr5fJn6yvJQQTXVtalibhGhhlzOMKt1OuEIHqeVc74e/l6xyRxMtWC2mNFw2tCADvW2jI7EiOKQwgm2vFpUonbpXP8A+INELGVnPpghY1RMhubxTsoiKx676/D1BqkJc9zY6lZtBlLaQTyMAn8q+iaexXoFmfabNdi118kRvssISlIGoKhWbdq+cn2iFuqJMw68xT/D8TcSVrKGXSUIB080L25Dz0uzgZHt/eOV3qK9jng8fvHy/wAdRegM2+rCj4lw5B88kHwR/wCKc3pHWhUaYLmw9ARQotWST9pXvr8hYQB7+tcLWTZXg+xP8zDd/ViQOOKSoqnt60wjtSuUMpYcGWLK5zK9jS2q1Fli+oz1L5bEGjiJPilorMpOWFbe1Oq+pStW7EG1jAyw46mZBA0nTb6VUanzDlhgyQwiTxhhKnVJcTGciI2Co2g8jrWtotdt9D9fMc02q2DY3UVG7x5gqSkqQrZQ1BkcjWyLOOJqLqmRfSY38BcXsWKHFONu3Fw+QkagIAAICZUZMlWumxFU2hjzKeYHX1sfnqVeIL4uOrcP31KP1NGrYFcibelYeSuICcXrUkyWbmX7P/LUakGAsePXwu4qWj+A84Sk/JIkjtmJ27VZq1sXBlLqPNryexGXGeHEqvWsRS+2lbU5gEK/iJggT59CATrHSs6ysNWa+eZmVg7hgQ+HPHStBgpcSQQdd0xXMpdd54Q8c8fTEesoFQz8QZ8P8COH2ZSShSSpThdkgqB2kRpAAG9dPX/+QwxM1V3MFHcxrj7Hftt0rIgAJJSNPMqOpFOWv/t+I7qrQAKh0sX0P5CR00pNxumNa285jHwnc+Gi4dIBlISDzH3jB/8AGs/WAsVrHvBZxIVqJMmZIBPrFPVEBAJqaYjyhPoy/aClIJ+6oH2rUNQfGfbmS1e7EnfuRtNXWs9wipiRvrlJoOopFlbKZDplTKbTYOtc6NC5TmBrO0yvituMh9KJpNDsPMvbYH4iEq4gnWq6nU36Zig6mdZc9RxG/gm7AQSCJk1peEA2UEnvMvpjvGTDz76VEhW9arHyl3e0exgcRZxQhKt6zvGLktoUgxPUN7zl5xKU6HU865eypHwpgVtK9Rn4euEAhAA0G/U12h0/lUKF6AjpU7MwvfqPUJSN1Gue8RXUWYWrge5nqiq8mYPxBwIld448bnxUOLK9U5Vak+WcxkDQTppyrS19n4SpbH5J6H/vaev9ADN2faFcLvPAhlACUggbcqRbx25qdoUDiA/HMy7SBCl0xrmnSuYoGeDFWXnMFLuApfhlQAgmTTi1bhzBuMnEJYVahIzTJik7lYoTLVIBzA+PYUhQJCfOdQrYg+tPaZ2VQc8fEqwED2VouP4i4HOd4o9xUH0iUxJsUfCkIKZhJGtQi4OJBlTFLNlcuOCFGBObcDme/etDRDUEek+mamk099i8dQfh1rbJJVKSrlO/saetruI7hbtHeBJMV8NafIFAgTqZHp60XTttG0xvwuwrmtosrOtMGPu3MvXGjFVJ4iljwczdkbEipDQ1eowJdRjDsR4i46Zj+9WD4jCajHMbeCOKnEKLS3IScygtRkoypJgTvJAHvSd2mVrVuA5Ht8ybW3oeMwrYY+64w5bBZCyknLOipG3aka7LNO5KdE/tOZF71Px2IlvrbaBCAnxDuRyphnZu4B7GfuAPB1k0TdPZh9YDbCEc1EE++v7UiDvtLfEHJisVAJkhyJ9Gqs88axXSG7yxNk3beBKNxhoSlbgJJ/aq6fxEWqSehmStpB5g9jEZBkRy1oGr8RoQYrOcyz3IRhZRvMaybGpp1tDLzFDYogvEOKApOWa8rKWyIMPkxeUQZPWs/wAQCu0W1TBjLHDeMIt7j+KqGz9Joeiv8jI9jB0vsMK4vxtb5itBPYU9frVevYI3+LXETn+JFuqKjz27VhNSfcxViX5Mjcx1yImoXTgHMGciOfC2IrcQk5ykp5jnXV6TxGkafbb2JqafVoKsMOYw4njilpCIOUfjWLqPEkDZC+mKNeC+cRavlE6gGaw9b4i2tu3vwOgPiUttNhyYFCF55UkjUa+9VyoXiCEO3JOVZmP7FJ0qWfCjJ+kuiM5woyYtXl2ylRJJXtOXYe/7TXSafwx2TL+n/madXg1zjc+F+/f7Q9gOPNOkNRkJECSNTyArP8R8KemkunqnrvDbKVyOR9Pb7w5jkeE2hKNRAJjmOfvS9DrqlWqger4PEz3TgCKmK4YoyqCBA30mRMDqaPVRYmd46izDmUxhSgwVPEoA66qPQBP7xUm1d4A5kheMmKnEC1Z9diJFdJQ6tWNs67TWq1I2SjgmGLuHQ2gwTz6UxVXvODJRN5OTxGi+4KvLZQJSXEHmnceo6VRql3emKFF35QxYxbDnGledJSDtIqzKRL2uTzIrp/8AhRQSYoX4gls16QplhFSI3XL9nuKusfrEJ3a1I/iIVlIMzznkKz7VG7E5rWpttMopsXXJeyjzkq8ugkmTpyqMgDEVyJOxYkrSCI119OdUd9qkyCZHjD2Z5Cen6n+lUoXFZMgdT8+/5jXlXiVE+pVviCAIPI1p6rT2WVMEPPtnqbIr5zK7NuotuZpjKY9YrnfAadVVY4uBAHHP8ydQ68YiHdKKRrU2AcgTLckRRxG7VOsxQqq8GL5OcmCL27SDNbVFmBDI2IRtbqU0G9syj8ykbVTi+1BUQeJRx6yyCRRkEsFgi1dVITrJ2FWK5lozL4edCM59YpZrApxIZfePnDaEIYTCSNOdSrgLmSq8SYvZ52Ebd6y7HN7EHgSehmRebTalzSgOMT2ZFeLOX5auyACTkmLXEN2SoonQR7nn+1dX4NpFp04fHqbnP09hOv8AB9ItVAsx6m5/T2gE4e88YabWvUA5Ukie52HvWm6E9R3UMFHM5vcNfZVldbUg6bjtyO1DKEdyiMGHBj3wDjRUr7O8rMCCUKVqRA+XuIk9o+mF4j4ZVkXqMfP95n67Qg+tP1H9YcuXsi1JUCojUKAHm0230PKsnVLY54Y4+Jz71CuKmMXCnkKVGUJ2Sdx1nvQqV2MFiTktzAN3h/j2wUPmSYHcD+4ra0d4qfYx4M0vDtUKzsbozj4dlDd4kOeVWwnrXQ1EYM6OvBRgOzN/ulpDWYiQB0ms19weYTZDQPfYKxfMjMgQRIkQR+xoP4lg+3MlWZDmYLxvw4bZ0thRKRtPMU6FyMy5rDDIiuhuvYkpWZZabqQI9XWRCNowZq/UbX0jJli9u28pQYUe3I+vKkmrZnzMDUUPbbnoSHCMQ8NUAkDvqP6VL0EjiDs0DEeg5jG+sZAuBqNCNd+9Z1wYEIZnOjIcMIFvmQSFc070RCcbZC88QO5ihkxEdxWitagYmuiVKoGJ9VhUia0cY4jEgxDEi20pI3VoOxO/4UlrcrXuXvqK6pcLuibiKCQB1rmLK7Et9XvEX5EXTa+KrLGorS0WibUNx1K1Umwxp4e4ZswmLllK1TosqWN4ABAMDXn3rbbw1UXA5j34MKIbuODrWDkQU9gSf90mk20NbdEiDOnSLj/CLZBIdLS+SVNn8wYI9KVt0506lrDx8jr/AN98QBpx3I//AExb5ghYWtZ1ClmABrshOnLnJ1oNrqAuw985/wCpUIAcGQscPs+MFFAlG2ntSlOpYuVMuVEYL+3QU5dKJajOcCQwyMST7GEtaDlR/wAFbsl9mBAHhbkVlChk5MDicrPMVSznmBYYn7EsQSGiI80aHvTXhem/E34bpeT9fpNXwvT/AIi3B6HJimxZG4uENzGdW/Qakn1gGu2Ue07FmFaZ+JrlhaIbQEISEpGwGn/JqSZhWOWOTKHE+HB9CG1fJmBVG5jYTy1qUUNkGTQ5Rszmy4ftUKStDSQpOxE8xGvXc0LUUrahRhxL2W2MpXPcg42ufDUh4JlEkKI+7I377fjXN67SshmBqAU7izfNJeTmQZB+YJ+8PTkaywBu5ibLnqVGiAEgJKUxEHcVDnL5g88yFjBGnbhEqKFzIKdzzrf8MstYE4yBNrw7UWp7ZAmuO3qLa3lZlKRqTVtReGY4kvm18yrY3ilagaHWOlcv+Mdr8KOI41CqmD3AHEeCN3ralFIzNkx7bj0rpqLCUwe4nnBxMrxjC7aMyAUq1kDtQhqXR9rSg1ZqbBghuzOnlMGdeWm9MfiwBGB4ooHU5xt4tfw0ncb9v+Z+lWquNozPLrGuXMBJokqpMmaNEEcrMv2mIFBHMAgwZiqW1q45Erqa67UII5+YyOELSvyQpQypMzBUDqOsDWsnO2wA/PM55CEfJiK+ypKilSdR2rVDAjImgGzzPqizeglsnzJ3/etVtrqLE5BjwAIyIK4huitSW0cjJI6xoKzrNTV5wqPPv/1FdQwOB8ThNmsBKlDQH8Kr4wtT1q6dgwFgUjicX1mltYWBvTnhCgKVELpcA4ltCc6COo9fTT1rQb0tGy0OC7cQ0kvgBR08sGAOZGwPpSArR7CKuvrFlCk+mCblbLxGV4BUjSDJPKRGtL6+it1Fd/HPzItpLcmRJw9Si4sOzlUUJ0gykeffodNOhrOq8Pq3P7+yxby85wZ7d2ngpSoHNMg+9YepxS6uvBHBlQCBALN2oL8x0JrVqIS1echv+ZVe4cFyVNkD61vam1a0x7wzkKOYAcbWDuI9a5i9yaz8xRnGIFx/jBu2PhpQHFgeYkkJSemg8x99KnQ+Gs6b7TgHoTS0+g3pvsOAevrATfFSbkhsoyLJkQZCuwnUHtrMVq6HQjTXFkPBGOY94fUum1HpPBGP1hLAXg3eMEyJUUmeWYED8TH0rZB5m1qGBrImpqfAGtWCEzHYQbiOIpDaiTonWmaqSWAkDidYZfZhI1mvXVbTLGW71bfhqS98igUn30j8ay9Xp/NTAi+op3jiZKq6TbuKbCsyDOVXUcp71zeq0TVvhh/3MS2o0vtaUTjLgkKOw03II5elalOn0moXO3BEfppo1HQxHD4Rn7Q8684NUwE9BTFm2mghZpWItVGFj5xZgv2toslakAxqkxEGawN7g7scRWsgcwEu8/w5hwPOlwpSSlSoBiNBpTGk0VRXzQMH3hbmZxnPE8wXihgWSFLWApSM2u5JEn860K6RnMFTQbDkdTJE3pcUsxopSo9zIoGpQH1QGsqHLS6nkkbJ/wCazTmZHMWOIWClYUTOYE+hkyK09KwKY+Jo6ZvRj4gjxaah9+J2l6vZhFuOJKhU1OYUWjHMZbK4JU2nXTTTTTb96yrVGSZhWcsT9ZbvQSslLgCdIBkmIA3AqiHC4khj7Ga9c3Wd0LnSCNOesVnafU3IjbWIz2JpeYS2AeJ2SkKSeYUDSumu2WKB8/8AMpt7EN3gzIMDSK2ta1pXassgXHMHW7qFphXLSr6PxDy04ODAhiOp0zcthxKEnzEwOxOgPsda3NNfdcpZhxJRzmW7t4qYQDBPmBI6iPzptQFsYiMdMYtYZYrTcBxQIAMgHmeVK6zUVPg2EBQc5P0hGuXbiF8SuAyQVeZWZa0pEwJOZSj7mKV0umW+1b3bC/7R9T7n+ggaqSzbjJbl1LzRKE6Rmmdo12pbxfTXWK9fljGMhs/H0xnP/syLFOeoEesEeGFLVooSI3B2I+tZq1hdLXYW44I+hihGO5LhpR4cEyORoi68Wf6khvUMyC6sdykg+lRZUreodRfbzzMau21qcyFJzZoIJA80wRJ511AXeQF5nb2kNgL1Nh4f+HNm802tbRCgADyJ01Co0J7jpvR9RctL+lRMzU3+W/AEJPMNMLCUJBWkZPEVqsgbJntR0U2/5je/tDJl13N7+3tGC0tA63C/rzHvSdlvltlYpe+w+mCbvhAEFJWSFSn1BHOpbxoKQu3kyv4rcORBWHWKrZYZUZAHlPUbfWtOy0X17x+sarO8cQN8Sb0qaDSFFOuZRG8AbVga/WinFa/mP8CJ63U+UNq9zM22yUgkq5nWevL6VlPczn1HMxntaw+oy3cEfZ5TrMj6UbQoQ7GaXha+syLh3it+znwyIO4Nau4bcMJtllK7WEZME49uLm7b8ZzIykyoJ0mBoCek0m6K3CiLblJKoJx8V+I03KR4ZHhjyjqTzPppUV1+UhHzE7cqhBi5eqStm3hQENAQOZ0n9aTodt7g/MW09pyVzA1xflIyoMdxTpAMadgeJRVcK5qV9TUbRB5ninyRBJI7mpwBKZEhKJ+XXr2rwGeoJgCeJJbWi1qCEIUpR2AE1DsEGW6k5x3GG1wUMmXyCoCfDBmOkmlTqN4ynXzA2XEjCyyy3mXPM8uSR+9Lk5ip+JVvLtWdWVQA2EjppRVAx1LATTGr0qt0qSdetZAQLcQeoUsQMiGcMhTZUpUqrQ1tGm/CmysYYQ1bZOYwW+Po8PKr5opzw0DVAA94kg5OIut3ULn7ubUdp1rHs061a41v1mDb0tGy8tWCmSEpgTmEJKY5yK69EIGxf4mh5QxiVr5lSUg5gorKZjaNfMByJB2qjtb5RAHQP3g3LleupA84UOjUQJkkToNT71NehouqRnQFgBjPtIrRcdcwda25u31urMIjKkdhvPSjkbPSOhGC2wbRPLdXh5ihQKdQpBMKA2kJPzJ9NuleTUV2gq0oTn80A4jihQjIROp1964gVlC1JPCkgTMtbacSOxWspIBidaWxyVMArGG8PtTlkmPejVs4HEYwCIr8b8PSftDJPifeQYOaPvJ6mNx2rovDNdyK24+D/Saug12AKXP2P9I/fDXGA8wkKgKAEjvtMRpW14hUSA/zCeIVkHdP2JYU8bjIgQFHNmO0TuTTFGpqFO5pavUKKwTHC0tg2gJmYGpPOsTUXZy0z7LDY2ZRYdUpwRsNh0FcRp9fdqfEQF6yf0HuYy6KtcS+O7hafFDSoKSCD06xX0e42JojYhwZNzMtAdTgxetWs7Da1qJVl3VoSc0EGff8K4mywtcSxznn9ZlvmysMe5RcdbMpKTpv8v8A/VVIbsRMyi7Z2xAzApAmI0A+lGr1WrrHp/vC12MnRgJ3hNSnP4awpB1T1I0kHlzrRr8TQj1jB95p060H88H4rYu26y2RHMEbKB5g/wB7U7Vetq7k6hzduGUg+5cJZk/z/kkUVjlYCxiU5+YZsGm3GUSRITBAKZ07Gs18q5mc3DZgDErUNknUSdAaarsLQ6WkylbsrcUEISVKOgA3NF+sICzdTQ+H+F2rZhb2IJTH8pAVlHLUCc3YUrZqNx219wu3A5jJhuHYe+yMjSFMrk+WUkHVMnmDvvSba19PYQx7l1rLDKwGxwGhS1ptX3mXUHZ0ApUDr5VtkEj1E9qcGrqdefeDKAErF+/w923uVtvkeLoQBqFTsoHpA/CodRsBTqKW17RxPzt+lsFCDKzurp6d6CqE8xcCBXTqaKOoSbPhuChtrLO9Zt1X++EUZ4hhvASlIWFH96paCaiPpHU04UZgIvSsoAJVPKi6cutS219xZgexLqbWBJ9xSupuZrBa3cHyRzC987mbCdSlQCTHfSu80VivStv0mrW2UBkeJ3xEpIIA27AACirZVWNzHEIpGMwFcX2cDMTuPNJ113FB8uqlHsoHJglrRX494wcO/wCQ4RsFn8UpNAW30+r2Eq/DYlK9wxMBZ+Zskj1Mg/mazVc1q5PRzKWv6D9pRbsw6ohQ00Jrnqay+oJHWZmsMgGE3LNsjy/dp7UVVvwZG0GVMQu8iwkAgRoeRpe9zQMgZErYdsF4tjSG2/HWCqPKhMgZle/IRqavo/MvbJ4EJp6za32k3wssnrt1d0pxDTYUZbaSApZ/7nI78t66aq1qqcE7geOTn9ZtWMa6gO89Zjw5jKl3DrPhOIShtLiVKKfOAohZyAkoT8sFYBVrGgqunILYitH5uYSZxxrKJVVrdA7HEs2nbPEuXN2220p7SAnN66aCs78NVQS4UA45MA5YcNMoxfEgS4twgJJEE6zOuw3ra11qHw4KhyWGOI1qLA1O1ZWD3joIQ4kzABCjIJ6iJTyrhbazW67hiZ4U7CDAK+CL3zOJW0YHyhSpMeqYn1p1L6zhYuKziU/AeQpLbraklXdCh/8Aif7iruu3kGVNZEbMAsUEFLclIJJcWAkCdSB27a0OrR3alsquPr7RijTtZ+UcfMJ4irDQlHjQ85CgiQYgaqKRsY015Vr6fRDSKctn3P3jvksnGYjYpws9dpBt20BCSfvAT00617zgeMSHxjGYwYDwxaW9qlq+CCtSirN8pBIgJCgZ0FCcFjkiD8oMIl8bYA2heVh3OkagKIJE8pFM1acAZBlxpMDIMVbC5XbPpWPmQQdOY5j3FWwAcMMylZ2NzPo3hq5bu7ZDyUZQsbHfofUVn36by24hG4MTb/FWbLE3G1oCELZbCEoSVFRzr3CRoT5oA7dTSGt0r6ipdnJBOYzpbRWxz7yVeLI+xnEB4jag6UAKSflKtErHITJB5FXrRqNEdnlkjIEWvbe5dYI4oabvvCuYWlxCYWkCc6BqCmNZE/jRU1AVDWe4o9oK/WL679lmQi3GYc3APy3qoVm5JiwBgN9edRUQATrCRlHsBTA44lsTU3bl7NEwByrMKu69wlanMPWuLPLGQARzM1aml3BQx8X8YhnAFMI+cAKJ1NMaEr5AU9jiLFgDOeJkpHmTtVtVpAULCUdpRwx1KwhHPNt2+anvBtbWKBp2/NzgfTuH01qldnvLmPNidp0/pWu1QtrKmMAdiL7CADkKZACoT6n+/rWVXeRWNMxxg9wFVgDbDxPcNuw26RCvDyqVlkkZhEfWIou3LBO4w55x7zrBipbTmckrKys66DMZgem1Z3itxB8vGAOvr9YvqPUdvsJ2p4BajsMopCoqtrH2wDFD+WGMCeYUjzSSdwAa2K9Dp8bm5J+TDIo2yW7wVlatUqKMp0MyknQajY/tS+toqpYccEdSrIDxE3iexzJSxAKE6bDUjvy3rPq1bJ/p4xApa9J9MVWrm9wl1TlsslufMCJQeyk8v9QitjT6pbBgftNWrUpYu0j9P6iOGF/GPOtActkjNlDqwdco3gR1JABP3vWml2Q501O3IJ/tDjF4i4lakluTJSNuoj2o1HjCBdpzkfzFk16qCMZIgPizHXX8rCElCEEHXYwdCo8x2FYuq1fnEgjA/kzOvvNhiPxI6kpEkhIMJIEkcoiQI5nnpVdGjhS2f0l9PkKTO+E7Y5LhQdSpKkJAKc+YGTrlUAQdaFrrMlBjkGF3lgYZw67bt/8AqXCjES4txRPLRKjA9QKUu823vAHwP7xctjuVXcVUV7IyJBKZ3/8Asrrz0p/TJUTmwEnjgQum2u2CMwJc8XPjKEhIQNkgSn6bH3rbfUenbjAmg+o2DbjiMuJXVz9mQq4UFKSFL2AyykpAERyVFZtNqX2kAcf294GqxbbduIO4G4l8FQaWTkUfoavfUTyIW1cGPuL4aHB4gM6aDeiUsLKzWYFvUpSJXE2EJU2pSQErSCrROqgBJEp/uaRTcrYmaHZTjMR/sQy59OpmDpRxYS2IfnAweYx/DLiJacRbDi1FKx4QBJgSPLp7R7067F6jWTHF5G0zacRw63bIuXco8MqdK1RvkyTPZOg9qwdljkoPeWTngTMviXxdbXOHtptnNXXZWiIISgK+YcvNlrR0tLU7syrkAcHuJeE4gpASnxshTEEzIG4E7R2oeorBbO3MQuHq4htvDy6CoKbcB3+U/wDFK7scciLkMJx/gpH/AEk/VX71fc3zPbmmioSM1D0nNYz8Rz3MlsPnNMaf/VMqvc6dPzUnT+Rv/wCjK2y08ZZE1oZPkyntOOGx/HT6K/2mszwj/wDcH2P9JfSf6ohfE/nV/pT+tdonU0z1F+/0dEfyiuc1v+vM3Ufnn66+RHfNPfStnw/mokxyo5XmC8IWZc1Ow/Osvx3/AEV+8mzqXmdVCf5f1rNp5sGfiJf7Y5cKNp8AmBueQrX045lq/wAsgsDKFzr/ABFD200rO8WJ80D6RkDmD7xA8RQgRrpy5cqydNw+PpF7wOIDebEqEDY8q0V4XiK5weIh4XbIznyJ/wA3oORMVq2EioEfE2bCfKB+k0TCNjWbV+YTGEDcWHzAdqpqP9SVPczjHzofUf8A7U9pfyGMU/kP3h/hQfwH/VP60jq/zL95Nf5WlO0Mk16zqKQfiJl5KTtO3L6Vt6ZRsHE6DSqNo4kzjSQ4kBIAlOgAjejagAA4+JbVqBnAjbx2f/bn/wCv51jeE/nb7RPQf6szZg+atdo1Z3Nowpw/ZWzJnKOdC04G4xf3kNpqNep/3Urd/qtEdR/qGZjfDzOjlmWPaTVU/MJ6vsRfsjDzZGnnR/uFPr2I+Pzj7iNPFWIvLtwlbrihnXopaiNFmNCaLcADxHdQMVkj6RWc+VPp+tLiZK/mjBibKQy0QkAwNYE7daRVibGEXY+sytw8si5bgkSoAwdx0PUVe0ZQyDNCTSg6gZ//2Q==";
        //        r1.Organization = "Sonora";
        //        r1.ContactPerson = "Ram";
        //        r1.Address = "Pune";
        //        r1.ContactEmail = "abc@gmail.com";
        //        r1.Gender = "Male";
        //        bool b = bl.UpdateArtistProfile(r1,false);
        //        Assert.IsTrue(b);
        //    }
        //    catch (Exception ex)
        //    {
        //        Assert.Fail("Error Occured", ex.Message);
        //        throw;
        //    }
        //}

        [TestMethod]
        public void InvalidUserID()
        {
            try
            {
                AccountBL bl = new AccountBL();
                UserProfileDTO r1 = new UserProfileDTO();

                r1.Id = 100;
                r1.CountryCode = "IN";
                r1.StateCode = "Uttar Pradesh";
                r1.CityId = 20;
                bool b = bl.UpdateArtistProfile(r1, false);
                Assert.IsFalse(b);
            }
            catch (Exception ex)
            {
                Assert.Fail("Error Occured", ex.Message);
                throw;
            }
        }


        [TestMethod]
        public void InvalidState()
        {
            try
            {
                CommonBL cl = new CommonBL();
                UserProfileDTO r1 = new UserProfileDTO();

                r1.Id = 1;
                r1.CountryId = 77;
                r1.StateId = 1000;
                r1.CityId = 20;
                Boolean b = cl.ValidateUserCountry(r1.Id, r1.CountryId, r1.StateId, r1.CityId);
                Assert.IsFalse(b);
            }
            catch (Exception ex)
            {
                Assert.Fail("Error Occured", ex.Message);
                throw;
            }
        }

        [TestMethod]
        public void InvalidCity()
        {
            try
            {
                CommonBL cl = new CommonBL();
                UserProfileDTO r1 = new UserProfileDTO();

                r1.Id = 1;
                r1.CountryId = 77;
                r1.StateId = 35;
                r1.CityId = 2000;
                bool b = cl.ValidateUserCountry(r1.Id, r1.CountryId, r1.StateId, r1.CityId);
                Assert.IsFalse(b);
            }
            catch (Exception ex)
            {
                Assert.Fail("Error Occured", ex.Message);
                throw;
            }
        }

        [TestMethod]
        public void InvalidArtistCategory()
        {
            try
            {
                AccountBL bl = new AccountBL();
                UserProfileDTO r1 = new UserProfileDTO();

                r1.Id = 1;
                r1.ArtistCategoryId = new int[10014];
                r1.CountryId = 77;
                r1.StateId = 35;
                r1.CityId = 20;
                r1.UserType = "test";
                bool b = bl.UpdateArtistProfile(r1,false);
                Assert.IsFalse(b);
            }
            catch (Exception ex)
            {
                Assert.Fail("Error Occured", ex.Message);
                throw;
            }
        }

        //[TestMethod]
        //public void InvalidDatatype()
        //{
        //    try
        //    {
        //        AccountBL bl = new AccountBL();
        //        UserProfileDTO r1 = new UserProfileDTO();

        //        r1.Id = 1;
        //        r1.BirthDate = "test";
        //        r1.ArtistCategoryId = 1;
        //        r1.CountryId = 77;
        //        r1.StateId = 35;
        //        r1.CityId = 20;
        //        Boolean b = bl.UpdateArtistProfile(r1, false);
        //        Assert.IsFalse(b);
        //    }
        //    catch (Exception ex)
        //    {
        //        Assert.Fail("Error Occured", ex.Message);
        //        throw;
        //    }
        //}

        /// <summary>
        /// Forgot Password Test Methods
        /// </summary>
        //[TestMethod]
        //public void UpdatedPassword()
        //{
        //    try
        //    {
        //        AccountBL account = new AccountBL();
        //        Boolean isUpdated = account.ForgotPassword(44, "1234");
        //        Assert.IsTrue(isUpdated);
        //    }
        //    catch (Exception ex)
        //    {
        //        Assert.Fail("Expected no exception, but got: " + ex.Message);
        //        throw;
        //    }
        //}

        //[TestMethod]
        //public void EmptyPassword()
        //{
        //    try
        //    {
        //        AccountBL account = new AccountBL();
        //        Boolean isUpdated = account.ForgotPassword(45, "");
        //        Assert.IsFalse(isUpdated);
        //    }
        //    catch (Exception ex)
        //    {
        //        Assert.Fail("Expected no exception, but got: " + ex.Message);
        //        throw;
        //    }
        //}
        //[TestMethod]
        //public void ValidatePasswordwithspaces()
        //{
        //    try
        //    {
        //        AccountBL account = new AccountBL();
        //        bool isUpdated = account.ForgotPassword(45, " 12345 ");
        //        Assert.IsTrue(isUpdated);
        //    }
        //    catch (Exception ex)
        //    {
        //        Assert.Fail("Expected no exception, but got: " + ex.Message);
        //        throw;
        //    }
        //}

        /// <summary>
        /// given valid id
        /// checked with email of id 4
        /// </summary>
        [TestMethod]
        public void GetDataWithValidID()
        {
            AccountBL account = new AccountBL();

            UserDetailDTO userprofileDTO = account.GetUserData(4);
            try
            {
                Assert.AreEqual("alka16@gmail.com", userprofileDTO.Email);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
                throw;
            }
        }

        /// <summary>
        /// given invalid id
        /// checked with id=null then data not exist
        /// </summary>
        [TestMethod]
        public void GetDataWithInvalidID()
        {
            AccountBL account = new AccountBL();

            UserDetailDTO userProfileDTO = account.GetUserData(798);
            try
            {
                Assert.AreEqual(0, userProfileDTO.Id);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
                throw;
            }
        }
        /// <summary>
        /// Forgot Password Test Methods
        /// </summary>
        [TestMethod]
        public void UpdatedPassword()
        {
            try
            {
                AccountBL account = new AccountBL();
                ForgotPasswordDTO data = new ForgotPasswordDTO();

                data.Email = "alka16@gmail.com";
                data.Password = "123456";
                bool isUpdated = account.ForgotPassword(data);
                Assert.IsTrue(isUpdated);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
                throw;
            }
        }
        [TestMethod]
        public void EmptyPassword()
        {
            try
            {
                AccountBL account = new AccountBL();
                ForgotPasswordDTO data = new ForgotPasswordDTO();
                data.Email = "alka16@gmail.com";
                data.Password = " ";
                bool isUpdated = account.ForgotPassword(data);
                Assert.IsFalse(isUpdated);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
                throw;
            }
        }
        [TestMethod]
        public void ValidatePasswordwithspaces()
        {
            try
            {
                AccountBL account = new AccountBL();
                ForgotPasswordDTO data = new ForgotPasswordDTO();

                data.Email = "alka16@gmail.com";
                data.Password = " 123456 ";
                bool isUpdated = account.ForgotPassword(data);
                Assert.IsTrue(isUpdated);
            }
            catch (Exception ex)
            {
                Assert.Fail("Expected no exception, but got: " + ex.Message);
                throw;
            }
        }
    }
}

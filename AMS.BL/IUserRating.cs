﻿using AMS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS.BL
{
  public interface IUserRating
    {
        bool AddUserRating(UserRatingDTO pRating);
    }
}

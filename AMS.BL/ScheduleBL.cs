using AMS.DAL;
using AMS.Models.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS.BL
{
  public class ScheduleBL : ISchedule
  {
    public bool CreateArtistSchedule(ArtistScheduleDTO pSchedule)
    {
      bool isAdded = false;
      CommonBL common = new CommonBL();
      try
      {
        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
         
          tblMstCountry country = entity.tblMstCountries.Where(r => r.CountryCode == pSchedule.CountryCode.ToUpper()).FirstOrDefault();
          tblMstState state = entity.tblMstStates.Where(r => r.StateCode == pSchedule.StateCode.ToUpper()).FirstOrDefault();
          if (country != null && state != null)
          {
            pSchedule.CountryId = country.Id;
            pSchedule.StateId = state.Id;

            bool isValid = common.ValidateUserCountry(pSchedule.ArtistId, pSchedule.CountryId, pSchedule.StateId, pSchedule.CityId);

            if (pSchedule.ScheduleId <= 0)
            {
              
                  tblArtistSchedule artSchedule = new tblArtistSchedule();

                  var config = new MapperConfiguration(cfg =>
                  {
                    cfg.CreateMap<ArtistScheduleDTO, tblArtistSchedule>();
                  });

                  IMapper mapper = config.CreateMapper();
                  artSchedule = mapper.Map<ArtistScheduleDTO, tblArtistSchedule>(pSchedule);
                  artSchedule.CreatedOn = DateTime.Now;
                  artSchedule.CreatedBy = pSchedule.ArtistId;
                  entity.tblArtistSchedules.Add(artSchedule);
                  entity.SaveChanges();
                  isAdded = true;
             }
 
            else
            {
              tblArtistSchedule artSchedule = entity.tblArtistSchedules.Where(r => r.Id == pSchedule.ScheduleId).FirstOrDefault();
              
              artSchedule.Address = pSchedule.Address;
              artSchedule.AvailableFrom = Convert.ToDateTime(pSchedule.AvailableFrom);
              artSchedule.AvailableTo = Convert.ToDateTime(pSchedule.AvailableTo);
              artSchedule.ArtistId = pSchedule.ArtistId;
              artSchedule.StateId = pSchedule.StateId;
              artSchedule.CityId = pSchedule.CityId;
              artSchedule.Status = pSchedule.Status;
              artSchedule.UpdatedOn = DateTime.Now;
              artSchedule.UpdatedBy = pSchedule.ArtistId;
              entity.SaveChanges();
              isAdded = true;
             
            }
          }
        }
        return isAdded;       
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    //Scheduled Events 
    public List<ScheduledEventsDTO> GetArtistsScheduledEvents(int pId)
    {
      try
      {
        List<ScheduledEventsDTO> artists = new List<ScheduledEventsDTO>();
        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
          List<usp_GetArtistSchedule_Result> artistList = entity.usp_GetArtistSchedule(pId).Where(x => x.EventEnd >= DateTime.Now).ToList();
          if (artistList != null)
          {
            var config = new MapperConfiguration(cfg =>
            {
              cfg.CreateMap<usp_GetArtistSchedule_Result, ScheduledEventsDTO>();

            });

            IMapper mapper = config.CreateMapper();
            artists = mapper.Map<List<usp_GetArtistSchedule_Result>, List<ScheduledEventsDTO>>(artistList);

          }
        }
        return artists;
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
    public List<ArtistScheduleDTO> GetArtistSchedules()
    {
      try
      {
        List<ArtistScheduleDTO> artists = new List<ArtistScheduleDTO>();
        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
          List<usp_GetAllArtistSchedules_Result> artistList = entity.usp_GetAllArtistSchedules().Where(x => x.AvailableTo >= DateTime.Now && x.Status=="AVAILABLE").OrderBy(x=>x.AvailableFrom).ToList();
          if (artistList != null)
          {
            var config = new MapperConfiguration(cfg =>
            {
              cfg.CreateMap<usp_GetAllArtistSchedules_Result, ArtistScheduleDTO>();

            });

            IMapper mapper = config.CreateMapper();
            artists = mapper.Map<List<usp_GetAllArtistSchedules_Result>, List<ArtistScheduleDTO>>(artistList);

          }
        }
        return artists;
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    public List<ArtistScheduleDTO> GetArtistScheduleByArtistId(int pArtistId)
    {

      try
      {
        List<ArtistScheduleDTO> artists = new List<ArtistScheduleDTO>();
        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
          List<usp_GetAllArtistSchedules_Result> artistList = entity.usp_GetAllArtistSchedules().Where(x=>x.ArtistId== pArtistId && x.AvailableTo >= DateTime.Now).OrderBy(x=>x.AvailableFrom).ToList();
          if (artistList != null)
          {
            var config = new MapperConfiguration(cfg =>
            {
              cfg.CreateMap<usp_GetAllArtistSchedules_Result, ArtistScheduleDTO>();

            });

            IMapper mapper = config.CreateMapper();
            artists = mapper.Map<List<usp_GetAllArtistSchedules_Result>, List<ArtistScheduleDTO>>(artistList);

          }
        }
        return artists;
      }
      catch (Exception ex)
      {
        throw ex;
      }

    }

    public ArtistScheduleDTO GetScheduleByScheduleId(int pScheduleId)
    {

      try
      {
        ArtistScheduleDTO artists = new ArtistScheduleDTO();
        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
          usp_GetAllArtistSchedules_Result scheduleList = entity.usp_GetAllArtistSchedules().Where(x => x.ScheduleId == pScheduleId && x.AvailableTo >= DateTime.Now).FirstOrDefault();
          if (scheduleList != null)
          {
            var config = new MapperConfiguration(cfg =>
            {
              cfg.CreateMap<usp_GetAllArtistSchedules_Result, ArtistScheduleDTO>();

            });

            IMapper mapper = config.CreateMapper();
            artists = mapper.Map<usp_GetAllArtistSchedules_Result, ArtistScheduleDTO>(scheduleList);

          }
        }
        return artists;
      }
      catch (Exception ex)
      {
        throw ex;
      }

    }

    public bool DeleteScheduleByScheduleId(int pScheduleId)
    {
      try
      {
        bool isDeleted = false;
        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
          tblArtistSchedule tblArtistSchedule = entity.tblArtistSchedules.Where(x => x.Id == pScheduleId).FirstOrDefault();
          if(tblArtistSchedule != null)
          {
            entity.tblArtistSchedules.Remove(tblArtistSchedule);
            entity.SaveChanges();
            isDeleted = true;
          }
          
        }
        return isDeleted;
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
   
  }
}

using AMS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS.BL
{
  public interface ISchedule
    {
        List<ScheduledEventsDTO> GetArtistsScheduledEvents(int Id);
        bool CreateArtistSchedule(ArtistScheduleDTO schedule);
        List<ArtistScheduleDTO> GetArtistSchedules();
        List<ArtistScheduleDTO> GetArtistScheduleByArtistId(int ArtistId);
        ArtistScheduleDTO GetScheduleByScheduleId(int ScheduleId);
        bool DeleteScheduleByScheduleId(int schedule);
  }
}

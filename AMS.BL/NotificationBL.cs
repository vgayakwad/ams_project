using AMS.DAL;
using AMS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS.BL
{
  public class NotificationBL:INotification
  {
    public bool AddNotificationData(NotificationDTO pNotification)
    {
      bool isAdded = false;
      using (AMS_DevpEntities entity = new AMS_DevpEntities())
      {
        if (pNotification.Id <= 0)
        {
          tblNotification notification = new tblNotification();
          notification.IsRead = false;
          notification.ReceiverId = pNotification.ReceiverId;
          notification.SenderId = pNotification.SenderId;
          notification.Subject = pNotification.Subject;
          notification.Message = pNotification.Message;
          notification.CreatedBy = pNotification.SenderId;
          notification.CreatedOn = DateTime.Now;
          entity.tblNotifications.Add(notification);
          entity.SaveChanges();
          isAdded = true;

        }
      }
        
        return isAdded;
    }
  }
}

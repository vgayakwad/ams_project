using AMS.Models;
using AMS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS.BL
{
  public interface IAccount
    {
        EventCategoryDTO GetCategory(int Id);
        LoginResponse Authentication(string email, string password, string provider, bool isWebAPI);
        bool Register(RegisterUserDTO RegisterData);
        bool UpdateArtistProfile(UserProfileDTO UserInput, bool isWebAPI);
        bool ForgotPassword(ForgotPasswordDTO password);
        UserDetailDTO GetUserData(long Id);
        bool Logout(string AccessToken);
        List<UserDetailDTO> SearchArtist(string pSearchTitle);
        List<UserDetailDTO> SearchUser(string pSearchTitle,string pUserType);
        List<ArtistSkillsDTO> GetArtistSkills();
        LoginResponse CheckUser(string Email,string Provider);
    }
}

using AMS.DAL;
using AMS.Models;
using AMS.Models.Models;
using AMS.Utility;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace AMS.BL
{
  public class EventBL : IEvent
  {
    /// <summary>
    /// Displaying artist lists acoording to choosen artist catyegory(not in use).
    /// </summary>
    /// <param name="pArtists"></param>
    /// <returns></returns>
    public List<ArtistListDTO> GetArtistList(ArtistListDTO pArtists)
    {
      try
      {
        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
          List<ArtistListDTO> artistDto = new List<ArtistListDTO>();

          List<usp_GetArtistScheduleList_Result> artistList = entity.usp_GetArtistScheduleList().ToList();
          if (artistList != null)
          {

            foreach (usp_GetArtistScheduleList_Result artistdata in artistList)
            {

              string availableTo = Convert.ToDateTime(pArtists.AvailableTo).ToString("yyyy-MM-dd");
              string availableFrom = Convert.ToDateTime(pArtists.AvailableFrom).ToString("yyyy-MM-dd");
              string Toavailable = Convert.ToDateTime(artistdata.AvailableTo).ToString("yyyy-MM-dd");
              string Fromavailable = Convert.ToDateTime(artistdata.AvailableFrom).ToString("yyyy-MM-dd");
              if (Convert.ToDateTime(Fromavailable) >= Convert.ToDateTime(availableFrom) && Convert.ToDateTime(Toavailable) <= Convert.ToDateTime(availableTo))
              {
                var config = new MapperConfiguration(cfg =>
                {
                  cfg.CreateMap<usp_GetArtistScheduleList_Result, ArtistListDTO>();

                });

                IMapper mapper = config.CreateMapper();
                ArtistListDTO artistList1 = new ArtistListDTO();
                artistList1 = mapper.Map<usp_GetArtistScheduleList_Result, ArtistListDTO>(artistdata);

                artistDto.Add(artistList1);

              }
            }
          }

          return artistDto;
        }

      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
    /// <summary>
    /// get list of events on passing country code,state code and city id and datefilter
    /// </summary>
    /// <param name="pSearchTitle"></param>
    /// <param name="pEventAttribute"></param>
    /// <param name="pUserId"></param>
    /// <param name="pIsPrivate"></param>
    /// <param name="pTotalRecords"></param>
    /// <param name="pDateFilter"></param>
    /// <returns></returns>
    public List<DashboardDTO> GetEventList(EventListDTO pEventData, out int pTotalRecords)
    {
      try
      {
        // usp_GetEventList_Result or usp_GetEventListWithRatings_Result
        List<DashboardDTO> objDashboard = null;
       
        ObjectParameter objParam = new ObjectParameter("RecordCount", typeof(int));

        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
          //int objParam = 10;
          var config = new MapperConfiguration(cfg =>
          {
            cfg.CreateMap<usp_GetEventListWithRatingsAndPastFilters_Result, DashboardDTO>();
          });
        
          IMapper mapper = config.CreateMapper();
          List<usp_GetEventListWithRatingsAndPastFilters_Result> sourcePersonDetails = entity.usp_GetEventListWithRatingsAndPastFilters(pEventData.SearchTitle.IfNotNullTrim().ToUpper(), pEventData.EventAttribute.IfNotNullTrim().ToUpper(), objParam, pEventData.UserId, pEventData.IsPrivate, pEventData.DateFilter.ToUpper(),pEventData.Type,pEventData.rating).GroupBy(x => x.Id).Select(x => x.FirstOrDefault()).ToList();

          pTotalRecords = Convert.ToInt32(objParam.Value);
          objDashboard = mapper.Map<List<usp_GetEventListWithRatingsAndPastFilters_Result>, List<DashboardDTO>>(sourcePersonDetails);
        }
        return objDashboard;
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
    /// <summary>
    /// Displaying Event details of given event id.
    /// </summary>
    /// <param name="pId"></param>
    /// <returns></returns>
    public DashboardDTO GetEventDetails(long pId,bool IsPast)
    {
      try
      {
        //usp_GetEventDetails_Result
        DashboardDTO objEvent = null;
        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
          usp_GetEventDetailsData_Result sourceDetails = entity.usp_GetEventDetailsData().Where(x => x.Id == pId).FirstOrDefault();

          if (sourceDetails != null)
          {
            var config = new MapperConfiguration(cfg =>
            {
              cfg.CreateMap<usp_GetEventDetailsData_Result, DashboardDTO>();
            });

            IMapper mapper = config.CreateMapper();
            objEvent = mapper.Map<usp_GetEventDetailsData_Result, DashboardDTO>(sourceDetails);
          }

          List<ArtistListDTO> artistList = new List<ArtistListDTO>();

          List<usp_GetPerformingArtists_Result> artist = entity.usp_GetPerformingArtists(pId).GroupBy(x=>x.Id).Select(x=>x.FirstOrDefault()).ToList();
          if (artist.Count > 0)
          {
            var config1 = new MapperConfiguration(cfg =>
            {
              cfg.CreateMap<usp_GetPerformingArtists_Result, ArtistListDTO>();
            });

            IMapper mapper1 = config1.CreateMapper();

            artistList = mapper1.Map<List<usp_GetPerformingArtists_Result>, List<ArtistListDTO>>(artist);

            objEvent.ArtistsList = artistList;

            foreach (ArtistListDTO artistListdata in objEvent.ArtistsList)
            {
              List<ArtistCategoryDTO> ArtistCategoryList = new List<ArtistCategoryDTO>();
              List<tblEventArtistMap> Objartistcategory = entity.tblEventArtistMaps.Where(x => x.EventId == pId && x.ArtistUserId == artistListdata.id).ToList();

              if (Objartistcategory.Count > 0)
              {
                foreach (tblEventArtistMap artistcategory in Objartistcategory)
                {
                  tblMstArtistCategory artistCategories = entity.tblMstArtistCategories.Where(x => x.Id == artistcategory.ArtistCategoryId).FirstOrDefault();
                  var config = new MapperConfiguration(cfg =>
                  {
                    cfg.CreateMap<tblMstArtistCategory, ArtistCategoryDTO>();
                  });

                  IMapper mapper = config.CreateMapper();
                  ArtistCategoryDTO ArtistCategory = new ArtistCategoryDTO();
                  ArtistCategory = mapper.Map<tblMstArtistCategory, ArtistCategoryDTO>(artistCategories);
                  ArtistCategoryList.Add(ArtistCategory);
                  artistListdata.ArtistCategories = ArtistCategoryList;
                  //artistList.ArtistCategoryList = ArtistCategoryList;
                  //objEvent.Append(artistList);
                }

              }

            }
          }

          usp_GetHostDetails_Result hostDetails = entity.usp_GetHostDetails(pId).FirstOrDefault();
          if (hostDetails != null)
          {
            var config = new MapperConfiguration(cfg =>
            {
              cfg.CreateMap<usp_GetHostDetails_Result, OrganizerDTO>();
            });

            IMapper mapper = config.CreateMapper();
            objEvent.OrganizerDetails = mapper.Map<usp_GetHostDetails_Result, OrganizerDTO>(hostDetails);
          }
          if (IsPast == true)
          {
            List<RatingsDTO> ratings = new List<RatingsDTO>();
            //RatingsDTO ratingsDTOs = new RatingsDTO();
            List<tblUserRating> tblUserRating = entity.tblUserRatings.Where(x => x.EventOrUserId == pId).ToList();
            foreach(tblUserRating UserRatings in tblUserRating)
            {
              if (UserRatings.Rating == 1)
              {
                RatingsDTO ratingsDTO = new RatingsDTO();
                ratingsDTO.EventId = (int)UserRatings.EventOrUserId;
                ratingsDTO.Ratings = 1;
                ratingsDTO.TotalCount = tblUserRating.Where(r => r.Rating == 1).Count();
                ratings.Add(ratingsDTO);
                //objEvent.RatingInfo = ratings;
              }
              else if (UserRatings.Rating == 2)
              {
                RatingsDTO ratingsDTO = new RatingsDTO();
                ratingsDTO.EventId = (int)UserRatings.EventOrUserId;
                ratingsDTO.Ratings = 2;
                ratingsDTO.TotalCount = tblUserRating.Where(r => r.Rating == 2).Count();
                ratings.Add(ratingsDTO);
                //objEvent.RatingInfo = ratings;
              }
              else if (UserRatings.Rating == 3)
              {
                RatingsDTO ratingsDTO = new RatingsDTO();
                ratingsDTO.EventId = (int)UserRatings.EventOrUserId;
                ratingsDTO.Ratings = 3;
                ratingsDTO.TotalCount = tblUserRating.Where(r => r.Rating == 3).Count();
                ratings.Add(ratingsDTO);
                //objEvent.RatingInfo = ratings;
              }
              else if (UserRatings.Rating == 4)
              {
                RatingsDTO ratingsDTO = new RatingsDTO();
                ratingsDTO.EventId = (int)UserRatings.EventOrUserId;
                ratingsDTO.Ratings = 4;
                ratingsDTO.TotalCount = tblUserRating.Where(r => r.Rating == 4).Count();
                ratings.Add(ratingsDTO);
                //objEvent.RatingInfo = ratings;
              }
              else if (UserRatings.Rating == 5)
              {
                RatingsDTO ratingsDTO = new RatingsDTO();
                ratingsDTO.EventId = (int)UserRatings.EventOrUserId;
                ratingsDTO.Ratings = 5;
                ratingsDTO.TotalCount = tblUserRating.Where(r => r.Rating == 5).Count();
                ratings.Add(ratingsDTO);
                //objEvent.RatingInfo = ratings;
              }
              else
              {
                RatingsDTO ratingsDTO = new RatingsDTO();
                ratingsDTO.EventId = (int)UserRatings.EventOrUserId;
                ratingsDTO.Ratings = 0;
                ratingsDTO.TotalCount = 0;
                ratings.Add(ratingsDTO);
              }
             
            }
            objEvent.RatingInfo = ratings.GroupBy(x => x.Ratings).Select(x => x.FirstOrDefault()).OrderByDescending(x => x.Ratings).ToList();

            foreach (RatingsDTO ratingsDTO1 in objEvent.RatingInfo)
            {
              if(ratingsDTO1.Ratings!=1)
              {
                RatingsDTO ratingsDTO = new RatingsDTO();
                ratingsDTO.EventId = (int)ratingsDTO1.EventId;
                  ratingsDTO.Ratings = 1;
                  ratingsDTO.TotalCount = 0;
                  ratings.Add(ratingsDTO);
              }
              if (ratingsDTO1.Ratings != 2)
              {
                RatingsDTO ratingsDTO = new RatingsDTO();
                ratingsDTO.EventId = (int)ratingsDTO1.EventId;
                ratingsDTO.Ratings = 2;
                ratingsDTO.TotalCount = 0;
                ratings.Add(ratingsDTO);
              }
              if (ratingsDTO1.Ratings != 3)
              {
                RatingsDTO ratingsDTO = new RatingsDTO();
                ratingsDTO.EventId = (int)ratingsDTO1.EventId;
                ratingsDTO.Ratings = 3;
                ratingsDTO.TotalCount = 0;
                ratings.Add(ratingsDTO);
              }
              if (ratingsDTO1.Ratings != 4)
              {
                RatingsDTO ratingsDTO = new RatingsDTO();
                ratingsDTO.EventId = (int)ratingsDTO1.EventId;
                ratingsDTO.Ratings = 4;
                ratingsDTO.TotalCount = 0;
                ratings.Add(ratingsDTO);
              }
              if (ratingsDTO1.Ratings != 5)
              {
                RatingsDTO ratingsDTO = new RatingsDTO();
                ratingsDTO.EventId = (int)ratingsDTO1.EventId;
                ratingsDTO.Ratings = 5;
                ratingsDTO.TotalCount = 0;
                ratings.Add(ratingsDTO);
              }
            }
            if (tblUserRating.Count()==0)
            {
              for (int i = 1; i <= 5; i++)
              {
                RatingsDTO ratingsDTO = new RatingsDTO();
                ratingsDTO.EventId = (int)pId;
                ratingsDTO.Ratings = i;
                ratingsDTO.TotalCount = 0;
                ratings.Add(ratingsDTO);
              }
            }
            objEvent.RatingInfo = ratings.GroupBy(x => x.Ratings).Select(x => x.FirstOrDefault()).OrderByDescending(x => x.Ratings).ToList();
            
          }
          
          return objEvent;
          
        }
      }

      catch (Exception ex)
      {
        throw ex;
      }
    }
    /// <summary>
    /// validating user country, artist categories and Event category.
    /// </summary>
    /// <param name="pEvent"></param>
    /// <returns></returns>
    public bool Validate(EventDTO pEvent)
    {
      bool IsAdded = false;
      try
      {
        CommonBL common = new CommonBL();

        bool res = common.ValidateUserCountry(pEvent.UserId, pEvent.CountryId, pEvent.StateId, pEvent.CityId);
        if (res)
        {
          bool validate = common.ValidateEventCategory(pEvent.EventTypeId, pEvent.UserId);
          if (validate)
          {
            if (pEvent.ArtistsList != null)
            {
              bool artistValidate = true;
              foreach (var artistid in pEvent.ArtistsList)
              {
                bool artist = common.ValidateArtistCategory(artistid.ArtistCategoryId, artistid.ArtistUserId);
                if (artist == false)
                {
                  artistValidate = false;
                  break;
                }
              }
              if (artistValidate == true)
              {
                IsAdded = true;
              }
            }
            else
            {
              IsAdded = true;
            }

          }
        }

        return IsAdded;
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
    /// <summary>
    /// Inserting event in TblEvent while creating Event.
    /// </summary>
    /// <param name="pEvent"></param>
    /// <param name="pEntity"></param>
    /// <returns></returns>
    public tblEvent InsertTblEvent(EventDTO pEvent, AMS_DevpEntities pEntity)
    {
      try
      {
        tblEvent objEvent = new tblEvent();
        if (pEvent.EventId > 0)
        {
          objEvent = pEntity.tblEvents.Where(r => r.Id == pEvent.EventId).FirstOrDefault();
          objEvent.Id = pEvent.EventId;
          objEvent.UpdatedOn = DateTime.Now;
          objEvent.UpdatedBy = pEvent.UserId;
        }
        else
        {
          objEvent.CreatedOn = DateTime.Now;
          objEvent.CreatedBy = pEvent.UserId;
          objEvent.UserId = pEvent.UserId;
        }
       
        DateTime startDate = Convert.ToDateTime(pEvent.EventStartDate);
        DateTime endDate = Convert.ToDateTime(pEvent.EventEndDate);

        objEvent.EventName = pEvent.EventName.IfNotNullTrim();
        objEvent.Address = pEvent.Address.IfNotNullTrim();
        objEvent.Description = pEvent.Description.IfNotNullTrim();
        objEvent.CountryId = pEvent.CountryId;
        objEvent.StateId = pEvent.StateId;
        objEvent.CityId = pEvent.CityId;
        objEvent.EventTypeId = pEvent.EventTypeId;
        objEvent.TimeZoneId = pEvent.TimeZoneId;
        objEvent.EventStart = startDate;
        objEvent.EventEnd = endDate;
        objEvent.IsActive = true;
        objEvent.IsDeleted = false;
        objEvent.IsPrivate = pEvent.isPrivate;
        if (objEvent.TimeZoneId == 0)
        {
          objEvent.TimeZoneId = null;
        }

        return objEvent;
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
    /// <summary>
    /// Add/Edit Image functionality.
    /// </summary>
    /// <param name="pEvent"></param>
    /// <param name="pEntity"></param>
    /// <param name="pEventId"></param>
    public void InsertOrUpdateImage(EventDTO pEvent, AMS_DevpEntities pEntity, long pEventId)
    {
      CommonBL common = new CommonBL();

      string Path = common.GetSystemSetting(UtilityBL.SystemSettings.EVENT_PHOTO_FOLDER.ToString());

      string folderName = DateTime.Now.ToString("dd-MM-yyyy-hh-mm-ss") + "_" + DateTime.Now.Ticks.ToString();
      Path = Path + "/" + folderName.ToString() + "/";
      string filename = Guid.NewGuid().ToString();
      if (!Directory.Exists(Path))
      {
        Directory.CreateDirectory(HttpContext.Current.Server.MapPath(Path)); //Create directory if it doesn't exist
      }
      //generate unique name for select image 
      var imageName = filename + ".jpg";
      var thumbnailUrl = Path + imageName;

      //this will write(save) image to created folder.

      byte[] imageBytes = Convert.FromBase64String(pEvent.EventImage);

      // File.WriteAllBytes(thumbnailUrl, imageBytes);
      File.WriteAllBytes(HttpContext.Current.Server.MapPath(thumbnailUrl), imageBytes);

      //tbluserimages insert
      tblEventImage tblImg = new tblEventImage();
      tblImg.ThumbnailImage = folderName + "/" + imageName;
      tblImg.OriginalImage = tblImg.ThumbnailImage;
      tblImg.IsActive = true;
      tblImg.CreatedOn = DateTime.Now;
      tblImg.CreatedBy = pEvent.UserId;
      pEntity.tblEventImages.Add(tblImg);
      pEntity.SaveChanges();

      //tblEventImageMap
      tblEventImageMap tblUserImage = new tblEventImageMap();
      tblUserImage.ImageId = tblImg.EventImageId;
      tblUserImage.EventId = pEventId;
      tblUserImage.CreatedOn = DateTime.Now;
      tblUserImage.IsDefault = true;
      tblUserImage.CreatedBy = pEvent.UserId;

      pEntity.tblEventImageMaps.Add(tblUserImage);
      pEntity.SaveChanges();

    }
    /// <summary>
    /// Inserting Artist in EventArtistmap table while creating Event.
    /// </summary>
    /// <param name="pEntity"></param>
    /// <param name="pEventId"></param>
    /// <param name="pEvent"></param>
    public void InsertArtist(AMS_DevpEntities pEntity, long pEventId, EventDTO pEvent)
    {
      try
      {
        List<tblEventArtistMap> tblEventArt = new List<tblEventArtistMap>();

        foreach (Artists item in pEvent.ArtistsList)
        {

          List<tblEventArtistMap> imageMap = pEntity.tblEventArtistMaps.Where(r => r.EventId == pEvent.EventId && r.ArtistUserId == item.ArtistUserId).ToList();

          if (imageMap.Count > 0)
          {

            pEntity.tblEventArtistMaps.RemoveRange(imageMap);
            pEntity.SaveChanges();
          }
          //if (item.IsRemoved)
          //{
          //    tblArtistSchedule artist = pEntity.tblArtistSchedules.Where(r => r.Id == item.ScheduleId).FirstOrDefault();
          //    artist.Status = item.Status.IfNotNullTrim().ToUpper();
          //    artist.UpdatedOn = DateTime.Now;
          //    artist.UpdatedBy = pEvent.UserId;
          //    pEntity.SaveChanges();
          //}item.ArtistCategoryId.FirstOrDefault()
          //else
          //{
          foreach (int category in item.ArtistCategoryId)
          {
            tblEventArtistMap eventArtist = new tblEventArtistMap();
            eventArtist.ArtistCategoryId = category;
            eventArtist.ArtistUserId = item.ArtistUserId;
            eventArtist.EventId = pEventId;
            // eventArtist.ScheduleId = item.ScheduleId;
            //eventArtist.Status = item.Status.IfNotNullTrim().ToUpper();
            eventArtist.CreatedOn = DateTime.Now;
            eventArtist.CreatedBy = pEvent.UserId;
            tblEventArt.Add(eventArtist);
          }


          //tblArtistSchedule artist1 = pEntity.tblArtistSchedules.Where(r => r.Id == item.ScheduleId).FirstOrDefault();
          //artist1.Status = item.Status.IfNotNullTrim().ToUpper();
          //artist1.UpdatedOn = DateTime.Now;
          //artist1.UpdatedBy = pEvent.UserId;
          //pEntity.SaveChanges();
          //}
        }

        pEntity.tblEventArtistMaps.AddRange(tblEventArt);
        pEntity.SaveChanges();
      }

      catch (Exception ex)
      {
        throw ex;
      }
    }
    /// <summary>
    /// Add and edit functionality of Event.
    /// </summary>
    /// <param name="pEvent"></param>
    /// <param name="isFromTest"></param>
    /// <returns></returns>
    public long AddUpdateEvent(EventDTO pEvent, bool isFromTest)
    {
      long insertedEventId = 0;
      try
      {
        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
          using (var transaction = entity.Database.BeginTransaction())
          {
            try
            {
              if (pEvent.IsVisibility == true)
              {
                tblEvent tblEvent = entity.tblEvents.Where(x => x.Id == pEvent.EventId).FirstOrDefault();
                tblEvent.IsPrivate = pEvent.isPrivate;

                entity.SaveChanges();
                insertedEventId = pEvent.EventId;
                transaction.Commit();
              }
              else
              {
                tblMstCountry country = entity.tblMstCountries.Where(r => r.CountryCode == pEvent.CountryCode.ToUpper()).FirstOrDefault();
                tblMstState state = entity.tblMstStates.Where(r => r.StateCode == pEvent.StateCode.ToUpper()).FirstOrDefault();
                if (country != null && state != null)
                {
                  pEvent.CountryId = country.Id;
                  pEvent.StateId = state.Id;
                }

                CommonBL common = new CommonBL();
                if (pEvent.EventId > 0)
                {
                  //Quick Access to public/private event change

                  //else
                  //{
                  bool validate = Validate(pEvent);


                  if (!string.IsNullOrWhiteSpace(pEvent.EventImage))
                  {
                    if (validate)
                    {
                      tblEvent objEvent = InsertTblEvent(pEvent, entity);

                      entity.SaveChanges();

                      if (pEvent.IsEventImageUpdated == true)
                      {
                        if (!isFromTest)
                        {

                          tblEventImageMap imageMap = entity.tblEventImageMaps.Where(r => r.EventId == pEvent.EventId && r.IsDefault == true).FirstOrDefault();
                          long? imgId = 0;
                          if (imageMap != null)
                          {
                            imgId = imageMap.ImageId;
                            entity.tblEventImageMaps.Remove(imageMap);
                            entity.SaveChanges();
                          }

                          if (imgId > 0)
                          {
                            tblEventImage userImage = entity.tblEventImages.Where(s => s.EventImageId == imgId).FirstOrDefault();
                            if (userImage != null)
                            {
                              entity.tblEventImages.Remove(userImage);
                              entity.SaveChanges();
                            }
                          }

                          InsertOrUpdateImage(pEvent, entity, pEvent.EventId);
                        }
                      }
                      //  first delete performing artist
                      List<tblEventArtistMap> eventArtists = entity.tblEventArtistMaps.Where(r => r.EventId == pEvent.EventId).ToList();
                      if (eventArtists.Count > 0)
                      {
                        entity.tblEventArtistMaps.RemoveRange(eventArtists);
                        entity.SaveChanges();
                      }
                      if (pEvent.ArtistsList != null)
                      {
                        InsertArtist(entity, pEvent.EventId, pEvent);

                        insertedEventId = pEvent.EventId;
                        // UpdateAristSchedule(entity, pEvent);
                        transaction.Commit();
                        //if (isAdded == true)
                        //{
                        //  insertedEventId = objEvent.Id;
                        //  transaction.Commit();
                        //}
                        //else
                        //{
                        //  transaction.Rollback();
                        //}

                      }
                      else
                      {
                        insertedEventId = pEvent.EventId;
                        transaction.Commit();
                      }

                    }
                    else
                    {
                      insertedEventId = 0;
                    }
                  }

                  //}
                  //Update Event
                  //  insertedEventId = pEvent.EventId;

                }
                else
                {
                  if (!string.IsNullOrWhiteSpace(pEvent.EventName) || !string.IsNullOrWhiteSpace(pEvent.EventStartDate) || !string.IsNullOrWhiteSpace(pEvent.EventEndDate) || pEvent.TimeZoneId > 0)
                  {
                    if (!string.IsNullOrWhiteSpace(pEvent.EventImage))
                    {
                      //Add Event
                      long eventId = 0;
                      bool validate = Validate(pEvent);

                      if (validate)
                      {
                        tblEvent objEvent = InsertTblEvent(pEvent, entity);

                        entity.tblEvents.Add(objEvent);
                        entity.SaveChanges();

                        eventId = objEvent.Id;

                        if (pEvent.IsEventImageUpdated == true)
                        {
                          if (!isFromTest)
                          {
                            InsertOrUpdateImage(pEvent, entity, eventId);
                          }
                        }

                        if (pEvent.ArtistsList != null)
                        {
                          InsertArtist(entity, eventId, pEvent);
                          //bool isAdded = UpdateAristSchedule(entity, pEvent);

                          //if (isAdded == true)
                          //{
                          //    insertedEventId = objEvent.Id;
                          //    transaction.Commit();
                          //}
                          //else
                          //{
                          //    transaction.Rollback();
                          //}
                          insertedEventId = objEvent.Id;
                          transaction.Commit();

                        }
                        else
                        {
                          insertedEventId = objEvent.Id;
                          transaction.Commit();
                        }
                      }
                    }
                  }
                }
              }

            }
            catch (Exception)
            {
              transaction.Rollback();
              throw;
            }
          }
          return insertedEventId;
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
    /// <summary>
    /// Inserting gallery images in gallery table.
    /// </summary>
    /// <param name="pEventImages"></param>
    /// <returns></returns>
    public bool AddGalleryImages(EventGallary pEventImages)
    {
      bool isSuccess = false;
      try

      {

        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
          using (var transaction = entity.Database.BeginTransaction())
          {
            try
            {

              CommonBL common = new CommonBL();

              string Path = common.GetSystemSetting(UtilityBL.SystemSettings.EVENT_GALLERY_FOLDER.ToString());

              string folderName = pEventImages.EventId.ToString();
              Path = Path + "/" + folderName.ToString() + "/";
              if (!Directory.Exists(Path))
              {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(Path)); //Create directory if it doesn't exist
              }

              List<tblEventImage> tblEventImg = new List<tblEventImage>();
              if (pEventImages.Images.Count > 0)
              {

                foreach (var eventImages in pEventImages.Images)
                {
                  string filename = Guid.NewGuid().ToString();

                  //generate unique name for select image 
                  var imageName = filename + ".jpg";
                  var thumbnailUrl = Path + imageName;

                  //this will write(save) image to created folder.

                  byte[] imageBytes = Convert.FromBase64String(eventImages.EventImageUrl);

                  File.WriteAllBytes(HttpContext.Current.Server.MapPath(thumbnailUrl), imageBytes);

                  //tbluserimages insert
                  tblEventImage tblImg = new tblEventImage();
                  tblImg.ThumbnailImage = folderName + "/" + imageName;
                  tblImg.OriginalImage = tblImg.ThumbnailImage;
                  tblImg.IsActive = false;
                  tblImg.CreatedOn = DateTime.Now;
                  tblImg.CreatedBy = pEventImages.UserId;
                  tblEventImg.Add(tblImg);
                }

                entity.tblEventImages.AddRange(tblEventImg);
                entity.SaveChanges();
              }

              List<tblEventImageMap> tblImgMap = new List<tblEventImageMap>();
              if (tblImgMap != null)
              {

                foreach (var imgMap in tblEventImg)
                {
                  //tblEventImageMap
                  tblEventImageMap tblImage = new tblEventImageMap();
                  tblImage.ImageId = imgMap.EventImageId;
                  tblImage.EventId = pEventImages.EventId;
                  tblImage.CreatedOn = DateTime.Now;
                  tblImage.IsDefault = false;
                  tblImage.CreatedBy = pEventImages.UserId;
                  tblImgMap.Add(tblImage);
                }
                entity.tblEventImageMaps.AddRange(tblImgMap);
                entity.SaveChanges();
              }
              transaction.Commit();
              isSuccess = true;
              return isSuccess;

            }
            catch (Exception)
            {
              transaction.Rollback();
              throw;
            }
          }
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }

    }
    /// <summary>
    /// Fetching gallery Images of given Id.
    /// </summary>
    /// <param name="pId"></param>
    /// <returns></returns>
    public List<GallaryImages> GetGalleryImages(long pId)
    {
      try
      {
        List<GallaryImages> galleryImages = new List<GallaryImages>();
        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
          List<usp_GetGalleryImages_Result> imageList = entity.usp_GetGalleryImages(pId).ToList();
          if (imageList != null && imageList.Count > 0)
          {
            var config = new MapperConfiguration(cfg =>
            {
              cfg.CreateMap<usp_GetGalleryImages_Result, GallaryImages>();

            });

            IMapper mapper = config.CreateMapper();
            galleryImages = mapper.Map<List<usp_GetGalleryImages_Result>, List<GallaryImages>>(imageList);

          }
        }
        return galleryImages;
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
    /// <summary>
    /// Deleting gallery Images.
    /// </summary>
    /// <param name="pEventImages"></param>
    /// <returns></returns>
    public bool DeleteGalleryImages(List<GallaryImages> pEventImages)
    {
      bool isSuccess = false;
      try
      {
        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
          using (var transaction = entity.Database.BeginTransaction())
          {
            try
            {
              foreach (var gallaryImages in pEventImages)
              {
                tblEventImageMap userImage = entity.tblEventImageMaps.Where(s => s.ImageId == gallaryImages.ImageId && s.IsDefault == false && s.EventId == gallaryImages.EventId).FirstOrDefault();
                if (userImage != null)
                {
                  entity.tblEventImageMaps.Remove(userImage);
                  entity.SaveChanges();
                }

                tblEventImage image = entity.tblEventImages.Where(r => r.EventImageId == gallaryImages.ImageId).FirstOrDefault();
                if (image != null)
                {
                  entity.tblEventImages.Remove(image);
                  entity.SaveChanges();
                  string[] spearator = { "EventGallery" };
                  string[] imgUrl = gallaryImages.EventImageUrl.Split(spearator, StringSplitOptions.None);
                  string folderPath = imgUrl[1];
                  folderPath = "/EventGallery" + folderPath;
                  FileInfo file = new FileInfo(HttpContext.Current.Server.MapPath(folderPath));

                  if (file.Exists)
                  {
                    file.Delete();         // remove uploaded temparary file from temparary folder                  
                  }
                  isSuccess = true;
                }
              }

              transaction.Commit();
              return isSuccess;
            }
            catch (Exception)
            {
              transaction.Rollback();
              throw;
            }
          }
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }

    }

    //public List<UserAndGroup> AdvancedSearch()
    //     {

    //         try
    //         {
    //             using (AMS_DevpEntities entity = new AMS_DevpEntities())
    //             {
    //                 List<UserProfileDTO> userProfileDTOs = new List<UserProfileDTO>();
    //                 List<UserAndGroup> lstData = new List<UserAndGroup>();
    //                 List<tblUser> userArtist = entity.tblUsers.Where(x => x.IsActive == true).ToList();

    //                 tblSystemSetting tblSystemSetting = entity.tblSystemSettings.Where(x => x.Name == "APP_URL").FirstOrDefault();
    //                 tblSystemSetting tblSystemprofile = entity.tblSystemSettings.Where(x => x.Name == "PROFILE_PHOTO_FOLDER").FirstOrDefault();

    //                 foreach (tblUser Artist in userArtist)
    //                 {
    //                     if (Artist.UserType == "Artist")
    //                     {
    //                         tblUserImageMap tblUserImageMap = entity.tblUserImageMaps.Where(x => x.UserId == Artist.Id).FirstOrDefault();

    //                         if (tblUserImageMap != null)
    //                         {
    //                             tblUserImage tblUserImage = entity.tblUserImages.Where(x => x.UserImageId == tblUserImageMap.ImageId).FirstOrDefault();
    //                             if (tblUserImage != null)
    //                             {
    //                                 UserAndGroup userAndGroup = new UserAndGroup();
    //                                 userAndGroup.Name = Artist.FirstName + " " + Artist.MiddleName + " " + Artist.LastName;
    //                                 userAndGroup.Id = Artist.Id;
    //                                 userAndGroup.ProfileImageURL = tblSystemSetting.Value + tblSystemprofile.Value + '/' + tblUserImage.OriginalImage;
    //                                 userAndGroup.Group = Artist.UserType;
    //                                 lstData.Add(userAndGroup);
    //                             }

    //                         }


    //                     }
    //                     else if (Artist.UserType == "Organizer")
    //                     {
    //                         tblUserImageMap tblUserImageMap = entity.tblUserImageMaps.Where(x => x.UserId == Artist.Id).FirstOrDefault();
    //                         if (tblUserImageMap != null)
    //                         {
    //                             tblUserImage tblUserImage = entity.tblUserImages.Where(x => x.UserImageId == tblUserImageMap.ImageId).FirstOrDefault();
    //                             if (tblUserImage != null)
    //                             {
    //                                 UserAndGroup userAndGroup = new UserAndGroup();
    //                                 userAndGroup.Name = Artist.FirstName + " " + Artist.MiddleName + " " + Artist.LastName;
    //                                 userAndGroup.Id = Artist.Id;
    //                                 userAndGroup.ProfileImageURL = tblSystemSetting.Value + tblSystemprofile.Value + '/' + tblUserImage.OriginalImage;
    //                                 userAndGroup.Group = Artist.UserType;
    //                                 lstData.Add(userAndGroup);
    //                             }

    //                         }

    //                     }

    //                 }

    //                 List<tblEvent> userEvent = entity.tblEvents.Where(x => x.EventEnd >= DateTime.Now && x.IsPrivate == false && x.IsDeleted == false).ToList();
    //                 foreach (tblEvent eventname in userEvent)
    //                 {
    //                     UserAndGroup userAndGroup = new UserAndGroup();
    //                     userAndGroup.Name = eventname.EventName;
    //                     userAndGroup.Id = eventname.Id;
    //                     userAndGroup.Group = "Event";
    //                     lstData.Add(userAndGroup);
    //                 }

    //                 return lstData;
    //             }


    //         }
    //         catch (Exception ex)
    //         {
    //             throw ex;
    //         }
    //     }
    /// <summary>
    /// Advanced Search functionality on names,desc,skills etc.
    /// </summary>
    /// <param name="pSearchTitle"></param>
    /// <returns></returns>
    public List<UserAndGroup> AdvancedSearch(string pSearchTitle)
    {
      try
      {
        List<UserAndGroup> searchData = new List<UserAndGroup>();
        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
          if (!string.IsNullOrWhiteSpace(pSearchTitle))
          {
            List<usp_GetEventBySearchTitle_Result> evenList = entity.usp_GetEventBySearchTitle(pSearchTitle.IfNotNullTrim()).ToList();
            if (evenList.Count > 0)
            {
              var config = new MapperConfiguration(cfg =>
              {
                cfg.CreateMap<usp_GetEventBySearchTitle_Result, UserAndGroup>();

              });

              IMapper mapper = config.CreateMapper();
              searchData = mapper.Map<List<usp_GetEventBySearchTitle_Result>, List<UserAndGroup>>(evenList);
            }

            List<usp_GetUserBySearchTitle_Result> userList = entity.usp_GetUserBySearchTitle(pSearchTitle.IfNotNullTrim()).ToList();

            if (userList.Count > 0)
            {
              foreach (usp_GetUserBySearchTitle_Result user in userList)
              {
                UserAndGroup userData = new UserAndGroup();
                userData.Id = user.Id;
                userData.Name = user.Name;
                userData.Group = user.Group;
                userData.ProfileImageURL = user.ProfileImageURL;
                searchData.Add(userData);
              }
            }

          }
          else
          {
            List<usp_GetUserBySearchTitle_Result> userList = entity.usp_GetUserBySearchTitle(string.Empty).ToList();
            if (userList.Count > 0)
            {
              var config = new MapperConfiguration(cfg =>
              {
                cfg.CreateMap<usp_GetUserBySearchTitle_Result, UserAndGroup>();

              });

              IMapper mapper = config.CreateMapper();
              searchData = mapper.Map<List<usp_GetUserBySearchTitle_Result>, List<UserAndGroup>>(userList);
            }

            List<tblEvent> userEvent = entity.tblEvents.Where(x => x.EventEnd >= DateTime.Now && x.IsPrivate == false && x.IsDeleted == false).ToList();

            if (userEvent.Count > 0)
            {
              foreach (tblEvent eventname in userEvent)
              {
                UserAndGroup userAndGroup = new UserAndGroup();
                userAndGroup.Name = eventname.EventName;
                userAndGroup.Id = eventname.Id;
                userAndGroup.Group = "Event";
                searchData.Add(userAndGroup);
              }
            }
          }
          return searchData;
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
    /// <summary>
    /// Not in use Artist schedule.
    /// </summary>
    /// <param name="pEntity"></param>
    /// <param name="pEvent"></param>
    /// <returns></returns>
    public bool UpdateAristSchedule(AMS_DevpEntities pEntity, EventDTO pEvent)
    {
      try
      {
        bool isAdded = false;
        foreach (var artistList in pEvent.ArtistsList)
        {
          if (artistList.AvailableFrom != null && artistList.AvailableTo != null)
          {

            List<tblArtistSchedule> userArtist = pEntity.tblArtistSchedules.Where(x => x.ArtistId == artistList.ArtistUserId).ToList();
            if (userArtist.Count > 0)
            {

              foreach (var artist in userArtist)
              {
                string dbAvailablefrom = Convert.ToDateTime(artist.AvailableFrom).ToString("yyyy-MM-dd HH:mm:ss");
                string dbAvailableTo = Convert.ToDateTime(artist.AvailableTo).ToString("yyyy-MM-dd HH:mm:ss");
                string availableTo = Convert.ToDateTime(artistList.AvailableTo).ToString("yyyy-MM-dd HH:mm:ss");
                string availableFrom = Convert.ToDateTime(artistList.AvailableFrom).ToString("yyyy-MM-dd HH:mm:ss");
                if (dbAvailablefrom == availableFrom && dbAvailableTo == availableTo)
                {

                  artist.Status = "BUSY";
                  pEntity.SaveChanges();
                  isAdded = true;

                }
              }
            }
          }
        }
        return isAdded;
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
    /// <summary>
    /// Displaying Event List of given organizer id.
    /// </summary>
    /// <param name="pSearchOrganizer"></param>
    /// <returns></returns>
    public UserDetailDTO GetEventsByOrganizerId(SearchArtistDTO pSearchOrganizer)
    {

      try
      {
        UserDetailDTO objEvent = null;
        //usp_GetUserDetails_Result
        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
          usp_GetUserDetailsWithRatings_Result sourceDetails = entity.usp_GetUserDetailsWithRatings(pSearchOrganizer.UserId).FirstOrDefault();

          if (sourceDetails != null)
          {
            var config = new MapperConfiguration(cfg =>
            {
              cfg.CreateMap<usp_GetUserDetailsWithRatings_Result, UserDetailDTO>();
            });

            IMapper mapper = config.CreateMapper();
            objEvent = mapper.Map<usp_GetUserDetailsWithRatings_Result, UserDetailDTO>(sourceDetails);
          }
          //string Todaydate = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd");
          //Convert.ToDateTime(pArtists.AvailableTo).ToString("yyyy-MM-dd");
          List<DashboardDTO> OrganizerList = new List<DashboardDTO>();
          List<DashboardDTO> OrganizerSearchdata = new List<DashboardDTO>();
          if (pSearchOrganizer.CategoryId.Length > 0)
          {
            foreach (int item in pSearchOrganizer.CategoryId)
            {
              //UserDetailDTO objData = null;
              List<usp_GetOrganizerEventswithSearchTitle_Result> artistsearch = entity.usp_GetOrganizerEventswithSearchTitle(pSearchOrganizer.SearchTitle, pSearchOrganizer.DateFormat.ToUpper(), item, pSearchOrganizer.UserId).Where(x=>x.EventImageUrl != null).GroupBy(x => x.Id).Select(x => x.FirstOrDefault()).ToList();

              if (artistsearch.Count > 0)
              {
                var config2 = new MapperConfiguration(cfg =>
                {
                  cfg.CreateMap<usp_GetOrganizerEventswithSearchTitle_Result, DashboardDTO>();
                });

                IMapper mapper2 = config2.CreateMapper();

                objEvent.EventList = mapper2.Map<List<usp_GetOrganizerEventswithSearchTitle_Result>, List<DashboardDTO>>(artistsearch);
                //foreach(usp_GetArtistEventswithSearchTitle_Result artistdata in artistsearch)
                //{
                foreach (DashboardDTO dashboardDTO in objEvent.EventList)
                {
                  OrganizerList.Add(dashboardDTO);

                }
                //}
                OrganizerSearchdata = OrganizerList.GroupBy(x => x.Id).Select(x => x.FirstOrDefault()).ToList();
              }
            }
            objEvent.EventList = new List<DashboardDTO>();
            foreach (DashboardDTO dashboardDTO in OrganizerSearchdata)
            {
              objEvent.EventList.Add(dashboardDTO);

            }
          }
          else
          {
            List<usp_GetOrganizerEventswithSearchTitle_Result> artist1search = entity.usp_GetOrganizerEventswithSearchTitle(pSearchOrganizer.SearchTitle, pSearchOrganizer.DateFormat.ToUpper(), null, pSearchOrganizer.UserId).Where(x => x.EventEnd >= DateTime.Now && x.EventImageUrl != null).GroupBy(x => x.Id).Select(x => x.FirstOrDefault()).ToList();
            if (artist1search.Count > 0)
            {
              var config2 = new MapperConfiguration(cfg =>
              {
                cfg.CreateMap<usp_GetOrganizerEventswithSearchTitle_Result, DashboardDTO>();
              });

              IMapper mapper2 = config2.CreateMapper();

              OrganizerList = mapper2.Map<List<usp_GetOrganizerEventswithSearchTitle_Result>, List<DashboardDTO>>(artist1search);
              objEvent.EventList = OrganizerList;
            }
          }
          //List<usp_GetEventDetailsByuserId_Result> artist = entity.usp_GetEventDetailsByuserId(pOrganizerId).Where(x => (x.EventEND).Date >=DateTime.Now.Date).OrderBy(x=>x.EventStart).ToList();
          //if (artist.Count > 0)
          //{
          //  var config1 = new MapperConfiguration(cfg =>
          //  {
          //    cfg.CreateMap<usp_GetEventDetailsByuserId_Result, DashboardDTO>();
          //  });

          //  IMapper mapper1 = config1.CreateMapper();

          //  OrganizerList = mapper1.Map<List<usp_GetEventDetailsByuserId_Result>, List<DashboardDTO>>(artist);
          //  objEvent.EventList = OrganizerList;
          //}
          return objEvent;
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }

    }
    /// <summary>
    /// Deleting Event on given event id and some other info also.
    /// </summary>
    /// <param name="pArtist"></param>
    /// <returns></returns>
    public bool DeleteEventByEventId(ScheduledEventsDTO pArtist)
    {
      try
      {
        bool isDeleted = false;
        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
          using (var transaction = entity.Database.BeginTransaction())
          {
            try
            {

              tblEvent tblEvent = entity.tblEvents.Where(x => x.Id == pArtist.EventId).FirstOrDefault();
              if (tblEvent != null)
              {
                tblEvent.IsDeleted = true;
                entity.SaveChanges();
                isDeleted = true;
                if (pArtist.ArtistsList.Count > 0)
                {
                  foreach (var artistList in pArtist.ArtistsList)
                  {
                    List<tblEventArtistMap> tblEventArtistMap = entity.tblEventArtistMaps.Where(x => x.EventId == pArtist.EventId && x.ArtistUserId == artistList.id).ToList();
                    if (tblEventArtistMap != null)
                    {
                      entity.tblEventArtistMaps.RemoveRange(tblEventArtistMap);
                      entity.SaveChanges();
                      isDeleted = true;
                      //foreach (var eventmap in tblEventArtistMap)
                      //{

                      //tblArtistSchedule objArtistSchedule = entity.tblArtistSchedules.Where(x => x.Id == eventmap.ScheduleId).FirstOrDefault();
                      //if (objArtistSchedule != null)
                      //{
                      //  objArtistSchedule.Status = "AVAILABLE";
                      //  entity.SaveChanges();
                      //  isDeleted = true;
                      //}
                      //}
                    }

                  }
                }
                //else
                //{
                //  isDeleted = true;
                //}
              }


              transaction.Commit();
              return isDeleted;

            }
            catch (Exception)
            {
              transaction.Rollback();
              throw;
            }
          }

        }

      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
    /// <summary>
    /// Search functionality on country, city and state names.
    /// </summary>
    /// <param name="pSearchkey"></param>
    /// <returns></returns>
    public List<CountryStateCityDTO> countryStateCityDTOSearch(string pSearchkey)
    {
      try
      {
        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
          List<CountryStateCityDTO> countryStateCityDTOs = new List<CountryStateCityDTO>();
          List<tblMstCountry> mstCountry = entity.tblMstCountries.Where(x => x.CountryName.StartsWith(pSearchkey)).ToList();
          foreach (tblMstCountry country in mstCountry)
          {
            CountryStateCityDTO countryState = new CountryStateCityDTO();
            countryState.Id = (int)country.Id;
            countryState.CountryName = country.CountryName;
            countryState.CountryCode = country.CountryCode;
            countryStateCityDTOs.Add(countryState);

          }

          List<tblMstState> mstStates = entity.tblMstStates.Where(x => x.StateName.StartsWith(pSearchkey)).ToList();
          foreach (tblMstState states in mstStates)
          {
            List<tblMstCountry> countryId = entity.tblMstCountries.Where(x => x.Id == states.CountryId).ToList();

            foreach (tblMstCountry countryname in countryId)
            {
              CountryStateCityDTO countryState = new CountryStateCityDTO();
              countryState.Id = (int)states.Id;
              countryState.CountryName = states.StateName + "," + countryname.CountryName;
              countryState.CountryCode = states.StateCode;
              countryStateCityDTOs.Add(countryState);
            }

          }

          List<CountryWiseSearch_Result> mstCities = entity.CountryWiseSearch(pSearchkey).ToList();
          foreach (CountryWiseSearch_Result cities in mstCities)
          {

            CountryStateCityDTO countryState = new CountryStateCityDTO();
            countryState.Id = (int)cities.CityId;
            countryState.CountryName = cities.Name;
            countryState.CountryCode = Convert.ToString(cities.CityCode);
            countryStateCityDTOs.Add(countryState);


          }

          return countryStateCityDTOs;
        }

      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    //public UserDetailDTO GetEventByArtistId(int pArtistId)
    //{
    //    try
    //    {
    //        UserDetailDTO objEvent = null;

    //        using (AMS_DevpEntities entity = new AMS_DevpEntities())
    //        {
    //            usp_GetUserDetails_Result sourceDetails = entity.usp_GetUserDetails(pArtistId).FirstOrDefault();

    //            if (sourceDetails != null)
    //            {
    //                var config = new MapperConfiguration(cfg =>
    //                {
    //                    cfg.CreateMap<usp_GetUserDetails_Result, UserDetailDTO>();
    //                });

    //                IMapper mapper = config.CreateMapper();
    //                objEvent = mapper.Map<usp_GetUserDetails_Result, UserDetailDTO>(sourceDetails);
    //            }

    //            List<DashboardDTO> ArtistList = new List<DashboardDTO>();
    //            List<usp_GetArtistSchedule_Result> artist = entity.usp_GetArtistSchedule(pArtistId).Where(x => x.EventEnd >= DateTime.Now).ToList();
    //            if (artist.Count > 0)
    //            {
    //                var config1 = new MapperConfiguration(cfg =>
    //                {
    //                    cfg.CreateMap<usp_GetArtistSchedule_Result, DashboardDTO>();
    //                });

    //                IMapper mapper1 = config1.CreateMapper();

    //                ArtistList = mapper1.Map<List<usp_GetArtistSchedule_Result>, List<DashboardDTO>>(artist);
    //                objEvent.EventList = ArtistList;


    //            }
    //            return objEvent;

    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //}
    /// <summary>
    /// Displaying EventList of Given Artist Id along with some artist details.
    /// </summary>
    /// <param name="searchArtist"></param>
    /// <returns></returns>
    public UserDetailDTO GetEventByArtistId(SearchArtistDTO searchArtist)
    {
      try
      {
        //usp_GetUserDetails_Result
        UserDetailDTO objEvent = new UserDetailDTO();

        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
          usp_GetUserDetailsWithRatings_Result sourceDetails = entity.usp_GetUserDetailsWithRatings(searchArtist.UserId).FirstOrDefault();

          if (sourceDetails != null)
          {
            var config = new MapperConfiguration(cfg =>
            {
              cfg.CreateMap<usp_GetUserDetailsWithRatings_Result, UserDetailDTO>();
            });

            IMapper mapper = config.CreateMapper();
            objEvent = mapper.Map<usp_GetUserDetailsWithRatings_Result, UserDetailDTO>(sourceDetails);
          }

          List<DashboardDTO> ArtistList = new List<DashboardDTO>();
          List<DashboardDTO> eventList = new List<DashboardDTO>();
          List<DashboardDTO> ArtistSearchdata = new List<DashboardDTO>();
          if (searchArtist.CategoryId.Length > 0)
          {
            foreach (int item in searchArtist.CategoryId)
            {
              //UserDetailDTO objData = null;
              List<usp_GetArtistEventswithSearchTitle_Result> artistsearch = entity.usp_GetArtistEventswithSearchTitle(searchArtist.SearchTitle, searchArtist.DateFormat.ToUpper(), item, searchArtist.UserId).Where(x=>x.EventImageUrl != null).GroupBy(x => x.Id).Select(x => x.FirstOrDefault()).ToList();

              if (artistsearch.Count > 0)
              {
                var config2 = new MapperConfiguration(cfg =>
                {
                  cfg.CreateMap<usp_GetArtistEventswithSearchTitle_Result, DashboardDTO>();
                });

                IMapper mapper2 = config2.CreateMapper();

                objEvent.EventList = mapper2.Map<List<usp_GetArtistEventswithSearchTitle_Result>, List<DashboardDTO>>(artistsearch);
                //foreach(usp_GetArtistEventswithSearchTitle_Result artistdata in artistsearch)
                //{
                foreach (DashboardDTO dashboardDTO in objEvent.EventList)
                {
                  ArtistList.Add(dashboardDTO);

                }
                //}
                ArtistSearchdata = ArtistList.GroupBy(x => x.Id).Select(x => x.FirstOrDefault()).ToList();
              }
            }
            objEvent.EventList = new List<DashboardDTO>();
            foreach (DashboardDTO dashboardDTO in ArtistSearchdata)
            {
              objEvent.EventList.Add(dashboardDTO);

            }
          }
          else
          {
            List<usp_GetArtistEventswithSearchTitle_Result> artist1search = entity.usp_GetArtistEventswithSearchTitle(searchArtist.SearchTitle, searchArtist.DateFormat.ToUpper(), null, searchArtist.UserId).Where(x => x.EventEnd >= DateTime.Now && x.EventImageUrl != null).GroupBy(x => x.Id).Select(x => x.FirstOrDefault()).ToList();
            if (artist1search.Count > 0)
            {
              var config2 = new MapperConfiguration(cfg =>
              {
                cfg.CreateMap<usp_GetArtistEventswithSearchTitle_Result, DashboardDTO>();
              });

              IMapper mapper2 = config2.CreateMapper();

              ArtistList = mapper2.Map<List<usp_GetArtistEventswithSearchTitle_Result>, List<DashboardDTO>>(artist1search);
              objEvent.EventList = ArtistList;
            }
          }
          List<ArtistCategoryDTO> ArtistCategoryList = new List<ArtistCategoryDTO>();
          List<tblUserCategoryMapping> Objartistcategory = entity.tblUserCategoryMappings.Where(x => x.UserId == objEvent.Id).ToList();

          if (Objartistcategory.Count > 0)
          {
            foreach (tblUserCategoryMapping artistcategory in Objartistcategory)
            {
              tblMstArtistCategory artistCategories = entity.tblMstArtistCategories.Where(x => x.Id == artistcategory.ArtistCategoryId).FirstOrDefault();
              var config1 = new MapperConfiguration(cfg =>
              {
                cfg.CreateMap<tblMstArtistCategory, ArtistCategoryDTO>();
              });

              IMapper mapper1 = config1.CreateMapper();
              ArtistCategoryDTO ArtistCategory = new ArtistCategoryDTO();
              ArtistCategory = mapper1.Map<tblMstArtistCategory, ArtistCategoryDTO>(artistCategories);
              ArtistCategoryList.Add(ArtistCategory);
              objEvent.ArtistCategoryList = ArtistCategoryList;
            }

          }
          return objEvent;

        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
    /// <summary>
    /// Displaying all OrganizerList.
    /// </summary>
    /// <returns></returns>
    public List<UserDetailDTO> GetOrganizerList()
    {
      try
      {
        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
          List<UserDetailDTO> objEvent = new List<UserDetailDTO>();
          List<usp_GetOrganizerDetails_Result> sourceDetails = entity.usp_GetOrganizerDetails().Where(x => x.UserType == "Organizer").ToList();

          if (sourceDetails.Count > 0)
          {
            var config = new MapperConfiguration(cfg =>
            {
              cfg.CreateMap<usp_GetOrganizerDetails_Result, UserDetailDTO>();
            });

            IMapper mapper = config.CreateMapper();
            objEvent = mapper.Map<List<usp_GetOrganizerDetails_Result>, List<UserDetailDTO>>(sourceDetails);
          }
          return objEvent;

        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
    /// <summary>
    /// Displaying Registered ArtistLists.(not in use)
    /// </summary>
    /// <returns></returns>
    public List<UserDetailDTO> GetRegisteredArtistList()
    {
      try
      {
        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
          List<UserDetailDTO> objEvent = new List<UserDetailDTO>();
          List<usp_GetOrganizerDetails_Result> sourceDetails = entity.usp_GetOrganizerDetails().Where(x => x.UserType == "Artist").ToList();

          if (sourceDetails.Count > 0)
          {
            var config = new MapperConfiguration(cfg =>
            {
              cfg.CreateMap<usp_GetOrganizerDetails_Result, UserDetailDTO>();
            });

            IMapper mapper = config.CreateMapper();
            objEvent = mapper.Map<List<usp_GetOrganizerDetails_Result>, List<UserDetailDTO>>(sourceDetails);
          }
          foreach (UserDetailDTO artistList in objEvent)
          {
            List<ArtistCategoryDTO> ArtistCategoryList = new List<ArtistCategoryDTO>();
            List<tblUserCategoryMapping> Objartistcategory = entity.tblUserCategoryMappings.Where(x => x.UserId == artistList.Id).ToList();

            if (Objartistcategory.Count > 0)
            {
              foreach (tblUserCategoryMapping artistcategory in Objartistcategory)
              {
                tblMstArtistCategory artistCategories = entity.tblMstArtistCategories.Where(x => x.Id == artistcategory.ArtistCategoryId).FirstOrDefault();
                var config1 = new MapperConfiguration(cfg =>
                {
                  cfg.CreateMap<tblMstArtistCategory, ArtistCategoryDTO>();
                });

                IMapper mapper1 = config1.CreateMapper();
                ArtistCategoryDTO ArtistCategory = new ArtistCategoryDTO();
                ArtistCategory = mapper1.Map<tblMstArtistCategory, ArtistCategoryDTO>(artistCategories);
                ArtistCategoryList.Add(ArtistCategory);
                artistList.ArtistCategoryList = ArtistCategoryList;
                objEvent.Append(artistList);
              }

            }
            List<ArtistSkillsDTO> ArtistSkills = new List<ArtistSkillsDTO>();
            List<tblArtistSkillsMapping> artistskills = entity.tblArtistSkillsMappings.Where(x => x.UserId == artistList.Id).ToList();

            if (artistskills.Count > 0)
            {
              foreach (tblArtistSkillsMapping artistskill in artistskills)
              {
                tblMstArtistSkill artistSkillsdata = entity.tblMstArtistSkills.Where(x => x.Id == artistskill.Skill_Id).FirstOrDefault();
                var config1 = new MapperConfiguration(cfg =>
                {
                  cfg.CreateMap<tblMstArtistSkill, ArtistSkillsDTO>();
                });

                IMapper mapper1 = config1.CreateMapper();
                ArtistSkillsDTO objSkills = new ArtistSkillsDTO();
                objSkills = mapper1.Map<tblMstArtistSkill, ArtistSkillsDTO>(artistSkillsdata);
                ArtistSkills.Add(objSkills);
                artistList.ArtistSkillsList = ArtistSkills;
                objEvent.Append(artistList);
              }

            }


          }

          //objEvent =artist;
          return objEvent;

        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
    /// <summary>
    /// Displaying EventList according to given EventcategoryId.
    /// </summary>
    /// <param name="pEventTypeId"></param>
    /// <returns></returns>
    public List<DashboardDTO> GetEventListByEventCategoryWise(int pEventTypeId, string pSearchTitle)
    {
      try
      {
        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
          List<DashboardDTO> objEvent = new List<DashboardDTO>();
          List<usp_GetEventDetailsData_Result> eventDetails = entity.usp_GetEventDetailsData().Where(x => x.EventTypeId == pEventTypeId && x.EventEND >= DateTime.Now && x.IsPrivate==false ).OrderBy(x => x.EventStart).ToList();
          if (pSearchTitle == null)
          {
            if (eventDetails.Count > 0)
            {
              var config = new MapperConfiguration(cfg =>
              {
                cfg.CreateMap<usp_GetEventDetailsData_Result, DashboardDTO>();
              });

              IMapper mapper = config.CreateMapper();
              objEvent = mapper.Map<List<usp_GetEventDetailsData_Result>, List<DashboardDTO>>(eventDetails);
            }
          }

          else
          {
            List<usp_EventBySearchTitle_Result> sourceDetails = entity.usp_EventBySearchTitle(pSearchTitle.IfNotNullTrim()).Where(x => x.EventTypeId == pEventTypeId && x.EventEnd >= DateTime.Now).ToList();

            if (sourceDetails.Count > 0)
            {
              var config = new MapperConfiguration(cfg =>
              {
                cfg.CreateMap<usp_EventBySearchTitle_Result, DashboardDTO>().
                              ForMember(dest => dest.EventName, opt => opt.MapFrom(src => src.Name));
              });

              IMapper mapper = config.CreateMapper();
              objEvent = mapper.Map<List<usp_EventBySearchTitle_Result>, List<DashboardDTO>>(sourceDetails);
            }
          }
          return objEvent;

        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    /// <summary>
    /// Search functionality on Events.
    /// </summary>
    /// <param name="pSearchTitle"></param>
    /// <returns></returns>
    public List<DashboardDTO> SearchEvent(string pSearchTitle)
    {
      try
      {
        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
          List<DashboardDTO> objEvent = new List<DashboardDTO>();
          List<usp_EventBySearchTitle_Result> sourceDetails = entity.usp_EventBySearchTitle(pSearchTitle.IfNotNullTrim()).ToList();

          if (sourceDetails.Count > 0)
          {
            var config = new MapperConfiguration(cfg =>
            {
              cfg.CreateMap<usp_EventBySearchTitle_Result, DashboardDTO>().
             ForMember(dest => dest.EventName, opt => opt.MapFrom(src => src.Name));
            });

            IMapper mapper = config.CreateMapper();
            objEvent = mapper.Map<List<usp_EventBySearchTitle_Result>, List<DashboardDTO>>(sourceDetails);
          }
          return objEvent;

        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
    /// <summary>
    /// Get Artist EventList of given skill id.
    /// </summary>
    /// <param name="pSkill_Id"></param>
    /// <returns></returns>
    public List<DashboardDTO> GetEventByArtistSkills(int pSkill_Id)
    {
      try
      {
        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
          List<DashboardDTO> objEvent = new List<DashboardDTO>();
          List<GetArtistEventsWithSkillId_Result> sourceDetails = entity.GetArtistEventsWithSkillId(pSkill_Id).Where(x => x.EventEND >= DateTime.Now).GroupBy(x=>x.Id).Select(x=>x.FirstOrDefault()).ToList();

          if (sourceDetails.Count > 0)
          {
            var config = new MapperConfiguration(cfg =>
            {
              cfg.CreateMap<GetArtistEventsWithSkillId_Result, DashboardDTO>();
                          //ForMember(dest => dest.EventName, opt => opt.MapFrom(src => src.Name));
                        });

            IMapper mapper = config.CreateMapper();
            objEvent = mapper.Map<List<GetArtistEventsWithSkillId_Result>, List<DashboardDTO>>(sourceDetails);
          }
          return objEvent;

        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    /// <summary>    /// get category list by artist Id     /// </summary>    /// <param name="Id"></param>    /// <returns></returns>    public List<ArtistCategoryDTO> GetCategoryListByUserId(long Id)
    {
      try
      {
        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
          List<ArtistCategoryDTO> category = new List<ArtistCategoryDTO>();
          List<tblUserCategoryMapping> userCategory = entity.tblUserCategoryMappings.Where(r => r.UserId == Id).ToList();

          if (userCategory.Count > 0)
          {
            var config = new MapperConfiguration(cfg =>
            {
              cfg.CreateMap<tblUserCategoryMapping, ArtistCategoryDTO>()
            .ForMember(dest => dest.ArtistCategoryName, opt => opt.MapFrom(src => src.tblMstArtistCategory.ArtistCategoryName))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.ArtistCategoryId));
            });

            IMapper mapper = config.CreateMapper();
            category = mapper.Map<List<tblUserCategoryMapping>, List<ArtistCategoryDTO>>(userCategory);
          }
          return category;
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
    /// <summary>
    /// get count of events for all event category
    /// </summary>
    /// <returns></returns>
    public List<EventCategoryDTO> GetEventCountByCategory()
    {
      try
      {
        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
          List<EventCategoryDTO> category = new List<EventCategoryDTO>();
          List<usp_GetEventCountByCategory_Result> eventCategory = entity.usp_GetEventCountByCategory().ToList();

          if (eventCategory.Count > 0)
          {
            var config = new MapperConfiguration(cfg =>
            {
              cfg.CreateMap<usp_GetEventCountByCategory_Result, EventCategoryDTO>();
            });

            IMapper mapper = config.CreateMapper();
            category = mapper.Map<List<usp_GetEventCountByCategory_Result>, List<EventCategoryDTO>>(eventCategory);
          }
          return category;
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    /// <summary>
    /// Get Event List for past Events with Date Filters for previous week and previous month
    /// </summary>
    /// <param name="eventList"></param>
    /// <returns></returns>
    public List<DashboardDTO> GetEventListForPastEvents(SearchArtistDTO pEventData)
    {
      try
      {
        //usp_GetEventListForPastEvents_Result
        // usp_GetEventList_Result or usp_GetEventListWithRatings_Result
        List<DashboardDTO> objDashboard = null;

        //ObjectParameter objParam = new ObjectParameter("RecordCount", typeof(int));
        List<DashboardDTO> OrganizerList = new List<DashboardDTO>();
        List<DashboardDTO> OrganizerSearchdata = new List<DashboardDTO>();
        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
          
          if (pEventData.CategoryId.Length > 0)
          {
            foreach(int categoryid in pEventData.CategoryId)
            {
             
              List<usp_GetPastEventsWithCategoryAndDateFilter_Result> sourcePersonDetails = entity.usp_GetPastEventsWithCategoryAndDateFilter(pEventData.SearchTitle, pEventData.DateFormat.ToUpper(), categoryid, pEventData.UserId,pEventData.Rating,pEventData.UserType.ToUpper()).Where(x=>x.EventImageUrl!=null).GroupBy(x => x.Id).Select(x => x.FirstOrDefault()).OrderByDescending(x=>x.EventEnd).ToList();
              if(sourcePersonDetails.Count>0)
              {
                var config = new MapperConfiguration(cfg =>
                {
                  cfg.CreateMap<usp_GetPastEventsWithCategoryAndDateFilter_Result, DashboardDTO>();
                });

                IMapper mapper = config.CreateMapper();

                objDashboard = mapper.Map<List<usp_GetPastEventsWithCategoryAndDateFilter_Result>, List<DashboardDTO>>(sourcePersonDetails);

              }
              foreach (DashboardDTO dashboardDTO in objDashboard)
              {
                OrganizerList.Add(dashboardDTO);

              }
              //}
              OrganizerSearchdata = OrganizerList.GroupBy(x => x.Id).Select(x => x.FirstOrDefault()).ToList();
            }
            
          }
          else
          {
            //send null
            List<usp_GetPastEventsWithCategoryAndDateFilter_Result> sourcePersonDetails = entity.usp_GetPastEventsWithCategoryAndDateFilter(pEventData.SearchTitle, pEventData.DateFormat.ToUpper(), null, pEventData.UserId, pEventData.Rating, pEventData.UserType.ToUpper()).Where(x => x.EventImageUrl != null).GroupBy(x => x.Id).Select(x => x.FirstOrDefault()).OrderByDescending(x=>x.EventEnd).ToList();
            // List<usp_GetEventListForPastEvents_Result> sourcePersonDetails = entity.usp_GetEventListForPastEvents(pEventData.SearchTitle.IfNotNullTrim().ToUpper(), pEventData.EventAttribute.IfNotNullTrim().ToUpper(), objParam, pEventData.UserId, pEventData.IsPrivate, pEventData.DateFilter.ToUpper(), pEventData.Type, pEventData.rating).GroupBy(x => x.Id).Select(x => x.FirstOrDefault()).ToList();
            if(sourcePersonDetails.Count>0)
            {
              var config = new MapperConfiguration(cfg =>
              {
                cfg.CreateMap<usp_GetPastEventsWithCategoryAndDateFilter_Result, DashboardDTO>();
              });

              IMapper mapper = config.CreateMapper();

              OrganizerSearchdata = mapper.Map<List<usp_GetPastEventsWithCategoryAndDateFilter_Result>, List<DashboardDTO>>(sourcePersonDetails);
            }            
          }
          //int objParam = 10;
         
        }
        return OrganizerSearchdata;
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

  }
}

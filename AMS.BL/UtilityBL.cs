using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS.BL
{
    public class UtilityBL
    {
        public enum SystemSettings
        {
            ENC_DEC_KEY,
            PROFILE_PHOTO_FOLDER,
            EVENT_PHOTO_FOLDER,
            EVENT_GALLERY_FOLDER
        }

        public struct ErrorMessages
        {
            public const string UNAUTHORIZED = "Unauthorized request";
            public const string INTERNAL_SERVER_ERROR = "Internal server error";
            public const string USER_NOT_FOUND = "User not found";
            public const string INVALID_LOGIN = "Invalid login details";
            public const string BAD_REQUEST = "Bad request";
            public const string EXTENDTICKET = "Ticket cannot be extended";
            public const string DATA_NOT_FOUND = "Data Not Fround";
            public const string ARTISTS_NOT_FOUND = "Performing artists are not added";
            public const string EMAIL_EXISTS = "Email id already exists";
            public const string EMAIL_DOESNOT_EXISTS = "Email id does not exist";
            public const string PARAMETER_MISSING = "Parameter missing";
            public const string IMAGES_NOT_FOUND = "Gallery images not found";
            public const string DUPLICATE_DATA = "Skill is already exists";
            public const string DATA_EXISTS = "Data is already exists";
    }
    }
}

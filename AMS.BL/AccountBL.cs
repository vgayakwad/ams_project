using AMS.DAL;
using AMS.Models;
using AMS.Models.Models;
using AMS.Utility;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Util;

namespace AMS.BL
{
  public class AccountBL : IAccount
  {

    /// <summary>
    /// Get event category og given event category id.
    /// </summary>
    /// <param name="pId"></param>
    /// <returns></returns>
    public EventCategoryDTO GetCategory(int pId)
    {
      try
      {
        EventCategoryDTO eventCategory = new EventCategoryDTO();
        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
          tblMstEventCategory category = new tblMstEventCategory();
          category = entity.tblMstEventCategories.Where(r => r.Id == pId).FirstOrDefault();
          eventCategory.Id = category.Id;
          eventCategory.EventCategoryName = category.EventCategoryName;
        }
        return eventCategory;
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }


    /// <summary>
    /// Generate and Add Access Token
    /// </summary>
    /// <param name="pId"></param>
    /// <returns></returns>
    public string AddAccessToken(long pId, AMS_DevpEntities pEntity)
    {
      try
      {
        string AccessToken;
        //tblUserSession userdetails = pEntity.tblUserSessions.Where(x => x.UserId == pId).FirstOrDefault();
        Guid accessToken = Guid.NewGuid();
        //if (userdetails == null)
        //{
        tblUserSession tblSession = new tblUserSession();
        tblSession.UserId = pId;
        tblSession.AccessToken = accessToken.ToString();
        pEntity.tblUserSessions.Add(tblSession);
        pEntity.SaveChanges();
        AccessToken = accessToken.ToString();
        // }
        //else
        //{
        //    AccessToken = userdetails.AccessToken = accessToken.ToString();
        //    pEntity.SaveChanges();
        //}
        return AccessToken;
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    /// <summary>
    /// Login User Method
    /// </summary>
    /// <param name="uname"></param>
    /// <param name="pPassword"></param>
    /// <returns></returns>
    public LoginResponse Authentication(string pEmail, string pPassword, string pProvider, bool isWebAPI)
    {
      try
      {
        LoginResponse response = new LoginResponse();
        CommonBL common = new CommonBL();
        if (!(string.IsNullOrWhiteSpace(pEmail)))
        {
          using (AMS_DevpEntities entity = new AMS_DevpEntities())
          {
            // pEmail = pEmail.IfNotNullTrim();
            // pPassword = pPassword.IfNotNullTrim();

            tblUser userdetails = new tblUser();
            //provider is empty or null then its a web site registration
            if (string.IsNullOrWhiteSpace(pProvider))
            {
              pPassword = common.EncryptString(pPassword.IfNotNullTrim());
              userdetails = entity.tblUsers.Where(x => x.Email == pEmail && x.Password == pPassword).FirstOrDefault();
            }
            else
            {
              userdetails = entity.tblUsers.Where(x => x.Email == pEmail && x.Provider == pProvider).FirstOrDefault();
            }
            if (userdetails != null)
            {
              string AccessToken = AddAccessToken(userdetails.Id, entity);

              var config = new MapperConfiguration(cfg =>
              {
                cfg.CreateMap<tblUser, LoginResponse>();

              });

              IMapper mapper = config.CreateMapper();
              response = mapper.Map<tblUser, LoginResponse>(userdetails);
              response.AccessToken = AccessToken;
              response.LoginMessage = "Success";
            }
            else
            {
              if (!isWebAPI)
              {
                response.LoginMessage = "Error";
              }
              else
              {
                response = null;
              }
            }
          }
        }
        else
        {
          response.LoginMessage = "Error";
        }
        return response;
      }
      catch (Exception ex)
      {
        throw ex;
      }

    }

    /// <summary>
    /// Register User
    /// </summary>
    /// <param name="pRegisterData"></param>
    /// <returns></returns>
    public bool Register(RegisterUserDTO pRegisterData)
    {
      try
      {
        bool isValid = false;

        CommonBL common = new CommonBL();
        if (!string.IsNullOrWhiteSpace(pRegisterData.Email) || !string.IsNullOrWhiteSpace(pRegisterData.FirstName) || !string.IsNullOrWhiteSpace(pRegisterData.UserType))
        {
          using (AMS_DevpEntities entity = new AMS_DevpEntities())
          {
            using (var transaction = entity.Database.BeginTransaction())
            {
              try
              {
                tblUser tbluser = new tblUser();

                tbluser = entity.tblUsers.Where(x => x.Email == pRegisterData.Email).FirstOrDefault();
                if (tbluser == null)
                {

                  tblUser tblModel = new tblUser();

                  tblModel.FirstName = pRegisterData.FirstName.IfNotNullTrim();
                  tblModel.MiddleName = pRegisterData.MiddleName.IfNotNullTrim();
                  tblModel.LastName = pRegisterData.LastName.IfNotNullTrim();
                  tblModel.Email = pRegisterData.Email.IfNotNullTrim();
                  if (!string.IsNullOrWhiteSpace(pRegisterData.Password))
                  {
                    tblModel.Password = common.EncryptString(pRegisterData.Password.IfNotNullTrim());
                  }
                  tblModel.IsActive = true;
                  tblModel.Provider = pRegisterData.Provider;
                  tblModel.CreatedOn = DateTime.Now;
                  tblModel.UpdatedOn = DateTime.Now;
                  tblModel.UserType = pRegisterData.UserType.IfNotNullTrim();
                  entity.tblUsers.Add(tblModel);
                  entity.SaveChanges();

                  AddAccessToken(tblModel.Id, entity);

                  tblModel.CreatedBy = tblModel.Id;
                  //tblModel.UpdatedBy = tblModel.Id;
                  entity.SaveChanges();

                  transaction.Commit();

                  isValid = true;
                }
                else
                {
                  if (string.IsNullOrWhiteSpace(pRegisterData.Provider))
                  {
                    isValid = false;
                  }
                  else
                  {
                    //update provider tbluser
                    //tbluser.Id = pRegisterData.Id;
                    tbluser.Provider = pRegisterData.Provider;

                    //entity.tblUsers.Add(tbluser);
                    entity.SaveChanges();
                    transaction.Commit();
                    isValid = true;

                  }

                }
              }
              catch (Exception)
              {
                transaction.Rollback();
                throw;
              }
            }
          }
        }
        return isValid;
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    /// <summary>
    /// Get  user details (used for unit testing)
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public bool GetData(UserProfileDTO pUserInput)
    {
      try
      {
        bool isValid = true;
        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
          tblUser db = new tblUser();

          db = entity.tblUsers.Where(x => x.Id == pUserInput.Id || x.FirstName == pUserInput.FirstName || x.LastName == pUserInput.LastName || x.PhoneNumber == pUserInput.PhoneNumber || x.Gender == pUserInput.Gender || x.CountryId == pUserInput.CountryId || x.StateId == pUserInput.StateId || x.CityId == pUserInput.CityId || x.Organization == pUserInput.Organization || x.ContactPerson == pUserInput.ContactPerson || x.ContactEmail == pUserInput.ContactEmail).FirstOrDefault();
          if (db == null)
          {
            isValid = false;
          }
        }
        return isValid;
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    /// <summary>
    ///  BL for updating Artist Profile
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    public bool UpdateArtistProfile(UserProfileDTO pUserInput, bool isWebAPI)
    {
      try
      {
        bool isValid = false;

        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
          using (var transaction = entity.Database.BeginTransaction())
          {
            try
            {
              tblUser db = new tblUser();
              CommonBL common = new CommonBL();

              tblUser userData = entity.tblUsers.Where(x => x.Id == pUserInput.Id).FirstOrDefault();
              if (userData != null)
              {
                tblMstCountry country = entity.tblMstCountries.Where(r => r.CountryCode == pUserInput.CountryCode.ToUpper()).FirstOrDefault();

                tblMstState state = entity.tblMstStates.Where(r => r.StateCode == pUserInput.StateCode.ToUpper()).FirstOrDefault();

                if (country != null && state != null)
                {
                  pUserInput.CountryId = country.Id;
                  pUserInput.StateId = state.Id;
                }
                if (!string.IsNullOrWhiteSpace(pUserInput.ProfileImage))
                {
                  bool res = common.ValidateUserCountry(pUserInput.Id, pUserInput.CountryId, pUserInput.StateId, pUserInput.CityId);
                  if (res == true)
                  {
                    bool artist = false;
                    if (pUserInput.UserType.ToLower() == "organizer")
                    {
                      artist = true;
                      userData.ArtistCategoryId = null;
                      pUserInput.ArtistSkills = null;
                      pUserInput.SkillsName = null;

                    }
                    else
                    {
                      //Convert.ToInt32(pUserInput.ArtistCategoryId);
                      artist = common.ValidateArtistCategory(pUserInput.ArtistCategoryId, pUserInput.Id);
                      userData.ArtistCategoryId = null;
                    }
                    if (artist == true)
                    {
                      userData.CityId = pUserInput.CityId;
                      userData.StateId = pUserInput.StateId;
                      userData.CountryId = pUserInput.CountryId;
                      userData.UserType = userData.UserType;
                      userData.Address = pUserInput.Address;
                      userData.Gender = pUserInput.Gender;
                      userData.BirthDate = Convert.ToDateTime(pUserInput.BirthDate);
                      userData.FirstName = pUserInput.FirstName.IfNotNullTrim();
                      userData.LastName = pUserInput.LastName.IfNotNullTrim();
                      userData.PhoneNumber = pUserInput.PhoneNumber.IfNotNullTrim();
                      userData.MiddleName = pUserInput.MiddleName.IfNotNullTrim();
                      userData.Description = pUserInput.Description.IfNotNullTrim();
                      userData.UpdatedOn = DateTime.Now;
                      userData.UpdatedBy = pUserInput.Id;
                      userData.Organization = pUserInput.Organization.IfNotNullTrim();
                      userData.ContactPerson = pUserInput.ContactPerson.IfNotNullTrim();
                      userData.ContactEmail = pUserInput.ContactEmail.IfNotNullTrim();

                      entity.SaveChanges();
                      if (pUserInput.ArtistCategoryId != null)
                      {
                        List<tblUserCategoryMapping> tbldata = new List<tblUserCategoryMapping>();
                        List<tblUserCategoryMapping> tblUserCategoryMapping = entity.tblUserCategoryMappings.Where(x => x.UserId == pUserInput.Id).ToList();
                        if (tblUserCategoryMapping.Count > 0)
                        {
                          entity.tblUserCategoryMappings.RemoveRange(tblUserCategoryMapping);
                          entity.SaveChanges();
                        }

                        foreach (int artistid in pUserInput.ArtistCategoryId)
                        {
                          tblUserCategoryMapping tblUserCategory = new tblUserCategoryMapping();
                          tblUserCategory.UserId = pUserInput.Id;
                          tblUserCategory.ArtistCategoryId = artistid;
                          tblUserCategory.CreatedBy = pUserInput.Id;
                          tblUserCategory.CreatedOn = DateTime.Now;
                          entity.tblUserCategoryMappings.Add(tblUserCategory);
                          entity.SaveChanges();
                        }
                      }
                      if (pUserInput.ArtistSkills != null)
                      {
                        tblArtistSkillsMapping ObjtblArtistSkills = new tblArtistSkillsMapping();
                        List<tblArtistSkillsMapping> tblArtist = entity.tblArtistSkillsMappings.Where(x => x.UserId == pUserInput.Id).ToList();
                        if (tblArtist.Count > 0)
                        {
                          entity.tblArtistSkillsMappings.RemoveRange(tblArtist);
                          entity.SaveChanges();
                        }
                        foreach (int artistskills in pUserInput.ArtistSkills)
                        {
                          ObjtblArtistSkills.UserId = pUserInput.Id;
                          ObjtblArtistSkills.Skill_Id = artistskills;
                          ObjtblArtistSkills.CreatedBy = pUserInput.Id;
                          ObjtblArtistSkills.CreatedOn = DateTime.Now;
                          entity.tblArtistSkillsMappings.Add(ObjtblArtistSkills);
                          entity.SaveChanges();
                        }

                      }
                      if (pUserInput.SkillsName != null)
                      {
                        foreach (var artistSkill in pUserInput.SkillsName)
                        {
                          tblMstArtistSkill tblMstArtist = entity.tblMstArtistSkills.Where(x => x.SkillsName.ToLower() == artistSkill.ToLower().ToString()).FirstOrDefault();

                          if (tblMstArtist == null)
                          {
                            tblMstArtistSkill tblMstArtistSkill = new tblMstArtistSkill();
                            //tblMstArtistSkill.Id = artistSkill.Id;
                            tblMstArtistSkill.SkillsName = artistSkill.ToString();
                            tblMstArtistSkill.CreatedOn = DateTime.Now;
                            tblMstArtistSkill.CreatedBy = pUserInput.Id;
                            entity.tblMstArtistSkills.Add(tblMstArtistSkill);
                            entity.SaveChanges();
                            int datalist = tblMstArtistSkill.Id;
                            tblArtistSkillsMapping tblArtistSkills = new tblArtistSkillsMapping();
                            tblArtistSkills.UserId = pUserInput.Id;
                            tblArtistSkills.Skill_Id = datalist;
                            tblArtistSkills.CreatedBy = pUserInput.Id;
                            tblArtistSkills.CreatedOn = DateTime.Now;
                            entity.tblArtistSkillsMappings.Add(tblArtistSkills);
                            entity.SaveChanges();
                            // int data = pUserInput.ArtistSkills.ElementAt(datalist);
                            // pUserInput.ArtistSkills.Append(data);

                          }
                          else
                          {
                            transaction.Rollback();
                            isValid = false;
                          }
                        }
                      }

                      if (isWebAPI)
                      {
                        if (pUserInput.IsProfileImageUpdated)
                        {
                          string Path = common.GetSystemSetting(UtilityBL.SystemSettings.PROFILE_PHOTO_FOLDER.ToString());
                          string folderName = DateTime.Now.ToString("dd-MM-yyyy-hh-mm-ss") + "_" + DateTime.Now.Ticks.ToString();
                          Path = Path + "/" + folderName.ToString() + "/";
                          string filename = Guid.NewGuid().ToString();
                          if (!Directory.Exists(Path))
                          {
                            Directory.CreateDirectory(HttpContext.Current.Server.MapPath(Path)); //Create directory if it doesn't exist
                          }
                          //generate unique name for select image 
                          var imageName = filename + ".jpg";
                          var thumbnailUrl = Path + imageName;

                          //this will write(save) image to created folder.

                          byte[] imageBytes = Convert.FromBase64String(pUserInput.ProfileImage);

                          // File.WriteAllBytes(thumbnailUrl, imageBytes);
                          File.WriteAllBytes(HttpContext.Current.Server.MapPath(thumbnailUrl), imageBytes);

                          tblUserImageMap imageMap = entity.tblUserImageMaps.Where(r => r.UserId == pUserInput.Id && r.IsDefault == true).FirstOrDefault();
                          long? imgId = 0;
                          if (imageMap != null)
                          {
                            imgId = imageMap.ImageId;
                            entity.tblUserImageMaps.Remove(imageMap);
                            entity.SaveChanges();
                          }

                          if (imgId > 0)
                          {
                            tblUserImage userImage = entity.tblUserImages.Where(s => s.UserImageId == imgId).FirstOrDefault();
                            if (userImage != null)
                            {
                              entity.tblUserImages.Remove(userImage);
                              entity.SaveChanges();
                            }
                          }

                          //tbluserimages insert
                          tblUserImage tblImg = new tblUserImage();
                          tblImg.ThumbnailImage = folderName + "/" + imageName;
                          tblImg.OriginalImage = tblImg.ThumbnailImage;
                          tblImg.IsActive = true;
                          tblImg.CreatedOn = DateTime.Now;
                          tblImg.CreatedBy = pUserInput.Id;
                          entity.tblUserImages.Add(tblImg);
                          entity.SaveChanges();

                          //tblUserimagemap
                          tblUserImageMap tblUserImage = new tblUserImageMap();
                          tblUserImage.ImageId = tblImg.UserImageId;
                          tblUserImage.UserId = pUserInput.Id;
                          tblUserImage.CreatedOn = DateTime.Now;
                          tblUserImage.IsDefault = true;
                          tblUserImage.CreatedBy = pUserInput.Id;

                          entity.tblUserImageMaps.Add(tblUserImage);
                          entity.SaveChanges();
                        }

                      }
                      isValid = true;
                      transaction.Commit();
                      // }

                      if (!isWebAPI)
                      {
                        isValid = GetData(pUserInput);
                      }
                    }
                  }
                }
              }
            }
            catch (Exception)
            {
              transaction.Rollback();
              throw;
            }
          }
        }
        return isValid;
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    /// <summary>
    /// Change Password if user forgot password
    /// </summary>
    /// <param name="id"></param>
    /// <param name="password"></param>
    /// <returns></returns>

    public bool ForgotPassword(ForgotPasswordDTO pPassword)
    {
      try
      {
        bool isValid = false;

        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
          if (pPassword != null)
          {
            CommonBL common = new CommonBL();
            tblUser tbluser = entity.tblUsers.Where(x => x.Email == pPassword.Email).FirstOrDefault();
            if (tbluser != null)
            {
              if (!string.IsNullOrWhiteSpace(pPassword.Password))
              {
                tbluser.Password = common.EncryptString(pPassword.Password.IfNotNullTrim());
                entity.SaveChanges();
                isValid = true;
              }
            }
          }

          return isValid;
        }

      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    /// <summary>
    /// Get User Data
    /// </summary>
    /// <param name="pId"></param>
    /// <returns></returns>
    public UserDetailDTO GetUserData(long pId)
    {
      try
      {
        UserDetailDTO desUser = new UserDetailDTO();
        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
          usp_GetUserDetails_Result srcUser = entity.usp_GetUserDetails(pId).FirstOrDefault();

          if (srcUser != null)
          {
            var config = new MapperConfiguration(cfg =>
            {
              cfg.CreateMap<usp_GetUserDetails_Result, UserDetailDTO>();

            });

            IMapper mapper = config.CreateMapper();
            desUser = mapper.Map<usp_GetUserDetails_Result, UserDetailDTO>(srcUser);
          }

          List<ArtistCategoryDTO> ArtistCategoryList = new List<ArtistCategoryDTO>();
          List<tblUserCategoryMapping> artist = entity.tblUserCategoryMappings.Where(x => x.UserId == desUser.Id).ToList();

          if (artist.Count > 0)
          {
            foreach (tblUserCategoryMapping artistcategory in artist)
            {
              tblMstArtistCategory artistCategories = entity.tblMstArtistCategories.Where(x => x.Id == artistcategory.ArtistCategoryId).FirstOrDefault();
              var config1 = new MapperConfiguration(cfg =>
              {
                cfg.CreateMap<tblMstArtistCategory, ArtistCategoryDTO>();
              });

              IMapper mapper1 = config1.CreateMapper();
              ArtistCategoryDTO ArtistCategory = new ArtistCategoryDTO();
              ArtistCategory = mapper1.Map<tblMstArtistCategory, ArtistCategoryDTO>(artistCategories);
              ArtistCategoryList.Add(ArtistCategory);
              desUser.ArtistCategoryList = ArtistCategoryList;
            }

          }
          List<ArtistSkillsDTO> ArtistSkills = new List<ArtistSkillsDTO>();
          List<tblArtistSkillsMapping> artistskills = entity.tblArtistSkillsMappings.Where(x => x.UserId == desUser.Id).ToList();

          if (artistskills.Count > 0)
          {
            foreach (tblArtistSkillsMapping artistskill in artistskills)
            {
              tblMstArtistSkill artistSkillsdata = entity.tblMstArtistSkills.Where(x => x.Id == artistskill.Skill_Id).FirstOrDefault();
              var config1 = new MapperConfiguration(cfg =>
              {
                cfg.CreateMap<tblMstArtistSkill, ArtistSkillsDTO>();
              });

              IMapper mapper1 = config1.CreateMapper();
              ArtistSkillsDTO objSkills = new ArtistSkillsDTO();
              objSkills = mapper1.Map<tblMstArtistSkill, ArtistSkillsDTO>(artistSkillsdata);
              ArtistSkills.Add(objSkills);
              desUser.ArtistSkillsList = ArtistSkills;
            }

          }
        }
        return desUser;
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
    /// <summary>
    /// Logout api
    /// </summary>
    /// <param name="pAccessToken"></param>
    /// <returns></returns>
    public bool Logout(string pAccessToken)
    {
      bool isSuccess = false;
      try
      {
        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
          tblUserSession userSession = entity.tblUserSessions.Where(r => r.AccessToken == pAccessToken).FirstOrDefault();
          if (userSession != null)
          {
            entity.tblUserSessions.Remove(userSession);
            entity.SaveChanges();
            isSuccess = true;

            if (HttpContext.Current.Session != null)
            {
              HttpContext.Current.Session.Clear();
              HttpContext.Current.Session.Abandon();
              HttpContext.Current.Session.RemoveAll();
            }
          }
        }
        return isSuccess;
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    public List<ArtistSkillsDTO> GetArtistSkills()
    {
      try
      {
        List<ArtistSkillsDTO> artistSkillsDTOs = new List<ArtistSkillsDTO>();
        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
          List<tblMstArtistSkill> artistSkills = entity.tblMstArtistSkills.ToList();
          if (artistSkills != null && artistSkills.Count > 0)
          {
            var config = new MapperConfiguration(cfg =>
            {
              cfg.CreateMap<tblMstArtistSkill, ArtistSkillsDTO>();

            });

            IMapper mapper = config.CreateMapper();
            artistSkillsDTOs = mapper.Map<List<tblMstArtistSkill>, List<ArtistSkillsDTO>>(artistSkills);

          }
        }
        return artistSkillsDTOs;
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
    /// <summary>
    /// get list or all artists as per search text(not in use)
    /// </summary>
    /// <param name="pSearchTitle"></param>
    /// <returns></returns>
    public List<UserDetailDTO> SearchArtist(string pSearchTitle)
    {
      try
      {
        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
          List<UserDetailDTO> objEvent = new List<UserDetailDTO>();
          List<usp_GetUserBySearchTitle_Result> sourceDetails = entity.usp_GetUserBySearchTitle(pSearchTitle.IfNotNullTrim()).Where(r => r.Group == "Artist").ToList();

          if (sourceDetails.Count > 0)
          {
            var config = new MapperConfiguration(cfg =>
            {
              cfg.CreateMap<usp_GetUserBySearchTitle_Result, UserDetailDTO>();
            });

            IMapper mapper = config.CreateMapper();
            objEvent = mapper.Map<List<usp_GetUserBySearchTitle_Result>, List<UserDetailDTO>>(sourceDetails);
          }
          return objEvent;

        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
    /// <summary>
    /// get list of organizers,Artists and also data as per searchtext on basis of desc,skills etc.
    /// </summary>
    /// <param name="pSearchTitle"></param>
    /// <param name="UserType"></param>
    /// <returns></returns>
    public List<UserDetailDTO> SearchUser(string pSearchTitle, string pUserType)
    {
      try
      {
        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
          List<UserDetailDTO> objEvent = new List<UserDetailDTO>();
          List<usp_GetUserSkillsBySearchTitle_Result> sourceDetails = entity.usp_GetUserSkillsBySearchTitle(pSearchTitle.IfNotNullTrim()).Where(r => r.Group == pUserType).GroupBy(x => x.Id).Select(x => x.FirstOrDefault()).ToList();
          //List<usp_GetUserBySearchTitle_Result> sourceDetails = entity.usp_GetUserBySearchTitle(pSearchTitle.IfNotNullTrim()).Where(r => r.Group == UserType).ToList();
          if (sourceDetails.Count > 0)
          {
           
            var config = new MapperConfiguration(cfg =>
            {
              cfg.CreateMap<usp_GetUserSkillsBySearchTitle_Result, UserDetailDTO>()
              .ForMember(dest => dest.UserType, opt => opt.MapFrom(src => src.Group));
            });

            IMapper mapper = config.CreateMapper();
            objEvent = mapper.Map<List<usp_GetUserSkillsBySearchTitle_Result>, List<UserDetailDTO>>(sourceDetails);
          }
          if (pUserType == "Artist")
          {
            foreach (UserDetailDTO artistList in objEvent)
            {
              usp_GetUserDetailsWithRatings_Result userRating = entity.usp_GetUserDetailsWithRatings(artistList.Id).FirstOrDefault();
              artistList.AverageRating = (float)userRating.AverageRating;
              artistList.CustomerRating = (int)userRating.CustomerRating;


              List<ArtistCategoryDTO> ArtistCategoryList = new List<ArtistCategoryDTO>();
              List<tblUserCategoryMapping> Objartistcategory = entity.tblUserCategoryMappings.Where(x => x.UserId == artistList.Id).ToList();

              if (Objartistcategory.Count > 0)
              {
                foreach (tblUserCategoryMapping artistcategory in Objartistcategory)
                {
                  tblMstArtistCategory artistCategories = entity.tblMstArtistCategories.Where(x => x.Id == artistcategory.ArtistCategoryId).FirstOrDefault();
                  var config1 = new MapperConfiguration(cfg =>
                  {
                    cfg.CreateMap<tblMstArtistCategory, ArtistCategoryDTO>();
                  });

                  IMapper mapper1 = config1.CreateMapper();
                  ArtistCategoryDTO ArtistCategory = new ArtistCategoryDTO();
                  ArtistCategory = mapper1.Map<tblMstArtistCategory, ArtistCategoryDTO>(artistCategories);
                  ArtistCategoryList.Add(ArtistCategory);
                  artistList.ArtistCategoryList = ArtistCategoryList;
                  objEvent.Append(artistList);
                }

              }
              List<ArtistSkillsDTO> ArtistSkills = new List<ArtistSkillsDTO>();
              List<tblArtistSkillsMapping> artistskills = entity.tblArtistSkillsMappings.Where(x => x.UserId == artistList.Id).ToList();

              if (artistskills.Count > 0)
              {
                foreach (tblArtistSkillsMapping artistskill in artistskills)
                {
                  tblMstArtistSkill artistSkillsdata = entity.tblMstArtistSkills.Where(x => x.Id == artistskill.Skill_Id).FirstOrDefault();
                  var config1 = new MapperConfiguration(cfg =>
                  {
                    cfg.CreateMap<tblMstArtistSkill, ArtistSkillsDTO>();
                  });

                  IMapper mapper1 = config1.CreateMapper();
                  ArtistSkillsDTO objSkills = new ArtistSkillsDTO();
                  objSkills = mapper1.Map<tblMstArtistSkill, ArtistSkillsDTO>(artistSkillsdata);
                  ArtistSkills.Add(objSkills);
                  artistList.ArtistSkillsList = ArtistSkills;
                  objEvent.Append(artistList);
                }

              }


            }
          }


          //objEvent =artist;
          return objEvent;
          //return objEvent;

        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    /// <summary>
    /// check for whether the email id is present or not.
    /// </summary>
    /// <param name="RegisterData"></param>
    /// <returns></returns>
    public LoginResponse CheckUser(string Email, string Provider)
    {
      try
      {
        bool IsAdded = false;
        LoginResponse response = new LoginResponse();
        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
          if (Email != null)
          {

            tblUser tbluser = entity.tblUsers.Where(x => x.Email == Email && x.Password == null && x.Provider == Provider).FirstOrDefault();
            if(tbluser != null)
            {
              IsAdded = true;
            }
            tblUser user = entity.tblUsers.Where(x => x.Email == Email && x.Password == null && x.Provider != Provider).FirstOrDefault();
            if(user!=null)
            {
              user.Provider = Provider;
              entity.SaveChanges();
              IsAdded = true;
              
            }

            tblUser userdata = entity.tblUsers.Where(x => x.Email == Email && x.Password != null && x.Provider == null).FirstOrDefault();
            if (userdata != null)
            {
              IsAdded = true;
            }

            if (IsAdded==true)
            {
              tblUser tblUsers = entity.tblUsers.Where(x => x.Email == Email).FirstOrDefault();
              string AccessToken = AddAccessToken(tblUsers.Id, entity);
              var config = new MapperConfiguration(cfg =>
              {
                cfg.CreateMap<tblUser, LoginResponse>();

              });

              IMapper mapper = config.CreateMapper();
              response = mapper.Map<tblUser, LoginResponse>(tblUsers);
              response.AccessToken = AccessToken;
              response.LoginMessage = "Success";
              response.IsValid = true;
            }
            else
            {
              response = null;
            }
          }
        }
        return response;
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    //public bool AddArtistSkills(ArtistSkillsDTO pArtistSkills)
    //{
    //  bool isAdded = false;
    //  using (AMS_DevpEntities entity = new AMS_DevpEntities())
    //  {
    //    //foreach (var artistSkill in pArtistSkills.SkillsName)
    //    //{
    //      tblMstArtistSkill tblMstArtist = entity.tblMstArtistSkills.Where(x => x.SkillsName != pArtistSkills.SkillsName).FirstOrDefault();

    //      if (tblMstArtist != null)
    //      {
    //        tblMstArtistSkill tblMstArtistSkill = new tblMstArtistSkill();
    //        //tblMstArtistSkill.Id = artistSkill.Id;
    //        tblMstArtistSkill.SkillsName = tblMstArtist.SkillsName;
    //        entity.tblMstArtistSkills.Add(tblMstArtistSkill);
    //        entity.SaveChanges();
    //        isAdded = true;

    //      }
    //    //}



    //  }

    //  return isAdded;
    //}

  }
}

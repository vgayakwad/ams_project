﻿using AMS.Models;
using AMS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS.BL
{
  public interface ICommon
    {
        List<CountryDTO> GetAllCountries();
        List<StateDTO> GetStateByCountryCode(string CountryCode);
        List<CityDTO> GetCityByStateCode(string StateCode);
        List<ArtistCategoryDTO> GetAllArtistCategory();
        string EncryptString(string plainSourceStringToEncrypt);
        string GetSystemSetting(string keyName);
        List<EventCategoryDTO> GetAllEventCategory();
        List<TimeZoneDTO> GetTimeZoneByCountryCode(string CountryCode);
    }
}

using AMS.DAL;
using AMS.Models;
using AMS.Models.Models;
using AMS.Utility;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AMS.BL
{
    public class CommonBL : ICommon
    {

        /// <summary>
        /// check valid accesstoken
        /// </summary>
        /// <param name="pAccessToken"></param>
        /// <returns></returns>
        public bool CheckAccessToken(string pAccessToken)
        {
            try
            {
                bool isValid = false;
                using (AMS_DevpEntities entity = new AMS_DevpEntities())
                {
                    tblUserSession userAccessToken = entity.tblUserSessions.Where(t => t.AccessToken == pAccessToken && t.tblUser.IsActive == true).FirstOrDefault();
                    if (userAccessToken != null)
                    {
                        isValid = true;
                        HttpContext.Current.Items["UserId"] = userAccessToken.UserId;
                    }
                }
                return isValid;
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// get all country list from tblMstCountry
        /// </summary>
        /// <returns></returns>
        /// 
        public List<CountryDTO> GetAllCountries()
        {
            try
            {
                List<CountryDTO> desCountry = new List<CountryDTO>();
                using (AMS_DevpEntities entity = new AMS_DevpEntities())
                {
                    List<tblMstCountry> srcCountry = entity.tblMstCountries.ToList();
                    if (srcCountry != null && srcCountry.Count > 0)
                    {
                        var config = new MapperConfiguration(cfg =>
                        {
                            cfg.CreateMap<tblMstCountry, CountryDTO>();

                        });

                        IMapper mapper = config.CreateMapper();
                        desCountry = mapper.Map<List<tblMstCountry>, List<CountryDTO>>(srcCountry);

                    }
                }

                return desCountry;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// get state list by country Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public List<StateDTO> GetStateByCountryCode(string pCountryCode)
        {
            try
            {
                List<StateDTO> desStates = new List<StateDTO>();
                if (!string.IsNullOrWhiteSpace(pCountryCode))
                {
                    pCountryCode = pCountryCode.IfNotNullTrim();
                    using (AMS_DevpEntities entity = new AMS_DevpEntities())
                    {
                        tblMstCountry country = entity.tblMstCountries.Where(r => r.CountryCode == pCountryCode.ToUpper()).FirstOrDefault();
                        if (country != null)
                        {
                            List<tblMstState> srcState = entity.tblMstStates.Where(r => r.CountryId == country.Id).ToList();

                            if (srcState != null && srcState.Count > 0)
                            {
                                var config = new MapperConfiguration(cfg =>
                                {
                                    cfg.CreateMap<tblMstState, StateDTO>();

                                });

                                IMapper mapper = config.CreateMapper();
                                desStates = mapper.Map<List<tblMstState>, List<StateDTO>>(srcState);

                            }
                        }
                    }
                }
                return desStates;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// get city list by state id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public List<CityDTO> GetCityByStateCode(string pStateCode)
        {
            try
            {
                List<CityDTO> desCities = new List<CityDTO>();
                if (!string.IsNullOrWhiteSpace(pStateCode))
                {
                    pStateCode = pStateCode.IfNotNullTrim();
                    using (AMS_DevpEntities entity = new AMS_DevpEntities())
                    {
                        tblMstState state = entity.tblMstStates.Where(r => r.StateCode == pStateCode.ToUpper()).FirstOrDefault();
                        if (state != null)
                        {
                            List<tblMstCity> srcCities = entity.tblMstCities.Where(r => r.StateId == state.Id).ToList();

                            if (srcCities != null && srcCities.Count > 0)
                            {
                                var config = new MapperConfiguration(cfg =>
                                {
                                    cfg.CreateMap<tblMstCity, CityDTO>();

                                });

                                IMapper mapper = config.CreateMapper();
                                desCities = mapper.Map<List<tblMstCity>, List<CityDTO>>(srcCities);
                            }
                        }
                    }
                }
                return desCities;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Get All Artist Category
        /// </summary>
        /// <returns></returns>
        public List<ArtistCategoryDTO> GetAllArtistCategory()
        {
            try
            {
                List<ArtistCategoryDTO> desArtist = new List<ArtistCategoryDTO>();
                using (AMS_DevpEntities entity = new AMS_DevpEntities())
                {
                    List<tblMstArtistCategory> srcArtist = entity.tblMstArtistCategories.ToList();
                    if (srcArtist != null && srcArtist.Count > 0)
                    {
                        var config = new MapperConfiguration(cfg =>
                        {
                            cfg.CreateMap<tblMstArtistCategory, ArtistCategoryDTO>();

                        });

                        IMapper mapper = config.CreateMapper();
                        desArtist = mapper.Map<List<tblMstArtistCategory>, List<ArtistCategoryDTO>>(srcArtist);

                    }
                }
                return desArtist;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get System Settings for Image creation
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public string GetSystemSetting(string pKeyName)
        {
            string value = string.Empty;
            using (AMS_DevpEntities entity = new AMS_DevpEntities())
            {
                tblSystemSetting tblmodel = new tblSystemSetting();

                tblmodel = entity.tblSystemSettings.Where(r => r.Name == pKeyName).FirstOrDefault();
                //system.Id = tblmodel.Id;
                //system.Name = tblmodel.Name;
                value = tblmodel.Value;
            }
            return value;
        }

        public List<EventCategoryDTO> GetAllEventCategory()
        {
            try
            {
                List<EventCategoryDTO> desArtist = new List<EventCategoryDTO>();
                using (AMS_DevpEntities entity = new AMS_DevpEntities())
                {
                    List<tblMstEventCategory> srcArtist = entity.tblMstEventCategories.ToList();
                    if (srcArtist != null && srcArtist.Count > 0)
                    {
                        var config = new MapperConfiguration(cfg =>
                        {
                            cfg.CreateMap<tblMstEventCategory, EventCategoryDTO>();

                        });

                        IMapper mapper = config.CreateMapper();
                        desArtist = mapper.Map<List<tblMstEventCategory>, List<EventCategoryDTO>>(srcArtist);

                    }
                }
                return desArtist;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<TimeZoneDTO> GetTimeZoneByCountryCode(string pCountryCode)
        {
            try
            {
                List<TimeZoneDTO> desZones = new List<TimeZoneDTO>();
                using (AMS_DevpEntities entity = new AMS_DevpEntities())
                {
                    pCountryCode = pCountryCode.IfNotNullTrim();
                    tblMstCountry country = entity.tblMstCountries.Where(r => r.CountryCode == pCountryCode.ToUpper()).FirstOrDefault();

                    if (country != null)
                    {
                        List<tblMstTimeZone> srcZones = entity.tblMstTimeZones.Where(r => r.CountryId == country.Id).ToList();

                        if (srcZones != null && srcZones.Count > 0)
                        {
                            var config = new MapperConfiguration(cfg =>
                            {
                                cfg.CreateMap<tblMstTimeZone, TimeZoneDTO>();

                            });

                            IMapper mapper = config.CreateMapper();
                            desZones = mapper.Map<List<tblMstTimeZone>, List<TimeZoneDTO>>(srcZones);

                        }
                    }
                }
                return desZones;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string EncryptString(string plainSourceStringToEncrypt)
        {
            try
            {
                string passPhrase = GetSystemSetting(UtilityBL.SystemSettings.ENC_DEC_KEY.ToString());
                //Set up the encryption objects
                using (AesCryptoServiceProvider acsp = GetProvider(Encoding.Default.GetBytes(passPhrase)))
                {
                    byte[] sourceBytes = Encoding.ASCII.GetBytes(plainSourceStringToEncrypt);
                    ICryptoTransform ictE = acsp.CreateEncryptor();

                    //Set up stream to contain the encryption
                    MemoryStream msS = new MemoryStream();

                    //Perform the encrpytion, storing output into the stream
                    CryptoStream csS = new CryptoStream(msS, ictE, CryptoStreamMode.Write);
                    csS.Write(sourceBytes, 0, sourceBytes.Length);
                    csS.FlushFinalBlock();

                    //sourceBytes are now encrypted as an array of secure bytes
                    byte[] encryptedBytes = msS.ToArray(); //.ToArray() is important, don't mess with the buffer

                    //return the encrypted bytes as a BASE64 encoded string
                    return Convert.ToBase64String(encryptedBytes);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Decrypts a BASE64 encoded string of encrypted data, returns a plain string
        /// </summary>
        /// <param name="base64StringToDecrypt">an Aes encrypted AND base64 encoded string</param>
        /// <param name="passphrase">The passphrase.</param>
        /// <returns>returns a plain string</returns>
        public string DecryptString(string base64StringToDecrypt, string decriptKey = "")
        {
            try
            {
                string passPhrase = string.Empty;
                //string passPhrase = "$DMPISYRB@67CHT#";// "RDMPISYRB167CHTR";
                if (string.IsNullOrEmpty(decriptKey))
                {
                    passPhrase = GetSystemSetting(UtilityBL.SystemSettings.ENC_DEC_KEY.ToString());
                }
                else
                {
                    passPhrase = decriptKey;
                }
                //Set up the encryption objects
                using (AesCryptoServiceProvider acsp = GetProvider(Encoding.Default.GetBytes(passPhrase)))
                {
                    byte[] RawBytes = Convert.FromBase64String(base64StringToDecrypt);
                    ICryptoTransform ictD = acsp.CreateDecryptor();

                    //RawBytes now contains original byte array, still in Encrypted state

                    //Decrypt into stream
                    MemoryStream msD = new MemoryStream(RawBytes, 0, RawBytes.Length);
                    CryptoStream csD = new CryptoStream(msD, ictD, CryptoStreamMode.Read);
                    //csD now contains original byte array, fully decrypted

                    //return the content of msD as a regular string
                    return (new StreamReader(csD)).ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private static AesCryptoServiceProvider GetProvider(byte[] key)
        {
            AesCryptoServiceProvider result = new AesCryptoServiceProvider();

            try
            {
                result.BlockSize = 128;
                result.KeySize = 128;
                result.Mode = CipherMode.CBC;
                result.Padding = PaddingMode.PKCS7;

                result.GenerateIV();
                result.IV = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

                byte[] RealKey = GetKey(key, result);
                result.Key = RealKey;
                // result.IV = RealKey;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        private static byte[] GetKey(byte[] suggestedKey, SymmetricAlgorithm p)
        {
            byte[] kRaw = suggestedKey;
            List<byte> kList = new List<byte>();

            for (int i = 0; i < p.LegalKeySizes[0].MinSize; i += 8)
            {
                kList.Add(kRaw[(i / 8) % kRaw.Length]);
            }
            byte[] k = kList.ToArray();
            return k;
        }

        /// <summary>
        /// Validate Country State and City
        /// </summary>
        /// <param name="userInput"></param>
        /// <returns></returns>
        public bool ValidateUserCountry(long pUserId, long pCountryId, long pStateId, long pCityId)
        {
            try
            {
                bool isValid;
                using (AMS_DevpEntities entity = new AMS_DevpEntities())
                {
                    tblUser userData = new tblUser();
                    userData = entity.tblUsers.Where(x => x.Id == pUserId).FirstOrDefault();
                    if (userData != null)
                    {
                        tblMstCountry country = entity.tblMstCountries.Where(x => x.Id == pCountryId).FirstOrDefault();
                        if (country != null)
                        {
                            tblMstState state = entity.tblMstStates.Where(x => x.CountryId == pCountryId && x.Id == pStateId).FirstOrDefault();
                            if (state != null)
                            {
                                tblMstCity city = entity.tblMstCities.Where(x => x.StateId == pStateId && x.Id == pCityId).FirstOrDefault();
                                if (city != null)
                                {
                                    isValid = true;
                                }
                                else
                                {
                                    isValid = false;
                                }
                            }
                            else
                            {
                                isValid = false;
                            }
                        }
                        else
                        {
                            isValid = false;
                        }
                    }
                    else
                    {
                        isValid = false;
                    }
                    return isValid;
                }
            }
            catch (Exception)
            {
                throw;

            }
        }

        /// <summary>
        /// Validate Artist Category
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool ValidateArtistCategory(int[] pArtistCategoryId, long pUserId)
        {
            try
            {
                bool isValid=false;
                tblUser db = new tblUser();

                using (AMS_DevpEntities entity = new AMS_DevpEntities())
                {
                    foreach(var artistid in pArtistCategoryId)
                    {
                      //if (artistid != null)
                      //{
                        tblMstArtistCategory artist = entity.tblMstArtistCategories.Where(x => x.Id == artistid).FirstOrDefault();
                        if (artist != null)
                        {
                          // model.ArtistCategoryId = Convert.ToInt32(model.ArtistCategoryId);

                          isValid = true;
                        }
                        else
                        {
                          isValid = false;
                        }
                     // }
                      //else
                      //{
                      //  isValid = false;
                      //}

                    }
                    //tblUser userData = entity.tblUsers.Where(x => x.Id == pUserId).FirstOrDefault();
                   
                }

                return isValid;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Validate Event Category
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool ValidateEventCategory(long pEventCategoryId, long pUserId)
        {
            try
            {
                bool isValid = false;
                tblUser db = new tblUser();

                using (AMS_DevpEntities entity = new AMS_DevpEntities())
                {
                    tblUser userData = new tblUser();
                    userData = entity.tblUsers.Where(x => x.Id == pUserId).FirstOrDefault();
                    if (userData != null)
                    {
                        tblMstEventCategory artist = entity.tblMstEventCategories.Where(x => x.Id == pEventCategoryId).FirstOrDefault();
                        if (artist != null)
                        {
                            isValid = true;
                        }
                    }

                    return isValid;

                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}

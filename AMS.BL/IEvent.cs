using AMS.Models;
using AMS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS.BL
{
  public interface IEvent
  {
    List<ArtistListDTO> GetArtistList(ArtistListDTO artistList);
    /*  List<DashboardDTO> GetEventList(string searchTitle,string eventAttribute, long userId, bool pIsPrivate, out int totalRecords,string pDateFilter);*/
    List<DashboardDTO> GetEventList(EventListDTO pEventData, out int pTotalRecords);
    DashboardDTO GetEventDetails(long Id, bool IsPast);
    bool AddGalleryImages(EventGallary pEventImages);
    long AddUpdateEvent(EventDTO pEvent, bool isFromAPi);
    List<GallaryImages> GetGalleryImages(long Id);
    bool DeleteGalleryImages(List<GallaryImages> pEventImages);
    List<UserAndGroup> AdvancedSearch(string pSearchTitle);
    UserDetailDTO GetEventsByOrganizerId(SearchArtistDTO searchOrganizer);
    bool DeleteEventByEventId(ScheduledEventsDTO scheduled);
    List<CountryStateCityDTO> countryStateCityDTOSearch(string searchkey);
    UserDetailDTO GetEventByArtistId(SearchArtistDTO searchArtist);
    List<UserDetailDTO> GetOrganizerList();
    List<UserDetailDTO> GetRegisteredArtistList();
    List<DashboardDTO> GetEventListByEventCategoryWise(int pEventTypeId, string pSearchTitle);
    List<DashboardDTO> SearchEvent(string pSearchTitle);
    List<DashboardDTO> GetEventByArtistSkills(int pSkill_Id);
    List<ArtistCategoryDTO> GetCategoryListByUserId(long Id);
    List<EventCategoryDTO> GetEventCountByCategory();
    List<DashboardDTO> GetEventListForPastEvents(SearchArtistDTO pEventData);
  }
}

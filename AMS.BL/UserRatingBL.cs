using AMS.DAL;
using AMS.Models.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS.BL
{
  public class UserRatingBL : IUserRating
  {/// <summary>
   /// add user ratings for event, artist and organizer
   /// </summary>
   /// <param name="pRating"></param>
   /// <returns></returns>
    public bool AddUserRating(UserRatingDTO pRating)
    {
      try
      {
        bool isAdded = false;
        using (AMS_DevpEntities entity = new AMS_DevpEntities())
        {
          //tblUserRating objRating = new tblUserRating();
          //var config = new MapperConfiguration(cfg =>
          //{
          //  cfg.CreateMap<UserRatingDTO, tblUserRating>();
          //});

          //IMapper mapper = config.CreateMapper();
          //objRating = mapper.Map<UserRatingDTO, tblUserRating>(pRating);
          //objRating.CreatedOn = DateTime.Now;
          //entity.tblUserRatings.Add(objRating);
          //entity.SaveChanges();
          //isAdded = true;


          if (pRating.IPAddress != null)
          {
            List<tblUserRating> tblUserRating = entity.tblUserRatings.Where(x => x.IPAddress == pRating.IPAddress).ToList();
            if (tblUserRating.Count > 0)
            {
              List<tblUserRating> tblUserRatings = entity.tblUserRatings.Where(x => x.EventOrUserId == pRating.EventOrUserId).ToList();
              if(tblUserRatings.Count>0)
              {
                isAdded = false;
              }
              else
              {
                isAdded = true;
              }
              //foreach (var item in tblUserRating)
              //{
              //  if (pRating.EventOrUserId == item.EventOrUserId)
              //  {
              //    
              //  }
              //  else
              //  {
              //    List<tblUserRating> tblUserRatings = entity.tblUserRatings.Where(x => x.EventOrUserId == pRating.EventOrUserId).ToList();
              //    if(tblUserRatings.Count<=0)
              //    {
              //      isAdded = true;
              //    }

              //  }
              //}
              if (isAdded == true)
              {
                tblUserRating objRating = new tblUserRating();
                var config = new MapperConfiguration(cfg =>
                {
                  cfg.CreateMap<UserRatingDTO, tblUserRating>();
                });

                IMapper mapper = config.CreateMapper();
                objRating = mapper.Map<UserRatingDTO, tblUserRating>(pRating);
                objRating.CreatedOn = DateTime.Now;
                entity.tblUserRatings.Add(objRating);
                entity.SaveChanges();
                isAdded = true;

              }
            }
            else
            {
              tblUserRating objRating = new tblUserRating();
              var config = new MapperConfiguration(cfg =>
              {
                cfg.CreateMap<UserRatingDTO, tblUserRating>();
              });

              IMapper mapper = config.CreateMapper();
              objRating = mapper.Map<UserRatingDTO, tblUserRating>(pRating);
              objRating.CreatedOn = DateTime.Now;
              entity.tblUserRatings.Add(objRating);
              entity.SaveChanges();
              isAdded = true;
            }

          }
        }

        return isAdded;


      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

  }
}

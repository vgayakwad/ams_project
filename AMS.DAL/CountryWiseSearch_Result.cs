//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AMS.DAL
{
    using System;
    
    public partial class CountryWiseSearch_Result
    {
        public long CityId { get; set; }
        public string Name { get; set; }
        public long CityCode { get; set; }
    }
}

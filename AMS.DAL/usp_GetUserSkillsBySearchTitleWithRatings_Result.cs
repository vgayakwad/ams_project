//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AMS.DAL
{
    using System;
    
    public partial class usp_GetUserSkillsBySearchTitleWithRatings_Result
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Group { get; set; }
        public string ProfileImageURL { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string MiddleName { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public Nullable<double> AverageRating { get; set; }
        public Nullable<int> Total { get; set; }
        public string ContactPerson { get; set; }
        public string ContactEmail { get; set; }
        public string SkillName { get; set; }
        public string Description { get; set; }
    }
}

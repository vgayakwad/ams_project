//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AMS.DAL
{
    using System;
    
    public partial class usp_GetPastEventsWithCategoryAndDateFilter_Result
    {
        public Nullable<int> RowNumber { get; set; }
        public Nullable<long> Id { get; set; }
        public Nullable<long> UserId { get; set; }
        public Nullable<bool> IsPrivate { get; set; }
        public string EventName { get; set; }
        public Nullable<System.DateTime> EventStart { get; set; }
        public Nullable<System.DateTime> EventEnd { get; set; }
        public string Description { get; set; }
        public string EventTypeName { get; set; }
        public string EventImageUrl { get; set; }
        public string CityName { get; set; }
        public string CountryName { get; set; }
        public string StateName { get; set; }
        public string Address { get; set; }
        public Nullable<double> AverageRating { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AMS.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblArtistSchedule
    {
        public long Id { get; set; }
        public System.DateTime AvailableFrom { get; set; }
        public System.DateTime AvailableTo { get; set; }
        public long ArtistId { get; set; }
        public string Address { get; set; }
        public long CountryId { get; set; }
        public long StateId { get; set; }
        public long CityId { get; set; }
        public string Status { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<long> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
    
        public virtual tblUser tblUser { get; set; }
        public virtual tblMstCity tblMstCity { get; set; }
        public virtual tblMstCountry tblMstCountry { get; set; }
        public virtual tblMstState tblMstState { get; set; }
    }
}

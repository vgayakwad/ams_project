//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AMS.DAL
{
    using System;
    
    public partial class usp_GetDashboardEventData_Result
    {
        public long Id { get; set; }
        public string Eventname { get; set; }
        public long UserId { get; set; }
        public string Address { get; set; }
        public long CountryId { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public long CityId { get; set; }
        public long StateId { get; set; }
        public string StateCode { get; set; }
        public string StateName { get; set; }
        public System.DateTime EventStart { get; set; }
        public System.DateTime EventEnd { get; set; }
        public string EventImageUrl { get; set; }
    }
}

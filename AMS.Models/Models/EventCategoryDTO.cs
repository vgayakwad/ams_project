﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS.Models
{
    public class EventCategoryDTO
    {
        public int Id { get; set; }
        public string EventCategoryName { get; set; }
        public Nullable<int> EventCount { get; set; }
    }
}

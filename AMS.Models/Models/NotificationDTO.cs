using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS.Models.Models
{
  public class NotificationDTO
  {
    public long Id { get; set; }
    public long SenderId { get; set; }
    public long ReceiverId { get; set; }
    public Nullable<bool> IsRead { get; set; }
    public string Subject { get; set; }
    public string Message { get; set; }
    public Nullable<long> CreatedBy { get; set; }
    public Nullable<System.DateTime> CreatedOn { get; set; }
    public Nullable<System.DateTime> UpdatedOn { get; set; }
  }
}

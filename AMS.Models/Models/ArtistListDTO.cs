using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS.Models.Models
{
   public class ArtistListDTO
    {
        public long id { get; set; }
        public string Name { get; set; } 
        public string ProfileImageURL { get; set; }
        public string ArtistCategoryName { get; set; }
        public int ArtistCategoryId { get; set; }
        public bool IsDefault { get; set; }
        public string Status { get; set; }
        public DateTime AvailableFrom { get; set; }
        public DateTime AvailableTo { get; set; }
        public long ScheduleId { get; set; }
        public List<ArtistCategoryDTO> ArtistCategories { get; set; }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS.Models.Models
{
  public  class TimeZoneDTO
    {
        public int Id { get; set; }
        public string TimeZone { get; set; }
        public decimal UTC_OffSet { get; set; }
    }
}

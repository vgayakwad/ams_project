using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS.Models.Models
{
  public class CountryStateCityDTO
  {
    public int Id { get; set; }
    public string CountryName { get; set; }
    public string CountryCode { get; set; }

  }
}

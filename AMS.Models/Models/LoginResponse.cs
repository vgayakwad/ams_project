using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS.Models.Models
{
    public class LoginResponse
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string LoginMessage { get; set; }
        public string AccessToken { get; set; }
        public string UserType { get; set; }
        public string Email { get; set; }
        public bool IsValid { get; set; }

    }
}

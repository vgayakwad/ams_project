using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS.Models.Models
{
   public class EventGallary
    {
        public  long EventId { get; set; }
        public long UserId { get; set; }

        public List<GallaryImages> Images { get; set; }
    }
    public class GallaryImages
    {
        public string EventImageUrl { get; set; }
        public Nullable<long> EventId { get; set; }
        public long ImageId { get; set; }
    }
    public class EventListDTO
    {
        public long UserId { get; set; }
        public string SearchTitle { get; set; }
        public bool IsPrivate { get; set; }
        public string EventAttribute { get; set; }
        public string DateFilter { get; set; }
        public string Type { get; set; }
        public int rating { get; set; }

    }
}

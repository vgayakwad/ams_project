using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS.Models.Models
{
    public class DashboardDTO
    {
        public int? RowNumber { get; set; }
        public long? Id { get; set; }
        public long? UserId { get; set; }
        public long HostId { get; set; }
        public string EventName { get; set; }
        public DateTime? EventStart { get; set; }
        public DateTime? EventEnd { get; set; }
        public string Description { get; set; }
        public string EventTypeName { get; set; }
        public int EventTypeId { get; set; }
        public string CityName { get; set; }
        public long CityId { get; set; }
        public string CountryName { get; set; }
        public string StateName { get; set; }
        public string CountryCode { get; set; }
        public string StateCode { get; set; }
        public string Address { get; set; }
        public string EventImageUrl { get; set; }
        public string TimeZone { get; set; }
        public long TimeZoneId { get; set; }
        public bool IsPrivate { get; set; }

        public float AverageRating { get; set; }
        public OrganizerDTO OrganizerDetails { get; set; }
        public List<ArtistListDTO> ArtistsList { get; set; }
        public List<RatingsDTO> RatingInfo { get; set; }
       
        //public List<ArtistCategoryDTO> categoryDTOs { get; set; }

    }
       public class OrganizerDTO  
        {
        public long id { get; set; }
        public string Name { get; set; }
        public string ProfileImageURL { get; set; }
        public bool IsDefault { get; set; }

       }

       public class RatingsDTO
       {
        public int EventId { get; set; }
        public int Ratings { get; set; }
        public int TotalCount { get; set; }

       }
}

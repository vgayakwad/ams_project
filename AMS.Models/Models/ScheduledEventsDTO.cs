using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS.Models.Models
{
  public  class ScheduledEventsDTO
    {
        public string EventStart { get; set; }
        public string EventEnd{ get; set; }
        public long EventId { get; set; }
        public string Name { get; set; }
        public string EventName { get; set; }
        public string ArtistCategoryName { get; set; }
        public string Description { get; set; }
       public List<ArtistListDTO> ArtistsList { get; set; }
  }

}

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS.Models.Models
{
   public class UserRatingDTO : IValidatableObject
    {
        public long EventOrUserId { get; set; }
        public int Rating { get; set; }
        public string Type { get; set; }
        public string IPAddress { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrWhiteSpace(Type) || Rating <= 0 || EventOrUserId <= 0)

                yield return new ValidationResult("error")
                {
                    ErrorMessage = "ErrorMessage"
                };
        }
    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS.Models.Models
{
  public class EventDTO : IValidatableObject
  {
    public long EventId { get; set; }
    public string EventName { get; set; }
    public string EventStartDate { get; set; }
    public string EventEndDate { get; set; }
    public int EventTypeId { get; set; }
    public long UserId { get; set; }
    public string Address { get; set; }
    public string Description { get; set; }
    public long CountryId { get; set; }
    public long StateId { get; set; }
    public long CityId { get; set; }
    public long? TimeZoneId { get; set; }
    public string EventImage { get; set; }
    public List<Artists> ArtistsList { get; set; }
    public string StateCode { get; set; }
    public string CountryCode { get; set; }
    public bool IsEventImageUpdated { get; set; }
    public bool isPrivate { get; set; }
    public bool IsVisibility { get;set; }

    public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
    {
      if(ArtistsList==null && IsVisibility==false)
      {
        if (string.IsNullOrWhiteSpace(EventName) || string.IsNullOrWhiteSpace(EventStartDate) || string.IsNullOrWhiteSpace(EventImage)
          || string.IsNullOrWhiteSpace(EventEndDate) || EventTypeId <= 0 || string.IsNullOrWhiteSpace(CountryCode) || string.IsNullOrWhiteSpace(StateCode) || CityId <= 0)

          yield return new ValidationResult("error")
          {
            ErrorMessage = "ErrorMessage"
          };
      }
      else if( ArtistsList != null && IsVisibility==false)
      {
        //  ArtistsList = new List<Artists>();
        if (string.IsNullOrWhiteSpace(EventName) || string.IsNullOrWhiteSpace(EventStartDate) || string.IsNullOrWhiteSpace(EventImage)
            || string.IsNullOrWhiteSpace(EventEndDate) || EventTypeId <= 0 || string.IsNullOrWhiteSpace(CountryCode) || string.IsNullOrWhiteSpace(StateCode) || CityId <= 0 || ArtistsList.Count <= 0)

          yield return new ValidationResult("error")
          {
            ErrorMessage = "ErrorMessage"
          };
      }
      else
      {
        if (IsVisibility==false )

          yield return new ValidationResult("error")
          {
            ErrorMessage = "ErrorMessage"
          };
      }
      


    }


  }

  
    public class Artists
    {
    
        public long ArtistUserId { get; set; }
        public int[] ArtistCategoryId { get; set; } 
        public string Status { get; set; }
        public string AvailableFrom { get; set; }
        public string AvailableTo { get; set; }
        public bool IsRemoved { get; set; }
        public long ScheduleId { get; set; }


    //|| ArtistsList.Count <= 0|| string.IsNullOrWhiteSpace(AvailableFrom) || string.IsNullOrWhiteSpace(AvailableTo)
    public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
    {
      if (ArtistUserId <= 0 || ArtistCategoryId.Length <= 0 )

        yield return new ValidationResult("error")
        {
          ErrorMessage = "ErrorMessage"
        };
    }

  }


}

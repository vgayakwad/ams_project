using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS.Models.Models
{
  public class SearchArtistDTO
  {
    public string SearchTitle { get; set; }
    public int[] CategoryId { get; set; }
    public string DateFormat { get; set; }
    public int UserId { get; set; }
    public int Rating { get; set; }
    public string UserType { get; set; }
  }
}

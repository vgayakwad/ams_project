﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS.Models.Models
{
    public class LoginModel: IValidatableObject
    {
        public long Id { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Provider { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrWhiteSpace(Email))

                yield return new ValidationResult("error")
                {
                    ErrorMessage = "ErrorMessage"
                };
        }
    }
}

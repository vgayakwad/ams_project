using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS.Models.Models
{
  public class UserDetailDTO
  {
    public long Id { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string MiddleName { get; set; }
    public string UserType { get; set; }
    public string Email { get; set; }
    public string BirthDate { get; set; }
    public string Gender { get; set; }
    public string ArtistCategoryName { get; set; }
    public int ArtistCategoryId { get; set; }
    public string CityName { get; set; }
    public long CityId { get; set; }
    public string CountryName { get; set; }
    public string StateName { get; set; }
    public string CountryCode { get; set; }
    public string StateCode { get; set; }
    public string ProfileImageURL { get; set; }
    public string Address { get; set; }
    public string Description { get; set; }
    public string Organization { get; set; }
    public string ContactEmail { get; set; }
    public string ContactPerson { get; set; }
    public string PhoneNumber { get; set; }
    public float AverageRating { get; set; }
    public int CustomerRating { get; set; }
    public List<DashboardDTO> EventList {get;set;}
    public List<ArtistCategoryDTO> ArtistCategoryList { get; set; }
    public List<ArtistSkillsDTO> ArtistSkillsList { get; set; }

    }
}

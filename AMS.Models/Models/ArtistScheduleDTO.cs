using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS.Models.Models
{
   public class ArtistScheduleDTO :IValidatableObject
    {
        public string Email { get; set; }
        public string ContactEmail { get; set; }
        public long ScheduleId { get; set; }
        public long ArtistId { get; set; }
        public string CountryCode { get; set; }
        public long CountryId { get; set; }
        public long StateId { get; set; }
        public string CountryName { get; set; }
        public string StateName { get; set; }
        public  string StateCode { get; set; }
        public int CityId { get; set; }
        public string Address { get; set; }
        public string Status { get; set; }
        public string AvailableFrom { get; set; }
        public string AvailableTo { get; set; }
        public string ProfileImageURL { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string ArtistCategoryName { get; set; }
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)        {            if (string.IsNullOrWhiteSpace(AvailableFrom) || string.IsNullOrWhiteSpace(AvailableTo)                || ArtistId <= 0 || string.IsNullOrWhiteSpace(CountryCode) || string.IsNullOrWhiteSpace(StateCode) || string.IsNullOrWhiteSpace(Status) || CityId <= 0)                yield return new ValidationResult("error")                {                    ErrorMessage = "ErrorMessage"                };        }    }}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS.Models.Models
{
  public class UserAndGroup
  {
    public string Name { get; set; }
    public long Id { get; set; }
    public string Group { get; set; }
    public string ProfileImageURL { get; set; }
    }
}

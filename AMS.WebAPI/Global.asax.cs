﻿using AMS.BL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.SessionState;

namespace AMS.WebAPI
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        public override void Init()
        {
            this.PostAuthenticateRequest += ApiApplication_PostAuthenticateRequest;
            base.Init();
        }

        void ApiApplication_PostAuthenticateRequest(object sender, EventArgs e)
        {
            System.Web.HttpContext.Current.SetSessionStateBehavior(SessionStateBehavior.Required);
        }
        protected void Application_Start()
        {
            //register unity component
            UnityConfig.RegisterComponents();

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            CommonBL common = new CommonBL();
            string photoFolder = common.GetSystemSetting("PROFILE_PHOTO_FOLDER");
            string eventFolder = common.GetSystemSetting("EVENT_PHOTO_FOLDER");
            string galleryFolder = common.GetSystemSetting("EVENT_GALLERY_FOLDER");
            //string photoFolder = "/Profilephoto";
            if (!Directory.Exists(photoFolder))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(photoFolder)); //Create directory if it doesn't exist
            }
            if (!Directory.Exists(eventFolder))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(eventFolder)); //Create directory if it doesn't exist
            }
            if (!Directory.Exists(galleryFolder))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(galleryFolder)); //Create directory if it doesn't exist
            }
        }
        protected void Application_BeginRequest(Object sender, EventArgs e)
        {
            HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
            if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
            {
                HttpContext.Current.Response.AddHeader("Cache-Control", "no-cache");
                HttpContext.Current.Response.AddHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
                HttpContext.Current.Response.AddHeader("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With, Authorization");
                HttpContext.Current.Response.AddHeader("Access-Control-Max-Age", "1728000");
                HttpContext.Current.Response.End();
            }
        }
    }
}

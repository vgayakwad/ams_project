﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS.Utility
{
    public static class StringExtension
    {
        public static string IfNotNullTrim(this string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                return value.Trim();
            }
            return value;
        }
    }
}

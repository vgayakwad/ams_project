using AMS.BL;
using AMS.Models;
using AMS.Models.Models;
using AMS.WebAPI.FilterAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace AMS.WebAPI.Controllers
{
  [ApiAuthenticationFilter]
  public class EventController : ApiController
  {
    private readonly IEvent _event;
    public EventController(IEvent events)
    {
      _event = events;
    }
    /// <summary>
    /// get list of registered artists
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    //Displaying artist according to artistcategoryId and Event dates. 
    public HttpResponseMessage GetArtistList(ArtistListDTO artist)
    {
      try
      {
        if (artist != null)
        {
          List<ArtistListDTO> artists = _event.GetArtistList(artist);

          if (artists != null)
          {
            return Request.CreateResponse(HttpStatusCode.OK, artists);
          }
          else
          {
            throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound, UtilityBL.ErrorMessages.DATA_NOT_FOUND));
          }
        }
        else
        {
          return Request.CreateResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.PARAMETER_MISSING);
        }
      }
      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
      }
    }

    /// <summary>
    /// get list of events on passing country code,state code and city id and datefilter
    /// Displaying EventList on passing country code,state code and city id and datefilter.
    /// </summary>
    /// <param name="pageSize"></param>
    /// <param name="pageIndex"></param>
    /// <param name="searchTitle"></param>
    /// <param name="totalRecords"></param>
    /// <param name="dateFilter"></param>
    /// <param name="eventAttribute"></param>'UPCOMINGS' and 'PASTEVENTS'
    /// <returns></returns>

    [AllowAnonymous]
    [HttpPost]
    public HttpResponseMessage GetEventList([FromBody] EventListDTO eventList)
    {
      try
      {
        int totalRecords = 0;
        eventList.UserId = 0;
        if (HttpContext.Current.Items["UserId"] != null)
        {
          eventList.UserId = Convert.ToInt64(HttpContext.Current.Items["UserId"]);
        }
        if (!string.IsNullOrWhiteSpace(eventList.SearchTitle))
        {
          List<DashboardDTO> dashboardData = _event.GetEventList(eventList, out totalRecords);
          if (dashboardData != null && dashboardData.Count > 0)
          {
            return Request.CreateResponse(HttpStatusCode.OK, dashboardData);
          }
          else
          {
            return Request.CreateResponse(HttpStatusCode.NotFound, UtilityBL.ErrorMessages.DATA_NOT_FOUND);
          }
        }
        else
        {
          return Request.CreateResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.BAD_REQUEST);
        }
      }
      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
      }
    }
    /// <summary>
    /// create new event 
    /// </summary>
    /// <param name="objEvent"></param>
    /// <returns></returns>
    public HttpResponseMessage AddEvent(EventDTO objEvent)
    {
      try
      {
        if (ModelState.IsValid)
        {
          if (objEvent.IsVisibility == true)
          {
            long userId = Convert.ToInt64(HttpContext.Current.Items["UserId"]);
            objEvent.UserId = userId;
            //if (objEvent.ArtistsList.Count > 0)
            //{
            long eventId = _event.AddUpdateEvent(objEvent, false);
            if (eventId > 0)
            {
              return Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = true, EventId = eventId });
            }
            else
            {
              throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.PARAMETER_MISSING));
            }

          }
          if (objEvent.EventStartDate != objEvent.EventEndDate)
          {
            long userId = Convert.ToInt64(HttpContext.Current.Items["UserId"]);
            objEvent.UserId = userId;
            //if (objEvent.ArtistsList.Count > 0)
            //{
            long eventId = _event.AddUpdateEvent(objEvent, false);
            if (eventId > 0)
            {
              return Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = true, EventId = eventId });
            }
            else
            {
              throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.PARAMETER_MISSING));
            }
          }
          else
          {
            return Request.CreateResponse(HttpStatusCode.OK, new { Message = UtilityBL.ErrorMessages.BAD_REQUEST });
          }
        }
        else
        {
          throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.BAD_REQUEST));
        }
      }
      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
      }
    }
    /// <summary>
    /// Displaying Event details with artist list and organizer details.
    /// </summary>
    /// <param name="Id"></param>
    /// <returns></returns>
    [AllowAnonymous]
    [HttpGet]
    public HttpResponseMessage GetEventDetails([FromUri] long Id, bool IsPast)
    {
      try
      {
        if (Id > 0)
        {
          DashboardDTO eventData = _event.GetEventDetails(Id, IsPast);
          if (eventData.Id > 0 && eventData != null)
          {
            return Request.CreateResponse(HttpStatusCode.OK, eventData);
          }
          else
          {
            return Request.CreateResponse(HttpStatusCode.NotFound, UtilityBL.ErrorMessages.DATA_NOT_FOUND);
          }
        }
        else
        {
          throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.BAD_REQUEST));
        }
      }
      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
      }
    }

    /// <summary>
    /// save gallary images to database
    /// </summary>
    /// <param name="objEvent"></param>
    /// <returns></returns>
    [HttpPost]
    public HttpResponseMessage AddEventGallary([FromBody]EventGallary objEvent)
    {
      try
      {
        if (ModelState.IsValid)
        {
          long userId = Convert.ToInt64(HttpContext.Current.Items["UserId"]);
          objEvent.UserId = userId;
          if (objEvent.Images.Count > 0)
          {
            bool IsAdded = _event.AddGalleryImages(objEvent);

            if (IsAdded)
            {
              return Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = IsAdded });
            }
            else
            {
              throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.BAD_REQUEST));
            }
          }
          else
          {
            return Request.CreateResponse(HttpStatusCode.OK, new { Message = UtilityBL.ErrorMessages.ARTISTS_NOT_FOUND });
          }
        }
        else
        {
          throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.BAD_REQUEST));
        }
      }
      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
      }
    }
    /// <summary>
    /// get gallery images of specific event by  event id
    /// </summary>
    /// <param name="EventId"></param>
    /// <returns></returns>
    [HttpGet]
    [AllowAnonymous]
    public HttpResponseMessage GetGalleryImages([FromUri] long EventId)
    {
      try
      {
        if (EventId > 0)
        {
          List<GallaryImages> gallaryData = _event.GetGalleryImages(EventId);
          if (gallaryData.Count > 0 && gallaryData != null)
          {
            return Request.CreateResponse(HttpStatusCode.OK, gallaryData);
          }
          else
          {
            return Request.CreateResponse(HttpStatusCode.NotFound, UtilityBL.ErrorMessages.IMAGES_NOT_FOUND);
          }
        }
        else
        {
          throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.BAD_REQUEST));
        }
      }
      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
      }
    }
    /// <summary>
    /// Deleting gallery images 
    /// </summary>
    /// <param name="pEventImages"></param>
    /// <returns></returns>
    [HttpDelete]
    public HttpResponseMessage DeleteGalleryImages([FromBody] List<GallaryImages> pEventImages)
    {
      try
      {
        if (pEventImages.Count > 0)
        {
          bool IsDeleted = _event.DeleteGalleryImages(pEventImages);
          if (IsDeleted)
          {
            return Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = IsDeleted });
          }
          else
          {
            throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.BAD_REQUEST));
          }
        }
        else
        {
          return Request.CreateResponse(HttpStatusCode.OK, new { Message = UtilityBL.ErrorMessages.BAD_REQUEST });
        }
      }
      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
      }
    }
    /// <summary>
    /// Searching data according to artist,organizer and event,skills, categories etc.
    /// </summary>
    /// <param name="searchTitle"></param>
    /// <returns></returns>
    [HttpGet]
    [AllowAnonymous]
    public HttpResponseMessage AdvancedSearch([FromUri] string searchTitle)
    {
      try
      {
        List<UserAndGroup> UserData = _event.AdvancedSearch(searchTitle);
        if (UserData.Count > 0 && UserData != null)
        {
          return Request.CreateResponse(HttpStatusCode.OK, UserData);
        }
        else
        {
          return Request.CreateResponse(HttpStatusCode.NotFound, UtilityBL.ErrorMessages.BAD_REQUEST);
        }
      }
      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
      }
    }

    /// <summary>
    /// Displaying Eventlist of given organizer id with organizer details.
    /// </summary>
    /// <param name="organizerId"></param>
    /// <returns></returns>
    [HttpPost]
    [AllowAnonymous]
    public HttpResponseMessage GetEventByOrganizerId(SearchArtistDTO searchData)
    {
      try
      {
        if (searchData != null)
        {
          UserDetailDTO events = _event.GetEventsByOrganizerId(searchData);
          if (events != null)
          {
            return Request.CreateResponse(HttpStatusCode.OK, events);
          }
          else
          {
            throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.DATA_NOT_FOUND));
          }
        }
        else
        {
          throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.PARAMETER_MISSING));
        }
      }
      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
      }
    }
    /// <summary>
    /// Deleting event by event id
    /// </summary>
    /// <param name="artistLists"></param>
    /// <returns></returns>
    [HttpDelete]
    public HttpResponseMessage DeleteEventByEventId([FromBody] ScheduledEventsDTO artistLists)
    {
      try
      {
        //add if condition for scheduleid if schedule id is missing
        if (artistLists != null)
        {
          bool IsSuccess = _event.DeleteEventByEventId(artistLists);
          if (IsSuccess)
          {
            return Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = IsSuccess });
          }
          else
          {
            throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.DATA_NOT_FOUND));
          }
        }
        else
        {
          return Request.CreateResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.PARAMETER_MISSING);
        }
      }
      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
      }
    }
    /// <summary>
    /// Displaying respective data on match of country name, state name and city name
    /// </summary>
    /// <param name="searchkey"></param>
    /// <returns></returns>
    [HttpGet]
    [AllowAnonymous]
    public HttpResponseMessage CountryWiseSearch(string searchkey)
    {
      try
      {
        List<CountryStateCityDTO> UserData = _event.countryStateCityDTOSearch(searchkey);
        if (UserData != null && UserData.Count > 0)
        {
          return Request.CreateResponse(HttpStatusCode.OK, UserData);
        }
        else
        {
          return Request.CreateResponse(HttpStatusCode.NotFound, UtilityBL.ErrorMessages.BAD_REQUEST);
        }
      }
      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
      }
    }
    /// <summary>
    /// Displaying Event of given Artist id.
    /// </summary>
    /// <param name="ArtistId"></param>
    /// <returns></returns>
    [HttpPost]
    [AllowAnonymous]
    public HttpResponseMessage GetEventByArtistId(SearchArtistDTO searchArtist)
    {
      try
      {
        if (searchArtist != null)
        {
          UserDetailDTO events = _event.GetEventByArtistId(searchArtist);
          if (events != null)
          {
            return Request.CreateResponse(HttpStatusCode.OK, events);
          }
          else
          {
            throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.DATA_NOT_FOUND));
          }
        }
        else
        {
          throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.PARAMETER_MISSING));
        }
      }
      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
      }
    }
    /// <summary>
    /// Displaying OrganizerList(not in use).
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    [AllowAnonymous]
    public HttpResponseMessage GetOrganizerList()
    {
      try
      {
        List<UserDetailDTO> events = _event.GetOrganizerList();
        if (events != null && events.Count > 0)
        {
          return Request.CreateResponse(HttpStatusCode.OK, events);
        }
        else
        {
          throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.DATA_NOT_FOUND));
        }
      }
      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
      }
    }
    /// <summary>
    /// Displaying Artistcategories of given user Id(not in use).
    /// </summary>
    /// <param name="Id"></param>
    /// <returns></returns>
    //[HttpGet]
    //public HttpResponseMessage GetArtistCategoryByUserId([FromUri] long Id)
    //{
    //    try
    //    {
    //        if (Id > 0)
    //        {
    //            DashboardDTO eventData = _event.GetEventDetails(Id);
    //            if (eventData.Id > 0 && eventData != null)
    //            {
    //                return Request.CreateResponse(HttpStatusCode.OK, eventData);
    //            }
    //            else
    //            {
    //                return Request.CreateResponse(HttpStatusCode.NotFound, UtilityBL.ErrorMessages.DATA_NOT_FOUND);
    //            }
    //        }

    //        else
    //        {
    //            throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.BAD_REQUEST));
    //        }
    //    }

    //    catch (HttpResponseException ex)
    //    {
    //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
    //        throw;
    //    }
    //    catch (Exception ex)
    //    {
    //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
    //        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
    //    }
    //}
    /// <summary>
    /// Displaying all registered Artist list.(not in use)
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    [AllowAnonymous]
    public HttpResponseMessage GetRegisteredArtistList()
    {
      try
      {
        List<UserDetailDTO> users = _event.GetRegisteredArtistList();
        if (users.Count > 0)
        {
          return Request.CreateResponse(HttpStatusCode.OK, users);
        }
        else
        {
          throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.DATA_NOT_FOUND));
        }
      }

      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
      }
    }
    /// <summary>
    /// Get EventList on basis of Event category Id.
    /// </summary>
    /// <param name="pEventTypeId"></param>
    /// <returns></returns>
    [AllowAnonymous]
    [HttpGet]
    public HttpResponseMessage GetEventListByEventCategoryWise([FromUri] int EventTypeId, string SearchTitle)
    {
      try
      {

        List<DashboardDTO> EventData = _event.GetEventListByEventCategoryWise(EventTypeId, SearchTitle);
        if (EventData.Count > 0 && EventData != null)
        {
          return Request.CreateResponse(HttpStatusCode.OK, EventData);
        }
        else
        {
          return Request.CreateResponse(HttpStatusCode.NotFound, UtilityBL.ErrorMessages.DATA_NOT_FOUND);
        }
      }
      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
      }
    }
    /// <summary>
    /// Search functionality on Events not in use
    /// </summary>
    /// <param name="searchTitle"></param>
    /// <returns></returns>
    [HttpGet]
    public HttpResponseMessage SearchEvent([FromUri] string pSearchTitle)
    {
      try
      {
        List<DashboardDTO> events = _event.SearchEvent(pSearchTitle);
        if (events != null && events.Count > 0)
        {
          return Request.CreateResponse(HttpStatusCode.OK, events);
        }
        else
        {
          throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.DATA_NOT_FOUND));
        }
      }

      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
      }
    }
    /// <summary>
    /// Get Artist EventList of given skill id.
    /// </summary>
    /// <param name="pSkillId"></param>
    /// <returns></returns>
    [AllowAnonymous]
    [HttpGet]
    public HttpResponseMessage GetEventByArtistSkills([FromUri] int SkillId)
    {
      try
      {
        List<DashboardDTO> events = _event.GetEventByArtistSkills(SkillId);
        if (events != null && events.Count > 0)
        {
          return Request.CreateResponse(HttpStatusCode.OK, events);
        }
        else
        {
          throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.DATA_NOT_FOUND));
        }
      }

      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
      }
    }
    /// <summary>
    /// get categorylist of user by userId
    /// </summary>
    /// <param name="Id"></param>
    /// <returns></returns>
    [HttpGet]
    public HttpResponseMessage GetCategoryByArtistId([FromUri] long ArtistId)
    {
      try
      {
        List<ArtistCategoryDTO> categoryList = _event.GetCategoryListByUserId(ArtistId);
        if (categoryList.Count > 0)
        {
          return Request.CreateResponse(HttpStatusCode.OK, categoryList);
        }
        else
        {
          throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.DATA_NOT_FOUND));
        }
      }

      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
      }
    }
    [HttpGet]
    [AllowAnonymous]
    public HttpResponseMessage GetEventCountByCategory()
    {
      try
      {
        List<EventCategoryDTO> categoryList = _event.GetEventCountByCategory();
        if (categoryList.Count > 0)
        {
          return Request.CreateResponse(HttpStatusCode.OK, categoryList);
        }
        else
        {
          throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.DATA_NOT_FOUND));
        }
      }

      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
      }
    }
    /// <summary>
    /// Get Event List for past Events with Date Filters for previous week and previous month
    /// </summary>
    /// <param name="eventList"></param>
    /// <returns></returns>
    [AllowAnonymous]
    [HttpPost]
    
    public HttpResponseMessage GetEventListForPastEvents([FromBody] SearchArtistDTO eventList)
    {
      try
      {
        //int totalRecords = 0;
        //eventList.UserId = 0;

        //eventList.UserId = 0;
        int userId =(int) Convert.ToInt64(eventList.UserId);
        eventList.UserId = userId;
        //if (HttpContext.Current.Items["UserId"] != null)
        //{
        //  eventList.UserId =(int)Convert.ToInt64(HttpContext.Current.Items["UserId"]);
        //}
        List<DashboardDTO> dashboardData = _event.GetEventListForPastEvents(eventList);
        if (dashboardData.Count > 0)
        {
          return Request.CreateResponse(HttpStatusCode.OK, dashboardData);
        }
        else
        {
          return Request.CreateResponse(HttpStatusCode.NotFound, UtilityBL.ErrorMessages.DATA_NOT_FOUND);
        }

      }
      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.DATA_NOT_FOUND));
      }
    }
  }
}

using AMS.BL;
using AMS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AMS.WebAPI.Controllers
{
    public class NotificationController : ApiController
    {
    private readonly INotification _notification;
    public NotificationController(INotification notification)
    {
      _notification = notification;
    }
    [HttpPost]
    public HttpResponseMessage Notification(NotificationDTO notificationDTO)
    {
      try
      {
        if (ModelState.IsValid)
        {
          bool isValid = _notification.AddNotificationData(notificationDTO);
          if (isValid)
          {
          //  LoginResponse response = new LoginResponse();
          //  response = _account.Authentication(RegisterData.Email, RegisterData.Password, true);
           return Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = true });
          }
          else
          {
            throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.Conflict, UtilityBL.ErrorMessages.PARAMETER_MISSING));
          }
        }
        else
        {
          throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, BL.UtilityBL.ErrorMessages.BAD_REQUEST));
        }
      }

      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
      }
    }

  }
}

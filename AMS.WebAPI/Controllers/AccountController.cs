using AMS.BL;
using AMS.Models;
using AMS.Models.Models;
using AMS.WebAPI.FilterAttributes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;

namespace AMS.WebAPI.Controllers
{
  public class AccountController : ApiController
  {
    private readonly IAccount _account;
    public AccountController(IAccount account)
    {
      _account = account;
    }
    /// <summary>
    /// Register User/Sign Up User
    /// </summary>
    /// <param name="RegisterData"></param>
    /// <returns></returns>
    [HttpPost]
    public HttpResponseMessage Signup(RegisterUserDTO RegisterData)
    {
      try
      {
        bool isValid = false;
        if (ModelState.IsValid)
        {
          if (string.IsNullOrWhiteSpace(RegisterData.Provider))
          {
            if (!string.IsNullOrWhiteSpace(RegisterData.Password))
            {
              isValid = _account.Register(RegisterData);
            }
            else
            {
              throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.BAD_REQUEST));
            }
          }
          else
          {
            isValid = _account.Register(RegisterData);
          }

          if (isValid)
          {
            LoginResponse response = new LoginResponse();
            if (string.IsNullOrWhiteSpace(RegisterData.Provider))
            {
              response = _account.Authentication(RegisterData.Email, RegisterData.Password, RegisterData.Provider, true);
              return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            else
            {
              response = _account.Authentication(RegisterData.Email, RegisterData.Password, RegisterData.Provider, true);
              return Request.CreateResponse(HttpStatusCode.OK, response);
            }
          }
          else
          {
            throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.Conflict, UtilityBL.ErrorMessages.EMAIL_EXISTS));
          }
        }
        else
        {
          throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.BAD_REQUEST));
        }
      }

      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
      }
    }

    /// <summary>
    /// Forgot Password
    /// </summary>
    /// <param name="password"></param>
    /// <returns></returns>

    [HttpPost]
    public HttpResponseMessage ForgotPassword(ForgotPasswordDTO password)
    {
      try
      {
        bool isUpdated = false;
        if (ModelState.IsValid)
        {
          bool check = _account.ForgotPassword(password);
          if (check == true)
          {
            isUpdated = true;
            return Request.CreateResponse(HttpStatusCode.OK, new { IsUpdated = isUpdated });
          }
          else
          {
            return Request.CreateResponse(HttpStatusCode.NotFound, new { Message = UtilityBL.ErrorMessages.EMAIL_DOESNOT_EXISTS });
          }
        }
        else
        {
          throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.BAD_REQUEST));
        }

      }
      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
      }
    }

    /// <summary>
    /// Get Event Category
    /// </summary>
    /// <param name="Id"></param>
    /// <returns></returns>
    [ApiAuthenticationFilter]
    [HttpGet]
    public HttpResponseMessage GetCategory([FromUri] int Id)
    {
      try
      {
        if (Id > 0)
        {
          EventCategoryDTO categoryDTO = new EventCategoryDTO();
          categoryDTO = _account.GetCategory(Id);
          return Request.CreateResponse(HttpStatusCode.OK, categoryDTO);
        }
        else
        {
          throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.BAD_REQUEST));
        }
      }

      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
      }

    }
    /// <summary>
    /// Login User
    /// </summary>
    /// <param name="email"></param>
    /// <param name="password"></param>
    /// <returns>HttpResponseMessage</returns>
    [HttpPost]
    public HttpResponseMessage Login(LoginModel loginInput)
    {
      try
      {
        var errors = ModelState.Values.SelectMany(v => v.Errors);
        if (ModelState.IsValid)
        {
          // var errors1 = ModelState.Values.SelectMany(v => v.Errors);
          LoginResponse response = new LoginResponse();
          if (string.IsNullOrWhiteSpace(loginInput.Provider))
          {
            if (!string.IsNullOrWhiteSpace(loginInput.Password))
            {
              response = _account.Authentication(loginInput.Email, loginInput.Password, loginInput.Provider, true);
            }
            else
            {
              throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.BAD_REQUEST));
            }

          }
          else
          {
            response = _account.Authentication(loginInput.Email, loginInput.Password, loginInput.Provider, true);
          }

          if (response == null)
          {
            return Request.CreateResponse(HttpStatusCode.NotFound, "User not found");
          }
          else
          {
            return Request.CreateResponse(HttpStatusCode.OK, response);
          }
        }
        else
        {
          throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.BAD_REQUEST));
        }
      }
      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
      }
    }

    /// <summary>
    /// Updating Artist Profile with skills and artist multiple category id.
    /// </summary>
    [ApiAuthenticationFilter]
    [HttpPost]
    public HttpResponseMessage AddProfile([FromBody]UserProfileDTO UserInput)
    {
      try
      {
        var errors = ModelState.Values.SelectMany(v => v.Errors);

        if (ModelState.IsValid)
        {
          long userId = Convert.ToInt64(HttpContext.Current.Items["UserId"]);
          UserInput.Id = userId;

          if (UserInput.UserType == "Artist")
          {
            if (UserInput.ArtistSkills.Length > 0 || UserInput.SkillsName.Length > 0)
            {

              bool check = _account.UpdateArtistProfile(UserInput, true);
              if (check)
              {
                return Request.CreateResponse(HttpStatusCode.OK, new { IsUpdated = check });
              }
              else
              {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.BAD_REQUEST));
              }
            }
            else
            {
              throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.PARAMETER_MISSING));
            }
          }
          else
          {

            bool check = _account.UpdateArtistProfile(UserInput, true);
            if (check)
            {
              return Request.CreateResponse(HttpStatusCode.OK, new { IsUpdated = check });
            }
            else
            {
              throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.BAD_REQUEST));
            }
          }
        }
        else
        {
          throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
        }
      }

      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.DUPLICATE_DATA));
      }
    }

    /// <summary>
    /// Get User Data with multiple skills and categories.
    /// </summary>
    /// <returns></returns>
    [ApiAuthenticationFilter]
    [HttpGet]
    public HttpResponseMessage GetUserDetails(long profileId)
    {
      try
      {
        long userId = 0;
        if (profileId == 0)
        {
          userId = Convert.ToInt64(HttpContext.Current.Items["UserId"]);
        }
        else
        {
          userId = profileId;
        }


        UserDetailDTO userDTO = new UserDetailDTO();
        userDTO = _account.GetUserData(userId);
        if (userDTO.Id > 0)
        {
          return Request.CreateResponse(HttpStatusCode.OK, userDTO);
        }
        else
        {
          throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.BAD_REQUEST));
        }
      }

      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
      }
    }

    /// <summary>
    /// logout api
    /// </summary>
    /// <returns></returns>
    [ApiAuthenticationFilter]
    [HttpDelete]
    public HttpResponseMessage Logout()
    {
      try
      {
        bool isSuccess = false;
        //long userId = Convert.ToInt64(HttpContext.Current.Items["UserId"]);

        if (HttpContext.Current.Session != null)
        {
          string token = HttpContext.Current.Session["AccessToken"].ToString();
          isSuccess = _account.Logout(token);
        }
        if (isSuccess)
        {
          return Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = isSuccess });
        }
        else
        {
          throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.BAD_REQUEST));
        }
      }
      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
      }
    }
    /// get list or all Artists as per search text
    /// </summary>
    /// <param name="searchTitle"></param>
    /// <returns></returns>
    [HttpGet]
    public HttpResponseMessage SearchArtist([FromUri] string searchTitle)
    {
      try
      {
        List<UserDetailDTO> events = _account.SearchArtist(searchTitle);
        if (events != null && events.Count > 0)
        {
          return Request.CreateResponse(HttpStatusCode.OK, events);
        }
        else
        {
          throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.DATA_NOT_FOUND));
        }
      }

      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
      }
    }

    /// get list or all Organizers as per search text
    /// </summary>
    /// <param name="searchTitle"></param>
    /// <returns></returns>
    [HttpGet]
    public HttpResponseMessage SearchUser([FromUri] string searchTitle, string UserType)
    {
      try
      {
        List<UserDetailDTO> events = _account.SearchUser(searchTitle, UserType);
        if (events != null && events.Count > 0)
        {
          return Request.CreateResponse(HttpStatusCode.OK, events);
        }
        else
        {
          throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.DATA_NOT_FOUND));
        }
      }

      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
      }
    }
    /// <summary>
    /// Get All Artist skills from master table.
    /// </summary>
    /// <returns></returns>
    [AllowAnonymous]
    [HttpGet]
    public HttpResponseMessage GetArtistSkills()
    {
      try
      {
        List<ArtistSkillsDTO> artistSkills = _account.GetArtistSkills();
        if (artistSkills.Count > 0)
        {
          return Request.CreateResponse(HttpStatusCode.OK, artistSkills);
        }
        else
        {
          throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.DATA_NOT_FOUND));

        }
      }
      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
      }
    }
    /// <summary>
    /// check for whether the email id is present or not.
    /// </summary>
    /// <param name="RegisterData"></param>
    /// <returns></returns>
    [AllowAnonymous]
    [HttpGet]
    public HttpResponseMessage CheckUser(string Email, string Provider)
    {
      try
      {
        var errors = ModelState.Values.SelectMany(v => v.Errors);
        if (ModelState.IsValid)
        {
          // var errors1 = ModelState.Values.SelectMany(v => v.Errors);
          LoginResponse response = new LoginResponse();
          response = _account.CheckUser(Email, Provider);

          if (response != null)
          {
            return Request.CreateResponse(HttpStatusCode.OK, response);
          }
          else
          {
            return Request.CreateResponse(HttpStatusCode.NotFound, UtilityBL.ErrorMessages.EMAIL_DOESNOT_EXISTS);
          }
        }
        else
        {
          throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.BAD_REQUEST));
        }
      }
      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
      }
    }


  }
}

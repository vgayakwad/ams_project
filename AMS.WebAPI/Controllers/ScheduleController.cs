using AMS.BL;
using AMS.Models.Models;
using AMS.WebAPI.FilterAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AMS.WebAPI.Controllers
{
  [ApiAuthenticationFilter]
  public class ScheduleController : ApiController
  {
    private readonly ISchedule _schedule;
    public ScheduleController(ISchedule schedule)
    {
      _schedule = schedule;
    }
    /// <summary>
    /// Displaying  Events of given artist id with schedule details .
    /// </summary>
    /// <param name="UserId"></param>
    /// <returns></returns>
    [HttpGet]
    [AllowAnonymous]
    public HttpResponseMessage GetArtistScheduledEvents(int UserId)
    {
      try
      {
        if (UserId != 0)
        {
          List<ScheduledEventsDTO> events = _schedule.GetArtistsScheduledEvents(UserId);
          if (events != null && events.Count > 0)
          {
            return Request.CreateResponse(HttpStatusCode.OK, events);
          }
          else
          {
            throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.DATA_NOT_FOUND));
          }
        }
        else
        {
          return Request.CreateResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.PARAMETER_MISSING);
        }
      }

      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
      }
    }
    /// <summary>
    /// Inserting Schedule data in database
    /// </summary>
    /// <param name="schedule"></param>
    /// <returns></returns>
    [HttpPost]
    public HttpResponseMessage AddArtistSchedule(ArtistScheduleDTO schedule)
    {
      try
      {
        if (ModelState.IsValid)
        {
          bool IsSuccess = _schedule.CreateArtistSchedule(schedule);
          if (IsSuccess)
          {
            return Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = IsSuccess });
          }
          else
          {
            throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.BAD_REQUEST));
          }

        }
        else
        {
          return Request.CreateResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.PARAMETER_MISSING);
        }

      }
      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
      }
    }
    /// <summary>
    /// Displaying all Artist schedules.
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public HttpResponseMessage GetAllArtistSchedules()
    {
      try
      {
        List<ArtistScheduleDTO> schedules = _schedule.GetArtistSchedules();
        if (schedules != null && schedules.Count > 0)
        {
          return Request.CreateResponse(HttpStatusCode.OK, schedules);
        }
        else
        {
          throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.DATA_NOT_FOUND));
        }
      }

      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
      }
    }
    /// <summary>
    /// Displaying artist schedules of given artist id.
    /// </summary>
    /// <param name="artistId"></param>
    /// <returns></returns>
    [HttpGet]
    [AllowAnonymous]
    public HttpResponseMessage GetArtistSchedulesByArtistId(int artistId)
    {
      try
      {
        if (artistId != 0)
        {
          List<ArtistScheduleDTO> schedules = _schedule.GetArtistScheduleByArtistId(artistId);
          if (schedules != null && schedules.Count > 0)
          {
            return Request.CreateResponse(HttpStatusCode.OK, schedules);
          }
          else
          {
            throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.DATA_NOT_FOUND));
          }
        }
        else
        {
          return Request.CreateResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.PARAMETER_MISSING);
        }

      }

      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
      }
    }

    /// <summary>
    /// Displaying Schedules of given schedule id
    /// </summary>
    /// <param name="ScheduleId"></param>
    /// <returns></returns>
    [HttpGet]
    
    public HttpResponseMessage GetScheduleByScheduleId(int ScheduleId)
    {
      try
      {
        //add if condition for scheduleid if schedule id is missing
        if (ScheduleId != 0)
        {
          ArtistScheduleDTO schedules = _schedule.GetScheduleByScheduleId(ScheduleId);
          if (schedules != null && schedules.ScheduleId > 0)
          {
            return Request.CreateResponse(HttpStatusCode.OK, schedules);
          }
          else
          {
            throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.DATA_NOT_FOUND));
          }
        }
        else
        {
          return Request.CreateResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.PARAMETER_MISSING);
        }

      }

      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
      }
    }
    /// <summary>
    /// Deleting Schedule of given Schedule Id
    /// </summary>
    /// <param name="ScheduleId"></param>
    /// <returns></returns>
    [HttpDelete]
    public HttpResponseMessage DeleteScheduleByScheduleId(int ScheduleId)
    {
      try
      {
        //add if condition for scheduleid if schedule id is missing
        if (ScheduleId != 0)
        {
          bool IsSuccess = _schedule.DeleteScheduleByScheduleId(ScheduleId);
          if (IsSuccess)
          {
            return Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = IsSuccess });
          }
          else
          {
            throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.DATA_NOT_FOUND));
          }
        }
        else
        {
          return Request.CreateResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.PARAMETER_MISSING);
        }

      }

      catch (HttpResponseException ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw;
      }
      catch (Exception ex)
      {
        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
      }
    }


  }
}

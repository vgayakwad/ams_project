using AMS.BL;
using AMS.Models;
using AMS.Models.Models;
using AMS.WebAPI.FilterAttributes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace AMS.WebAPI.Controllers
{
    [ApiAuthenticationFilter]
    public class CommonController : ApiController
    {
        private readonly ICommon _common;
        
        public CommonController(IAccount account, ICommon common)
        {
            _common = common;
        }
        /// <summary>
        /// get  artists category list from database
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetArtistCategory()
        {
            try
            {
                List<ArtistCategoryDTO> categoryDTO = new List<ArtistCategoryDTO>();
                categoryDTO = _common.GetAllArtistCategory();
                return Request.CreateResponse(HttpStatusCode.OK, categoryDTO);
            }

            catch (HttpResponseException ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
            }
        }
        /// <summary>
        /// get country list from database
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage GetCountryList()
        {
            try
            {
                List<CountryDTO> countryDTO = new List<CountryDTO>();
                countryDTO = _common.GetAllCountries();
                return Request.CreateResponse(HttpStatusCode.OK, countryDTO);
            }

            catch (HttpResponseException ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
            }
        }
        /// <summary>
        /// get state list by countryCode
        /// </summary>
        /// <param name="countryCode"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetStateByCountryCode([FromUri] string countryCode)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(countryCode))
                {
                    List<StateDTO> lstStates = new List<StateDTO>();
                    lstStates = _common.GetStateByCountryCode(countryCode);
                    if (lstStates.Count > 0)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, lstStates);
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.BAD_REQUEST);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.PARAMETER_MISSING);
                }
            }

            catch (HttpResponseException ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
            }

        }
        /// <summary>
        /// get city list by statecode
        /// </summary>
        /// <param name="stateCode"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetCityByStateCode([FromUri] string stateCode)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(stateCode))
                {

                    List<CityDTO> lstCities = new List<CityDTO>();
                    lstCities = _common.GetCityByStateCode(stateCode);
                    if (lstCities.Count > 0)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, lstCities);
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.BAD_REQUEST);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.PARAMETER_MISSING);
                }
            }
            catch (HttpResponseException ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
            }
        }
        /// <summary>
        /// get evet category list from database
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public HttpResponseMessage GetEventCategory()
        {
            try
            {
                List<EventCategoryDTO> categoryDTO = new List<EventCategoryDTO>();
                categoryDTO = _common.GetAllEventCategory();
                if (categoryDTO.Count > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, categoryDTO);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.BAD_REQUEST);
                }
              
            }

            catch (HttpResponseException ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
            }
        }
        /// <summary>
        /// get timezone of country by country code
        /// </summary>
        /// <param name="countryCode"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetTimeZoneByCountryCode([FromUri]string countryCode)
        {
            try
            {
                List<TimeZoneDTO> lstZones = new List<TimeZoneDTO>();
                if (!string.IsNullOrWhiteSpace(countryCode))
                {
                    lstZones = _common.GetTimeZoneByCountryCode(countryCode);
                    if (lstZones.Count > 0)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, lstZones);
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.BAD_REQUEST);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.PARAMETER_MISSING);
                }
            }

            catch (HttpResponseException ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
            }

        }
    }
}


using AMS.BL;
using AMS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AMS.WebAPI.Controllers
{
    public class RateAndReviewController : ApiController
    {
        private readonly IUserRating _rating;

        public RateAndReviewController(IUserRating rating)
        {
            _rating = rating;
        }

        [HttpPost]
        public HttpResponseMessage AddUserRating([FromBody]UserRatingDTO objRating)
        {
            try
            {
                if (ModelState.IsValid)
                {                  
                        bool IsAdded = _rating.AddUserRating(objRating);

                        if (IsAdded)
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, new { IsSuccess = IsAdded });
                        }
                        else
                        {
                            throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.DATA_EXISTS));
                        }
                }
                else
                {
                    throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, UtilityBL.ErrorMessages.BAD_REQUEST));
                }
            }
            catch (HttpResponseException ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, UtilityBL.ErrorMessages.INTERNAL_SERVER_ERROR));
            }
        }

    }
}

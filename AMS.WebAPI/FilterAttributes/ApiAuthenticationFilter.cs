using AMS.BL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;

namespace AMS.WebAPI.FilterAttributes
{
    public class ApiAuthenticationFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            HttpStatusCode errorCode = HttpStatusCode.InternalServerError;
            CommonBL common = new CommonBL();
            bool isValid = false;
            try
            {
                if (actionContext != null)
                {
                    //Check if authentication token exist                  

                    if (actionContext.Request.Headers.Contains("Authorization"))
                    {
                        string accessToken = actionContext.Request.Headers.GetValues("Authorization").FirstOrDefault();

                        string tokenVal = Convert.ToString(accessToken).Trim();
                        HttpContext.Current.Session["AccessToken"] = tokenVal;

                        //string value = actionContext.ActionArguments.Where(r => r.Key.ToString() == "eventList").FirstOrDefault();
                        string value = string.Empty;
                        var value1 = actionContext.ActionArguments.Where(r => r.Key.ToString() == "eventList").FirstOrDefault();
                       
                        if (value1.Value != null)
                        {
                            value = ((AMS.Models.Models.EventListDTO)value1.Value).EventAttribute;
                        }

                        if (value.Contains("PASTEVENTS"))
                        {
                            if (string.IsNullOrEmpty(tokenVal))
                            {
                                errorCode = HttpStatusCode.BadRequest;
                                throw new Exception(UtilityBL.ErrorMessages.BAD_REQUEST);
                            }
                            else
                            {
                                isValid = common.CheckAccessToken(tokenVal);
                            }
                        }
                        else if (tokenVal != "dummyaccessToken")
                        {
                            if (string.IsNullOrEmpty(tokenVal))
                            {
                                errorCode = HttpStatusCode.BadRequest;
                                throw new Exception(UtilityBL.ErrorMessages.BAD_REQUEST);
                            }
                            else
                            {
                                isValid = common.CheckAccessToken(tokenVal);
                            }
                        }
                        else
                        {
                            if (actionContext.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any())
                            {
                                return;
                            }
                        }

                    }
                    else
                    {
                        if (actionContext.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any())
                        {
                            return;
                        }
                    }
                   
                  
                }
                if (!isValid)
                {
                    errorCode = HttpStatusCode.Unauthorized;
                    throw new Exception(UtilityBL.ErrorMessages.UNAUTHORIZED);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                HttpResponseMessage response = new HttpResponseMessage(errorCode);
                response.Content = new StringContent("{\"Message\":\"" + ex.Message + "\"}", System.Text.Encoding.UTF8, "application/json");
                throw new HttpResponseException(response);
            }
        }
    }
}
